package s10;

public class ex1002 {

  public static void main(String[] args) {
    int[][] matrix = { { 1, 2, 3 }, { 4, 5, 6 } };

    printArray(matrix);
    System.out.println("-----------");
    try {

      printArray(transposeMatrix(matrix));
    } catch (RuntimeException e) {
      System.out.println("Exception: " + e);
    }

  }

  public static int[][] transposeMatrix(int[][] matrix)
      throws RuntimeException {
    int[][] matrixTrans = new int[matrix[0].length][matrix.length];

    for (int i = 0; i < matrix.length; ++i) {
      for (int j = 0; j < matrix[0].length; ++j) {
        matrixTrans[j][i] = matrix[i][j];
      }
    }

    return matrixTrans;

  }


  public final static void printArray(int[][] array) {
      for (int i = 0; i < array.length; i++) {
        printArray(array[i]);
        System.out.println();
      }

    }

    // affiche un tableau 1D
  public static void printArray(int[] array) {
      for (int val : array) {
        System.out.print(val + " ");

      }
    }

}
