package s10;

import proglib.SimpleIO;

public class ex1003 {

  public static void main(String[] args) {
    char[][] grille = new char[2][3];
   int area=grille[0].length*grille.length;
    int colonne=0;
    char joueur;

    for (int i = 0; i <= area; i++) {

      if (i % 2 == 0) {
        joueur = 'r';
      } else {
        joueur = 'j';
      }

      colonne = SimpleIO.readInt("Colonne joueur " + joueur);

    try {
        play(grille, colonne, joueur);
    } catch (RuntimeException e) {
      System.out.println("Rejouer");
        play(grille, SimpleIO.readInt("Colonne"), SimpleIO.readChar("Joueur"));
      }
      printArray(grille);
    }
    printArray(grille);
  }

  public static void play(char[][] grille, int colonne, char joueur)
      throws RuntimeException {


    for (int i = grille.length; i >= 0; --i) {
      

      if (!(grille[colonne][i] == 'r' || grille[colonne][i] == 'j')) {
        grille[colonne][i] = joueur;
        break;
      }

    }
   
  }


  public static void printArray(char[][] array) {
      for (int i = 0; i < array.length; i++) {
        printArray(array[i]);
        System.out.println();
      }

    }

    // affiche un tableau 1D
  public static void printArray(char[] array) {
    for (char val : array) {
        System.out.print(val + " ");

      }
    }
}
