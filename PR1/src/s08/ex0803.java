package s08;

public class ex0803 {
  public static void main(String[] args) {
    char[] array1 = { 'a', 'b', 'c' };
    char[] array2 = { 'x', 'y', 'z' };

    // affichage des valeurs du tableau alterné
    for (char val : alternateArray(array1, array2)) {
      System.out.print(val + " ");
    }
  }

  // retourne le tab alterné
  public static char[] alternateArray(char[] array1, char[] array2) {
    int dimension = array1.length + array2.length;
  
    // taille du nouveau tab
    char[] alternArray=new char[dimension];
    
    int idAltern=0;
    int idArray = 0;

    while (idAltern < dimension) {
      
      // assignation de l'élément i de alternArray à l'élément i de array1
      alternArray[idAltern] = array1[idArray];
      // assignation de l'élément i+1 de alternArray à l'élément i de array2
      alternArray[++idAltern] = array2[idArray];

      idAltern++;
      idArray++;
    }
    
    return alternArray;
}

}
