package s08;

public class ex0801 {

  public static void main(String[] args) {

    int array[][] = { { 1, 2, 4, 5 }, { 12343, 423423432 },
        { 5, 5, 5, 6, 21, 2 } };
    

    printArray(array);

  }

  public static void printArray(int[][] array) {

    // affiche le tableau de 1ère dimension
    for (int i = 0; i < array.length; i++) {
      System.out.print(i + ": ");
      // affiche élément de la 2ème dimension
      for (int elem2 : array[i]) {
        System.out.print(elem2 + " ");

      }
      System.out.println();

    }

  }

}
