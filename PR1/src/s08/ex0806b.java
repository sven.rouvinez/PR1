package s08;

public class ex0806b {

  public static void main(String[] args) {
    // appel de la méthode avec le nombre d'éléments voulu
    areTwins(15);
  }

  public static void areTwins(int k) {
    // initialisation de la taille selon le k inférieur
    boolean[] array = new boolean[k];

    // fonction pour initialiser les cases si oui ou non des nombres premiers
    isPrime(allTrue(array));

    for (int i = 2; i < array.length; ++i) {
      // si indice i est un nombre premier alors on teste si la case suivante
      if (array[i]) {
        for (int j = i + 1; j < array.length; ++j) {
          if (array[j]) {
            // si j et i sont des nombres premiers, calcule et teste la valeur
            // absolue et égale à 2
            int absSubstract = Math.abs(i - j);
            if (absSubstract == 2)
              System.out.println("|" + i + "-" + j + "|" + " = " + 2);

          }

        }

      }

    }
  }
  public static boolean[] isPrime(boolean[] array) {

    // début à 2 car 0 et 1 sont déjà faux
    for (int i = 2; i < array.length; ++i) {
      // début à 2 car 0 et 1 sont false
      for (int j = 2; j < i; ++j) {

        // si % ==0 alors pas un nombre premier et fin de boucle
        if (i % j == 0) {
          array[i] = false;
          break;
          // si aucun nombre divisible alors est un nombre premier
        } else {

          array[i] = true;

        }

      }
    }

    return array;
  }

  public static boolean[] allTrue(boolean[] array) {
    array[0] = false;
    array[1] = false;

    // initialisation des cases >= 2 à true
    for (int i = 2; i < array.length; ++i) {
      array[i] = true;
    }

    return array;
  }

}
