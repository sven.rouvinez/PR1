package s08;

public class ex0805 {

  public static void main(String[] args) {
    byte[][] image = { { 5, 8 }, { 10, 13 }, { 5, 7 }, { 4, 2 }, { 1, 8 },
        { 3, 10 } };


    // affiche le tableau réduit
    printArray(reduceImage(image));
  }

  public static byte[][] reduceImage(byte[][] image) {
    // long/2 et larg/2 car le l'image doit être réduite d'un facteur 4

    int sizeImageReducedVert = (getVertSize(image) / 2);
    int sizeImageReducedHorz = (getHorzSize(image) / 2);

    byte[][] reducedImage = new byte[sizeImageReducedVert][sizeImageReducedHorz];

    int incVert = 1;

    // parcourt de façon vertivale les éléments du tab réduit
    for (int vertReduced = 0; vertReduced < reducedImage.length; vertReduced++) {
      // remis à 1 quand les 2 premiers tableau ont été faient
      int inc = 1;
      // parcout les éléments des tableaux
      for (int horzReduced = 0; horzReduced < reducedImage[vertReduced].length; horzReduced++) {
        int sommesVal = 0;

        // le parcourt des éléments de l'image doit se faire 2 par 2 dans chaque
        // tableau, donc incVert permet de faire ces pas
        for (int vert = vertReduced * 2; vert <= incVert; vert++) {
          for (int horz = horzReduced * 2; horz <= inc; horz++) {
            sommesVal += image[vert][horz];
          }
        }

        reducedImage[vertReduced][horzReduced] = (byte) Math
            .round(sommesVal / 4.0);
        // ajout de +2 pour aller aux autres éléments
        inc += 2;
      }
      incVert += 2;
    }
    return reducedImage;
  }

  // retourne la taille d'une colonne
  public static int getVertSize(byte[][] array) {
    return array.length;
  }

  // retourne la taille d'une ligne
  public static int getHorzSize(byte[][] array) {
    return array[0].length;
  }

  // affiche un tableau 2D
  public static void printArray(byte[][] array) {
    for (int i = 0; i < array.length; i++) {
      printArray(array[i]);
      System.out.println();
    }

  }

  // affiche un tableau 1D
  public static void printArray(byte[] array) {
    for (byte val : array) {
      System.out.print(val + " ");

    }
  }

}
