package s08;

public class ex0807 {

  public static void main(String[] args) {
    int array[] = { 24, 5, 6, 6, 2323, 36, 7, 50, 4 };

    System.out.println(minValue(array));
  }

  public static int minValue(int[] array) {
    // valeur abitraire du tableau
    int minValue = array[0];

    for (int i = 0; i < array.length; ++i) {
      // si valeur plus petite dans le tableau, devient minValue
      if (array[i] < minValue)
          minValue = array[i];
    }

    return minValue;
  }
}
