package s08;

public class ex0806 {
  public static void main(String[] args) {
    int dimension=14;
    boolean[] array = new boolean[dimension];

    printArray(isPrime(allTrue(array)));
      
    


  }

  public static boolean[] isPrime(boolean[] array) {

    // début à 2 car 0 et 1 sont déjà faux
    for (int i = 2; i < array.length; ++i) {
      // début à 2 car 0 et 1 sont false
      for (int j = 2; j < i; ++j) {

        // si % ==0 alors pas un nombre premier et fin de boucle
        if (i % j == 0) {
          array[i] = false;
          break;
          // si aucun nombre divisible alors est un nombre premier
        } else {
          array[i] = true;

        }

      }
    }

    return array;
  }

  public static boolean[] allTrue(boolean[] array) {
    array[0] = false;
    array[1] = false;

    // initialisation des cases >= 2 à true
    for (int i = 2; i < array.length; ++i) {
      array[i] = true;
    }

    return array;
  }

  public static void printArray(boolean[] array) {
    for (boolean val : array) {
      System.out.print(val + " ");

    }
  }
}
