package s08;

public class ex0804 {

  public static void main(String[] args) {
    int[] monoSerie = { 3, 1, 4, 6, 2, 9, 1, 2, 3, 4, 6 };

    System.out.println(countMonot(monoSerie));

  }

  public static int countMonot(int[] array) {

    int counter = 0;
    int nextIndex = 0;

    for (int i = 0; i < array.length - 1; ++i) {
      // index suivant
      nextIndex = i + 1;

      // test si valeur suivante < que actuelle
      if (array[i] < array[nextIndex]) {

        // test si fin de tableau si oui ajout d'une monotonie
        if (nextIndex + 1 >= array.length)
          counter++;
        continue;

      } else {
        // si monotonie trouvée boucle pour tester si val suivant est <
        counter++;
      }
    }
    return counter;
  }
}
