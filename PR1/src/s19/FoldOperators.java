
package s19;

import s19.PlayWithFunctions.IFoldableOperation;

public class FoldOperators {

  // ***********************************
  // ****** With internal classes ******
  // ***********************************

  public static final IFoldableOperation SUM     = new SumOperation();
  public static final IFoldableOperation PRODUCT = new ProductOperation();
  public static final IFoldableOperation MAX     = new MaxOperation();

  private static class SumOperation implements IFoldableOperation {


    @Override
    public double initialValue() {
      // initial value of sum
      return 0.0;
    }

    @Override
    public double combine(double accumulated, double newValue) {
      // series of sum
      return initialValue() + accumulated + newValue;
    }

  }

  //
  private static class ProductOperation implements IFoldableOperation {

    @Override
    public double initialValue() {
      // initial value of product
      return 1.0;
    }

    @Override
    public double combine(double accumulated, double newValue) {
      // series of product
      return initialValue() * accumulated * newValue;

    }

  }

  //
  private static class MaxOperation implements IFoldableOperation {

    @Override
    public double initialValue() {
      // smaller value for a double
      return Double.MIN_VALUE;
    }

    @Override
    public double combine(double accumulated, double newValue) {
      // return the maximum value from a series
      return Math.max(Math.max(initialValue(), accumulated), newValue);
    }

  }

  // ********************************************
  // ****** With anonymous classes (ex. 3) ******
  // ********************************************

  public static final IFoldableOperation SUM1     = new SumOperation() {
                                                    @Override
                                                    public double initialValue() {

                                                      return 0.0;
                                                    }

                                                    @Override
                                                    public double combine(
                                                        double accumulated,
                                                        double newValue) {

                                                      return initialValue()
                                                          + accumulated
                                                          + newValue;
                                                    }

                                                  };

  public static final IFoldableOperation PRODUCT1 = new ProductOperation() {
                                                    @Override
                                                    public double initialValue() {

                                                      return 1.0;
                                                    }

                                                    @Override
                                                    public double combine(
                                                        double accumulated,
                                                        double newValue) {

                                                      return initialValue()
                                                          * accumulated
                                                          * newValue;

                                                    }
                                                  };

  public static final IFoldableOperation MAX1     = new MaxOperation() {

                                                    @Override
                                                    public double initialValue() {

                                                      return Double.NEGATIVE_INFINITY;
                                                    }

                                                    @Override
                                                    public double combine(
                                                        double accumulated,
                                                        double newValue) {

                                                      return Math.max(
                                                          Math.max(
                                                              initialValue(),
                                                              accumulated),
                                                          newValue);
                                                    }

                                                  };

}