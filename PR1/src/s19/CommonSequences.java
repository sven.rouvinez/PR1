
package s19;

import s19.PlayWithFunctions.IFunction;
import s19.PlayWithFunctions.INumberSequence;

public class CommonSequences {
  // ***********************************
  // ****** With internal classes ******
  // ***********************************

  // applies specific computation on an array of double
  public static INumberSequence fromArray(double[] t) {
    return new ArrayNumberSequence(t);
  }

  // applies specific computation and function on a range of value
  public static INumberSequence fromSeries(IFunction f, int from, int to) {
    return new SeriesNumberSequence(f, from, to);
  }

  // applies specific computation and function from some samples
  public static INumberSequence fromSampledFunction(IFunction f, double from,
      double to, int nSubSamples) {

    return new SampledFunctionNumberSequence(f, from, to, nSubSamples);
  }

  // ************************************
  // ****** With internals classes ******
  // ************************************
  static class ArrayNumberSequence implements INumberSequence {
    private int      index = 0;
    private double[] t;

    public ArrayNumberSequence(double[] t) {
      this.t = t;
    }

    @Override
    public boolean hasMoreNumbers() {
      // return false when no more numbers left
      return index < t.length;

    }

    @Override
    public double nextNumber() {
      // return a value from the actual index
      return t[index++];

    }

  }

  static class SeriesNumberSequence implements INumberSequence {
    private int       from;
    private int       to;
    private IFunction f;

    public SeriesNumberSequence(IFunction f, int from, int to) {
      this.from = from;
      this.to = to;
      // function is defined in PlayWithFunctions class
      this.f = f;

    }

    @Override
    public boolean hasMoreNumbers() {
      return from <= to;
    }

    @Override
    public double nextNumber() {
      // return the value with the associated function
      return f.valueAt(from++);

    }

  }

  static class SampledFunctionNumberSequence implements INumberSequence {
    private double       from;
    private double       to;
    private int          nSubSamples;
    private IFunction    f;
    private int       index = 1;

    private double    delta;

    public SampledFunctionNumberSequence(IFunction f, double from, double to,
        int nSubSamples) {
      this.from = from;
      this.to = to;
      this.nSubSamples = nSubSamples;
      this.f = f;

    }

    @Override
    public boolean hasMoreNumbers() {
      return index++ <= nSubSamples;
    }

    @Override
    public double nextNumber() {
      // get the delta between last value and first
      delta = (to - from);
      // addition each samples on each iterations
      from += delta / nSubSamples;

      // return the value of the sample with the associated function
      return f.valueAt(from);
    }

  }

  // ************************************
  // ****** With anonymous classes ******
  // ************************************

  public static INumberSequence fromArray1(final double[] t) {
    return new INumberSequence() {
      int index;

      @Override
      public boolean hasMoreNumbers() {

        return index < t.length;

      }

      @Override
      public double nextNumber() {
        return t[index++];

      }

    };

  }

  public static INumberSequence fromSeries1(final IFunction f, final int from,
      final int to) {

    return new INumberSequence() {
      // deep copy because from is final and it has to be incremented
      int CopyFrom = from;

      @Override
      public boolean hasMoreNumbers() {
        return CopyFrom <= to;
      }

      @Override
      public double nextNumber() {
        return f.valueAt(CopyFrom++);

      }

    };
  }

  public static INumberSequence fromSampledFunction1(final IFunction f,
      final double from, final double to, final int nSubSamples) {
    return new INumberSequence() {
      // deep copy because from is final and it has to be incremented
      double fromCopy = from;
      int    index    = 1;
      private double delta;

      @Override
      public boolean hasMoreNumbers() {

        return index++ <= nSubSamples;
      }

      @Override
      public double nextNumber() {
        delta = (to - fromCopy);

        fromCopy += delta / (double) nSubSamples;

        return f.valueAt(fromCopy);
      }

    };
  }

}
