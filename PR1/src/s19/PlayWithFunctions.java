
package s19;

public class PlayWithFunctions {

  public interface IFoldableOperation {
    double initialValue();

    double combine(double accumulated, double newValue);
  }

  // =================================================
  public interface INumberSequence {
    boolean hasMoreNumbers();

    double nextNumber();
  }

  static class Fa implements IFunction {

    @Override
    public double valueAt(double x) {
      // specific function to applies on each elements
      return x / Math.pow(2, x);
    }

  }

  static class Fb implements IFunction {

    @Override
    public double valueAt(double x) {
      // specific function to applies on each elements
      return (x * x) / Math.pow(2, x);
    }

  }

  static class Fc implements IFunction {

    @Override
    public double valueAt(double x) {
      // specific function to applies on each elements
      return Math.sin(x) * Math.sin(x) * Math.cos(x);
    }

  }

  // =================================================
  public interface IFunction {
    double valueAt(double x);
  }


  public static double foldLeft(INumberSequence ns, IFoldableOperation op) {
    // store the initial value from a specific operation (SUM,PRODUCT, MAX)
    double result = op.initialValue();

    // iterate until the sequence is over
    while (ns.hasMoreNumbers()) {
      // applies the operation to the precedent result and next number of the
      // sequence
      result = op.combine(ns.nextNumber(), result);
    }
    return result;

  }

  private static void computeAll(IFunction fa, IFunction fb, IFunction fc) {

    // create a sequence
    double[] t = { 1.2, 3.4, 5.6 };

    // use the sequence from the array t
    System.out.println("**With internal static classes**");
    System.out
        .println(foldLeft(CommonSequences.fromArray(t), FoldOperators.PRODUCT));

    // applies sum operation between values from 0 to 20 with fa
    System.out.println(
        foldLeft(CommonSequences.fromSeries(fa, 0, 20), FoldOperators.SUM));
    // applies sum operation between values from 0 to 20 with fb
    System.out.println(
        foldLeft(CommonSequences.fromSeries(fb, 0, 20), FoldOperators.SUM));

    // applies max operation between values from 0 to PI with 1002 samples
    System.out.println(
        foldLeft(CommonSequences.fromSampledFunction(fc, 0.0, Math.PI, 1002),
            FoldOperators.MAX));

    // *************************************
    // ****** With anonymous classes: ******
    // *************************************

    System.out.println("**With anonymous classes**");
    System.out.println(
        foldLeft(CommonSequences.fromArray1(t), FoldOperators.PRODUCT1));

    System.out.println(
        foldLeft(CommonSequences.fromSeries1(fa, 0, 20), FoldOperators.SUM1));
    System.out.println(
        foldLeft(CommonSequences.fromSeries1(fb, 0, 20),
            FoldOperators.SUM1));

    System.out.println(
        foldLeft(CommonSequences.fromSampledFunction1(fc, 0.0, Math.PI, 1002),
            FoldOperators.MAX1));


  }

  public static void main(String[] args) {
    IFunction fa = null, fb = null, fc = null;
    // *******************************************
    // ****** With internal static classes: ******
    // *******************************************
    fa = new Fa();
    fb = new Fb();
    fc = new Fc();

    System.out.println("***With internal function classes***");
    computeAll(fa, fb, fc);

    // *************************************
    // ****** With anonymous classes: ******
    // *************************************
    fa = new IFunction() {

      @Override
      public double valueAt(double x) {
        return x / Math.pow(2, x);
      }

    };

    fb = new IFunction() {

      @Override
      public double valueAt(double x) {
        return (x * x) / Math.pow(2, x);
      }

    };

    fc = new IFunction() {

      @Override
      public double valueAt(double x) {
        return Math.sin(x) * Math.sin(x) * Math.cos(x);
      }

    };


    System.out.println("***With anonymous function classes***");
    computeAll(fa, fb, fc);

    // **************************************
    // ****** With lambda expressions: ****** <----- Ce sera pour le TP S22 !!!
    // **************************************
    fa = (x) -> (x / Math.pow(2, x));
    fb = (x) -> (Math.sin(x) * Math.sin(x) * Math.cos(x));
    fc = (x) -> (Math.sin(x) * Math.sin(x) * Math.cos(x));
    computeAll(fa, fb, fc);

  }
}
