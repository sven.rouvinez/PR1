
package s19;

import s19.PlayWithSeries.Series.SeriesB1;
import s19.PlayWithSeries.Series.SeriesB2;

public class PlayWithSeries {
  
  public static abstract class Series {
    public abstract double function(int i);

    // generalization of a sum series
    public double evaluateBetween(int from, int to) {
      double sum=0; 
      for(int i=from; i<=to; i++) {
        sum += function(i);
      }
      return sum;
    }

    // function who use sum
    public static class SeriesB1 extends Series {

      @Override
      public double function(int i) {
        return i / (Math.pow(2, i));
      }

    }

    // function who use sum
    public static class SeriesB2 extends Series {

      @Override
      public double function(int i) {
        return (i * i) / Math.pow(2, i);
      }

    }
  }



  public static void main(String[] args) {
    // *******************************************
    // ****** With internal static classes: ******
    // *******************************************
    System.out.println("With internal class");
    Series sa = new SeriesB1();
    Series sb = new SeriesB2();
    System.out.println(sa.evaluateBetween(0, 20));
    System.out.println(sb.evaluateBetween(0, 20));
    System.out.println();


    
    // *********************************************
    // ****** With anonymous classes: (ex. 3) ******
    // *********************************************
    Series saA = new Series() {

      @Override
      public double function(int i) {

        return i / (Math.pow(2, i));
      }

      };

    Series sbB = new Series() {

      @Override
      public double function(int i) {

        return (i * i) / Math.pow(2, i);
      }

    };

    System.out.println("With anonymous class");
    System.out.println(saA.evaluateBetween(0, 20));
    System.out.println(sbB.evaluateBetween(0, 20));

    }
  
}
