package s16.vobject;

public class Rectangle {
  
  private double width;
  private double length;
  
  public Rectangle(double width, double length) {
    this.width = width;
    this.length = length;
  }

  public double getWidth() {
    return width;
  }

  public double getLength() {
    return length;
  }

  public double area() {
    return length * width;
  }

  // modify the size of the rectangle with by number
  public void multiplyByK(int k) {
    width *= k;
    length *= k;
  }

  public boolean isGreaterThan(Rectangle r) {
    return this.area() > r.area() ? true : false;

  }

}
