package s16.vobject;

public class Ex1604 {

  public static void main(String[] args) {
    Rectangle x, y;
    x = new Rectangle(2.1, 3.9);
    y = new Rectangle(2.4, 3.9);



    System.out.println(String.format("Area x is %.3f", x.area()));
    System.out.println(String.format("Area y is %.3f", y.area()));

    System.out.println(x.isGreaterThan(y));

    x.multiplyByK(3);
    System.out.println(String.format("Length of x is %.3f", x.getLength()));
    System.out.println(x.isGreaterThan(y));

  }

}
