package s16.ex1605.RectangleB;


public final class Rectangle {

  private final double length, width;

  private Rectangle(double length, double width) {
    this.length = length;
    this.width = width;
  }

  public final static Rectangle create(double length, double width) {
    Rectangle r = new Rectangle(length, width);

    return r;
  }

  public final static double area(Rectangle r) {
    return r.length * r.width;

  }

  public final static double getLength(Rectangle r) {
    return r.length;
  }

  public final static double getWidth(Rectangle r) {
    return r.width;
  }

  public final static Rectangle multiplyByK(Rectangle r, int k) {
    double newLength = r.length * k;
    double newWidth = r.width * k;

    return new Rectangle(newLength, newWidth);
  }

  public final static boolean isGreaterThan(Rectangle r1, Rectangle r2) {
    if (area(r1) > area(r2))
      return true;

    return false;
  }

}
