package s16.ex1605.DateA;

public final class DateA {
  private final static int MIN_DAY            = 1;
  private final static int MIN_MONTH          = 1;
  private final static int MAX_MONTH          = 12;

  // declare number of day in month JANUAR to DECEMBER
  private final static int NUMBER_DAY_MONTH[] = { 31, 28, 31, 30, 31, 30, 31,
      31, 30, 31, 30, 31 };

  private final int        day;
  private final int        month;

  public DateA(int day, int month) throws Exception {
    if (month < MIN_MONTH || month > MAX_MONTH) {
      throw new Exception("false month");
    }

    if (day > NUMBER_DAY_MONTH[month - 1] || day < MIN_DAY) {
      throw new Exception("false day");
    }

    this.day = day;
    this.month = month;
  }

  public final int dayOfMonth() {
    return day;
  }

  public final int month() {
    return month;
  }
}
