package s16.ex1605.RectangleA;


final class Rectangle {

  private final double width;
  private final double length;

  public Rectangle(double width, double length) {
    this.width = width;
    this.length = length;
  }

  public final double getWidth() {
    return width;
  }

  public final double getLength() {
    return length;
  }

  public double area() {
    return length * width;
  }

  public final void multiplyByK(int k) {

    double newWidth = width * k;
    double newLength = length * k;
    new Rectangle(newWidth, newLength);
  }

  public final boolean isGreaterThan(Rectangle r) {
    if (this.area() > r.area())
      return true;

    return false;
  }

}
