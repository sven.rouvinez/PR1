package s16.vstatic;


public class Rectangle {

  private double length, width;

  // need a constructor because we have static methods
  private Rectangle(double length, double width) {
    this.length = length;
    this.width = width;
  }

  // create the object rectangle and return it
  public static Rectangle create(double length, double width) {

    return new Rectangle(width, length);
  }

  public static double area(Rectangle r) {
    return r.length * r.width;
    
  }

  public static double getLength(Rectangle r) {
    return r.length;
  }

  public static double getWidth(Rectangle r) {
    return r.width;
  }

  public static Rectangle multiplyByK(Rectangle r, int k) {
    r.length *= k;
    r.width *= k;

    return r;
  }

  public static boolean isGreaterThan(Rectangle r1, Rectangle r2) {
    return area(r1) > area(r2) ? true : false;
  }

}
