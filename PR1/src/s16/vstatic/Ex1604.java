package s16.vstatic;

public class Ex1604 {

  public static void main(String[] args) {
    Rectangle x, y;
    x = Rectangle.create(2.1, 3.9);

    y = Rectangle.create(2.4, 3.9);

    double areaX=Rectangle.area(x);
    double areaY=Rectangle.area(y);
    
    System.out.println(String.format("Area x is %.3f", areaX));
    System.out.println(String.format("Area y is %.3f", areaY));

    System.out.println(Rectangle.isGreaterThan(x, y));

    Rectangle.multiplyByK(x, 3);
    System.out
        .println(String.format("Length of x is %.3f", Rectangle.getLength(x)));
    System.out.println(Rectangle.isGreaterThan(x, y));

  }

}
