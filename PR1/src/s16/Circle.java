//==============================================================================
//  HEIA-FR
//==============================================================================
package s16;

public class Circle {

  // private because we don't need it in other classes (arbitrary)
  private final static double PI = 3.14159265358;
  private double              radius;

  public Circle(double r) {
    radius = r;
  }

  public Circle() {
    radius = 0;
  }

  // nothing is modified so we can use a static method
  public static double perimeter(Circle c) {

    return 2 * PI * c.radius;
  }

  public double area() {

    return PI * sqrRad();
  }

  public boolean isSmaller(Circle c) {
    return radius < c.radius ? true : false;

  }

  public Circle fromArea(double area) {
    radius = Math.sqrt(area / PI);

    return this;
  }

  // private because we don't need it in other classes (arbitrary)
  private double sqrRad() {
    return radius * radius;
  }
}
