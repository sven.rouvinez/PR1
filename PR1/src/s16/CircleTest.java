package s16;

public class CircleTest {

  public static void main(String[] args) {
    Circle c1 = new Circle(10);
    Circle c2 = new Circle(11);
    Circle c3 = new Circle();

    c3.fromArea(40);

    System.out.println(String.format("Perimeter: %.3f", Circle.perimeter(c1)));
    System.out.println(String.format("Area: %.3f", c1.area()));
    System.out.println(String.format("Is smaller: %b", c1.isSmaller(c2)));
    System.out
        .println(String.format("Radius from area: %.3f", c3.area()));
  }

}
