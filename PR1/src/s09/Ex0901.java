package s09;

public class Ex0901 {
  
  public static void main(String[] args) {
    System.out.println("A");
    try {
      System.out.println("B");
      //generate an exception and print a W
      px(0);
      System.out.println("C");
    } catch (Exception e) {
        //catch the exception because the loop is out of array
      System.out.println("Z");
    }
    //print D at the end
    System.out.println("D");
  }

  public static void px(int i) throws Exception {
    int[] t = new int[3];
    int j;
    try {
      j = 100 / i;
      System.out.println("X");
    } catch (Exception e) {
      System.out.println("W");
    } 
    
    for (j=0; j<5; j++) {
        //print from 0 to 2
      System.out.println(j);
      //can do it only once
      t[j] = 3+j;
    }
    //print 3
    System.out.println(t.length);
  }  
}
