package s09;

public class SquareRoot {

  public static void main(String[] args) {

    try {
      System.out.println(intSqrt(49));
    } catch (NegativeNumberException e) {
      System.out.println(e);
    }
  }
    //throws show that this method is possibly risky and could transmit an error
  public static int intSqrt(int number) throws NegativeNumberException {
    if (number < 0) {
        //if the condition above is verified, transmit the Exception
      throw new NegativeNumberException("Nombre negatif");
    }

    int squareValue = 1;
    // use the loop to check each root of the number starting from 1 to i
    // (ex:1*1, 2*2, 3*3)
    for (int i = 1; i < number; i++) {
      // check if the result of i*i is perfectly the root of the number
      if ((i * i) == number) {
        squareValue = i;
        break;
        // if the result of the root is a double . this if check when the next
        // i*i is > number and return the i from before.
      } else if (i * i > number) {
        squareValue = i - 1;
        break;
      }
    }

    return squareValue;

  }

}
