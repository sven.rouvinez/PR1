package s09;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ex0903 {
    // the exception isnt verified in this method but forward the exception further
  static void readFirstLine(BufferedReader br) throws IOException {
    String s;

    s = br.readLine();
    if (!s.startsWith("/*"))
      println(s);

  }

   // the exception isnt verified in this method but forward the exception further
  static void readOtherLines(BufferedReader br) throws IOException {
    String s;

    s = br.readLine();
    while (s != null) {
      println(s);
      s = br.readLine();
    }

  }

  static void processFile(String filename) {
    // try to excecute the lines belows 
    try (FileReader fp = new FileReader(filename);
        BufferedReader br = new BufferedReader(fp)) {

      readFirstLine(br);
      readOtherLines(br);
     // if an IOexception appear, the line below will treat it
    } catch (IOException e) {
      println("erreur... ");
      System.exit(-1);
    }

  }

  // print the String s
  public static void println(String s) {
    System.out.println(s);
  }

  // excecute the method processFile
  public static void main(String[] args) {
    String filename = "LineKiller.java";
    processFile(filename);
  }

}