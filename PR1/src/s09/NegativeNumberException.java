package s09;

//Create a new Exception from the class Exception
public class NegativeNumberException extends Exception {

  public NegativeNumberException() {
    super();
  }

  public NegativeNumberException(String message) {
    super(message);
  }

}
