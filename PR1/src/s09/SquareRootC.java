package s09;

public class SquareRootC {

  public static void main(String[] args) throws NegativeNumberException {
    int[] array = { 25, 36, 49, 64, -25, -36, -49, 125 };

    System.out.println(++array[0]);
    intArraySqrt(new int[] { 14, 18, -36, -49, 125 });

  }
    //int p is the position
  public static int intSqrt(int number, int p) throws NegativeNumberException {
    if (number < 0) {
      throw new NegativeNumberException();
    }
    int squareValue = 1;
    //use the loop to check each root of the number starting from 1 to i (ex:1*1, 2*2, 3*3)
    for (int i = 1; i < number; i++) {
        //check if the result of i*i is perfectly the root of the number
      if ((i * i) == number) {
        squareValue = i;
        break;
        //if the result of the root is a double . this if check when the next i*i is > number and return the i from before.
      } else if (i * i > number) {
        squareValue = i - 1;
        break;
      }
    }

    return squareValue;


  }

  public static void intArraySqrt(int[] array) throws NegativeNumberException {
    int sqrt=0;
    
    for (int i = 0; i < array.length; ++i) {

      try {
         //invoke the method intSqrt to get the root int sqrt of each 
        sqrt = intSqrt(array[i], i);
        //convert the sqrt in string to match the gender of the printJoli method
        String intToStrSqrt = Integer.toString(sqrt);
        //invoke the method printJoli for the shape
        printJoli(i, array[i], "entière " + intToStrSqrt);
      }
        catch(NegativeNumberException e){
        printJoli(i, array[i], "non définie");

        }


     
    }

  }
    // print the output in a good shape ( p=position, number= number , string = root converted in string)
  public static void printJoli(int p, int number, String string) {
    System.out.println("Indice " + p + ", " + "Nombre " + number + ", "
        + "racine carrée " + string);
  }

}
