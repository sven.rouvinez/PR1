package s02;

public class Ex0203 {
  public static void main(String[] args) {
    int a = 2, b = 3, c = 4, x = 0, y = 0, z = 0;

    x = a + b + c;
    y = a * b * c;
    z = b;
    System.out.println(x + " " + y + " " + z);

    b = b + c;
    a = a + b;
    c = b - c;
    b = b - c;
    b = b * c * (a - b - c);
    x = a;
    y = b;
    z = c;

    System.out.println(x + " " + y + " " + z);

  }
}
