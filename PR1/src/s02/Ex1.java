package s02;

import proglib.SimpleIO;

public class Ex1 {
  public static void main(String[] args) {
    
    

//    1 
     byte b = 5; short s = 8; int i = 9; float f = 2.5F; double d = 5.2;
//
System.out.println(d / 2);
//     System.out.println(typeof(s*4));
//     System.out.println(typeof((b+s) > (5*f)));
//     System.out.println(typeof(i/4 + d));
//     System.out.println(typeof(1/3));
//     System.out.println(typeof(b&6));
//     System.out.println(typeof(b*b));
//     System.out.println(typeof(i^3));
//     System.out.println(typeof(i*4/12%6));
//     System.out.println("Maybe int:"+typeof(b<<2));
//     System.out.println(typeof(s==5 | !false));
//     System.out.println(typeof(i++));
//     
     
    /**
     * 2 int j = 7, k = 4; boolean a = true, b = false;
     * 
     * a = 7 >= 3 | (7 != 3); b = a != true; a ^= true; a &= false | b | 7 == 3
     * + 4; b= k++ >=3 || j -- == 4; a=j!=6 | k++ == 6 && j-- >=0;
     */

    
     /* int a = 2, b=3,c=4,x=0,y=0,z=0; 
      b+=c; 
      a+=b; 
      c=b-c; 
      b=b-c;
      b*=c*(a-b-c);
      
      x=a; y=b; z=c;
      System.out.println(x+"/"+y+"/"+z);
      
     
/**4
    int a, c;
    int b = 4;
    c = 8;
    a = b++ + --c;

    if (a != b) {
      int d = 3;
      a = 2 * d;
    }
    a += b;
    **/
  }
  
  public static Class<Integer> typeof(final int expr) {
    return Integer.TYPE;
  }
  public static Class<Byte> typeof(final byte expr) {
    return Byte.TYPE;
  }
  public static Class<Short> typeof(final short expr) {
    return Short.TYPE;
  }
  public static Class<Boolean> typeof(final boolean expr) {
    return Boolean.TYPE;
  }
  public static Class<Double> typeof(final double expr) {
    return Double.TYPE;
  }
  public static Class<Float> typeof(final float expr) {
    return Float.TYPE;
  }
  public static Class<Long> typeof(final long expr) {
    return Long.TYPE;
  }

}
