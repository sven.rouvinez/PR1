package s02;

public class Ex0202 {
  public static void main(String[] args) {
    int j = 7, k = 4;
    boolean a = true, b = false;

    a = 7 >= 3 | (7 != 3);
    b = a != true;
    a ^= true;
    
    /**
     * + 11G
     * == 8G
     * | 5G
     * &= 1D
     */
    //a &= false | b | 7 == 3 + 4;
    a &= (false | b | (7 == (3 + 4)));
    
    
    b = k++ >= 3 || j-- == 4;
    
    /**Précedence
     * ++,-- 14D
     * >= 9G
     * ==,!= 8G
     * | 5G
     * && 4G
     * = 1D
     */
    
    a = j != 6 | k++ == 6 && j-- >= 0;
   // a = (((j != 6) | ((k++) == 6))) && ((j--) >= 0);
    System.out.println(a+"   "+b);

  }
}
