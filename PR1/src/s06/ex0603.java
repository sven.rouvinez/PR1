package s06;

public class ex0603 {
  public static void main(String[] args) {
    int a = 12;
    int b = 13;
    int c = 14;
    int d = 15;

    System.out.println("Max a,b: " + isMax(a, b));
    System.out.println("Max a,b,c,d: " + isMax(a, b, c, d));
  }

  public static int isMax(int a, int b) {
    // retourne la valeur max entre a et b
    return a < b ? b : a;
  }

  public static int isMax(int a, int b, int c, int d) {
    // retourne la valeur max entre 4 nombres
    // surcharge de la fonction isMax(int , int)

    // compare d'abord a, b, ensutie c, d puis compare le résultat des 2
    // comparaisons
    return isMax(isMax(a, b), isMax(c, d));
  }
}
