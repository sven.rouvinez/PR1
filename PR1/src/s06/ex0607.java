package s06;

import proglib.SimpleIO;

public class ex0607 {
  public static void main(String[] args) {
    
    System.out.println(approxPi(SimpleIO.readInt("M: ")));
  }
  
  public static double approxPi(int m) {
    // pi = 4 car sera ensuite multiplié par approx
    double pi = 4.0;
    double approx = 1.0;

    // i = 1 car approx == 1 et permet de faire une addition directement
    for (int i = 1; i <= m; ++i) {
      approx += -(1.0 / (4.0 * i - 1.0)) + (1.0 / (4.0 * i + 1.0));
    }

    return pi *= approx;
    
    
  }
}
