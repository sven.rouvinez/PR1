package s06;

import proglib.SimpleIO;

public class ex0605 {
  public static void main(String[] args) {
    

    System.out
        .println("PGDC: "
            + pgdc(SimpleIO.readInt("Nombre 1"), SimpleIO.readInt("Nombre 2")));

  }

  public static int pgdc(int num1, int num2) {
    int pgdc = 0;

    int min = min(num1, num2);
    int max = max(num1, num2);

    for (int i = 1; i < max; i++) {
      // si i % max et min == 0 et que le i est > que le pgdc précédent,
      // remplacement
      if (min % i == 0 && max % i == 0 && i > pgdc)
        pgdc = i;

    
    }
    return pgdc;
  }

  public static int min(int x, int y) {
    return x > y ? y : x;
  }

  public static int max(int x, int y) {
    return x > y ? x : y;
  }
}
