package s06;

import proglib.SimpleIO;

public class ex0604 {
  public static void main(String[] args) {
    int p = SimpleIO.readInt("P: ");
    // les valeurs ne peuvent pas être égale à p
    int pMinus = p - 1;
    int countPair = 0;
    // a doit être < que b et a,b > 0
    int a = 1, b = 2;

    //
    while (a < pMinus) {
      // arrête de la boucle si b atteint p-1 ou a > b car remplit plus 1 cond.
      for (b = 2; b < pMinus; b++) {
        if (printPair(a, b, p))
          countPair++;

      }
      a++;
    }
    System.out.println("Number of pairs for " + p + " are " + countPair);
  }

  //retourne boolean pour compter les paires valides
  public static boolean printPair(int a, int b, int p) {
    // a<p et b<p
    if (isMin(a, b) && isMin(b, p)) {

      // math.pow retourne double donc cast
      int calc = (int) (Math.pow(a, 2) + Math.pow(b, 2) + 1);
      // attention a ou b > 0;
      int multipli = a * b;

      if (calc % multipli == 0) {

        System.out.println(a + "," + b);
        return true;
      }
    }
    return false;

  }

  // retourne si num1 < que num2
  public static boolean isMin(int num1, int num2) {
    return num1 < num2 ? true : false;
  }
}
