package s06;

import proglib.SimpleIO;

public class ex0606 {
  public static void main(String[] args) {
    System.out.println(sumDigit(SimpleIO.readInt("Nombre: ")));
  }

  public static int sumDigit(int number) {
    int numberSum = 0;
    
    // s'arrête lorsque le chiffre aura été complétement divisé
    while (number > 0) {
      // ajoute la valeur du modulo à la somme
      numberSum += number % 10;
      // divise le nombre pour enlever 1 unité
      number /= 10;

    }

    return numberSum;
  }
}
