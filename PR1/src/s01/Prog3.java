package s01;

import proglib.*;

public class Prog3 {
  public static void main(String[] args) {
    int n1, n2, gcd, k;
    n1 = SimpleIO.readInt("Entier 1: ");
    n2 = SimpleIO.readInt("Entier 1: ");
    gcd = 1;
    k = 1;

    while (k <= n1 && k <= n2) {
      if (n1 % k == 0 && n2 % k == 0)
        gcd = k;
      k++;

    }
    
    SimpleIO.display("PCDC: " ,gcd);
  }
}
