package s01;

public class SecondProgram {
  public static void main(String[] args) {
     int length, width, area;
     
     length = 8;
     width = 6;
     area = length * width;
     
     System.out.println("Rect. len:" + length);
     System.out.println("Rect. wid:" + width);
     System.out.println("Rect. are:" + area);
     System.out.println();
     
     length = length * 2;
     width = width *2;
     area = length * width;
     
     System.out.println("Rect. len:" + length);
     System.out.println("Rect. wid:" + width);
     System.out.println("Rect. are:" + area);
     System.out.println();
     
  }
}
