//==============================================================================
//  HEIA-FR / Rudolf Scheurer
//==============================================================================

package s15;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import proglib.SimpleIO;

public class Ex1502 {

  // ----------------------------------------------------------------------------
  public static void main(String[] args) {
    String fileName = "";

    if (args.length != 1) {
      fileName = SimpleIO.fileChooser(".");
    }
    try {
       fileName= args[0];
      makeLetterStatistics(fileName);
    } catch (IOException e) {
      System.out.println("something went wrong...");
    }
  }

  // ----------------------------------------------------------------------------
  public static void makeLetterStatistics(String filename) throws IOException {

    Scanner inStream = new Scanner(
        new BufferedReader(new FileReader(filename)));

    String line = "";
    int[] counterChar = new int[26];
    int sumCarac = 0;
    int numberOfApp=0;
    double moyenne = 0.0;
    while (inStream.hasNextLine()) {
      line = inStream.nextLine();

      Scanner inLine = new Scanner(line);
      inLine.useDelimiter("[\\s]+");
      while (inLine.hasNext()) {
      String word = inLine.next().toUpperCase();

      for (int i = 0; i < word.length(); ++i) {
        for (char carac = 'A'; carac <= 'Z'; carac++) {
          char caracWord = word.charAt(i);
          if (carac == caracWord) {
              sumCarac++;
            counterChar[carac - 65]++;
            break;
          }
        }
        }
      }

    }

    // Close file
    inStream.close();

    // Print statistics

      for (char val = 'A'; val <= 'Z'; val++) {
      numberOfApp = counterChar[val - 65];
      moyenne = ((double) numberOfApp / (double) sumCarac) * 100;
        System.out
          .println(String.format("Number of %s: %d - %.3f %%", val,
              numberOfApp, moyenne));
      }


  }
}
