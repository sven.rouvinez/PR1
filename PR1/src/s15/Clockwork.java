
package s15;

public class Clockwork {
  private int h, m, s;

  public Clockwork(int h, int m, int s) {
    this.h = h;
    this.m = m;
    this.s = s;
  }

  public int getHours() {

    return h;
  }

  public int getMinutes() {

    return m;
  }

  public int getSeconds() {

    return s;
  }

  public void stepSecond() {
    s++;
    
  }

}
