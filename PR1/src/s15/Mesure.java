package s15;

public class Mesure {
  private double mesure;
  private double margin;

  public Mesure(double mesure, double margin) {

    this.mesure = mesure;
    this.margin = margin;
  }

  public double maxMesure() {

    return mesure + margin;
  }

  public String toString() {

    return String.format("%.3f [+- %.3f]", mesure, margin);
  }

  public double modifyMargin(int k) {

    return this.margin * k;
  }

  public void multiply(Mesure m) {
    mesure += m.mesure;
    margin += m.margin;
  }

  public boolean isVIncluded(double v) {
    double mesureMax = maxMesure();
    double mesureMin = mesure - margin;

    if (v < mesureMin && v > mesureMax)
      return false;

    return true;
  }

}
