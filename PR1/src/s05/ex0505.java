package s05;

public class ex0505 {
  public static void main(String[] args) {
    int borneMin = 50;
    int borneMax = 1000;
    int maxIter = 10;
    int iter = 0; // permet de s'arrêter à un nombre défini d'itérations dans la
                  // boucle

    boolean div5;
    boolean div6;

    // boucle commence à la borne min pour aller au max
    for (int chiffreDiv = borneMin; chiffreDiv <= borneMax; chiffreDiv++) {
      div5 = chiffreDiv % 5 == 0;
      div6 = chiffreDiv % 6 == 0;

      // contrôle si le max d'itérations a été atteint
      // if (iter < maxIter) {
      // si le chiffre est divisible par 6 et 5 alors affiche et incrémente
      // itération
      if (div5 && div6) {
        System.out.print(chiffreDiv);
        iter++;

        // si iter <= 9, sépare les chiffres par une virgule. évite une
        // virgule sur le dernier nombre
        if (iter <= maxIter - 1 || iter % 10 != 0) {
          System.out.print(",");

        } else {
          System.out.println(" ");
        }
        // }
      }
    }
  }
}