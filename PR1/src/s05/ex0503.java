package s05;

public class ex0503 {
  public static void main(String[] args) {

    double eApprox = 1.0;
    double n = 1.0; // >=1 sinon division par 0
    int iter = 1; // compte le nombre d'itérations
    double incVal = 1.0; // valeur ajoutée à e pour chaque itération
    double approxMax = 1.0e-12; // valeur incrément maximal voulue

    // s'arrête dès que la valeur de l'incrément est plus petit que 1*10^-12
    while (incVal > approxMax) {

      n *= iter; // fractionnelle multiplie n par le nombre
      incVal = 1.0 / n; // division par n!
      eApprox += incVal; // ajout du résultat de la division à e
      iter++;

    }
    // printf permet de formatter l'affichage, ici il affiche 16 chiffre après
    // la virgule
    System.out
        .printf(" e= %.16f" + "\n" + "Nombre iter:" + iter, eApprox);

  }
}

