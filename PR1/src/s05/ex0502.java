package s05;

import proglib.SimpleIO;

public class ex0502 {

  public static void main(String[] args) {

    int dents = SimpleIO.readInt("Nombre dents?");
    int hauteur = SimpleIO.readInt("Hauteur?");

    for (int k = 1; k <= dents; k++) { // affiche les dents
      for (int i = 0; i < hauteur; ++i) { // affiche la longueur d'une dent

        for (int j = 0; j <= i; ++j) { // affiche la dent

          System.out.print("*");

        }
        System.out.println("\r");

      }

    }
  }

}
