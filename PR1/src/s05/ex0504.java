package s05;

import proglib.SimpleIO;

public class ex0504 {
  public static void main(String[] args) {

    int nombre = SimpleIO.readInt("Nombre >1");
    int diviseur = 2; // premier nombre premier
    int iter = 0;
    System.out.print(nombre + " = ");

    // si le nombre est < que 2 c'est qu'on a divisé ce nombre complètement
    while (nombre >= 2) {

      // boucle jusqu'à trouver le nombre premier et affiche
      while (nombre % diviseur == 0) {


        nombre /= diviseur;
        // affiche pas le "x" si 1ère itération
        if (iter == 0) {
          System.out.print(diviseur);
        } else {
          System.out.print(" x " + diviseur);
        }
        iter++;
      }
      // incrémente le diviseur dès que le nombre à été divisé
      diviseur++;

    }

  }
}
