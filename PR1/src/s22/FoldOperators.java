
package s22;

public class FoldOperators {
  // TODO ...
  
  // ***********************************
  // ****** With internal classes ******
  // ***********************************

 // public static final IFoldableOperation SUM = new SumOperation();

 

  
  //public static final IFoldableOperation CONCAT=...
  
  //private static class SumOperation ...

  //private static class ConcatOperation ...
  
  
  
  // ************************************
  // ****** With anonymous classes ******
  // ************************************

  //public static final IFoldableOperation SUM1=...
  
  //public static final IFoldableOperation CONCAT1=...

}