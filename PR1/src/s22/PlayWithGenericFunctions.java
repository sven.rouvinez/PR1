
package s22;

public class PlayWithGenericFunctions {
  // =====================================
  public interface IFoldableOperation<T> {
    T initialValue();

    T combine(T accumulated, T newValue);
  }

  // =====================================
  public interface ISequence<T> {
    boolean hasMoreValues();

    T nextValues();
  }

  // =====================================
  public interface IFunction<T> {
    T valueAt(T x);
  }

  // =====================================
  static class MySeriesFct implements IFunction<Double> {
    @Override
    public Double valueAt(Double i) {
      return i / Math.pow(2.0, i);
    }
  }

  static class MyTrigonometricFct implements IFunction<Double> {
    @Override
    public Double valueAt(Double x) {
      return Math.sin(x) * Math.sin(x) * Math.cos(x);
    }
  }

  // =================================================
  static class MyFunctionEx2a implements IFunction<Double> {
    @Override
    public Double valueAt(Double x) {
      return x / Math.pow(2, x);
    }
  }

  // =================================================
  public static void ex2a() {
    IFunction<Double> f;
    // **** with internal class ***
    f = new MyFunctionEx2a();
    System.out.println(f.valueAt(3.0));

    // **** with anonymous class ***
    f = new IFunction<Double>() {
      @Override
      public Double valueAt(Double x) {
        return x / Math.pow(2, x);
      }
    };
    System.out.println(f.valueAt(3.0));

    // **** with lambda expression ***
    f = (x) -> x / Math.pow(2, x);
    System.out.println(f.valueAt(3.0));
  }

  static <T> T foldLeft(ISequence<T> ns, IFoldableOperation<T> op) {
    T res = op.initialValue();
    while (ns.hasMoreValues())
      res = op.combine(res, ns.nextValues());
    return res;
  }

  private static void computeAll(IFunction<Double> f) {
    // INumberSequence nsArray=... 1.2, 3.4, 5.6
    // INumberSequence nsSeries= fonction f entre 0 et 20

    // System.out.println(... le produit sur nsArray
    // System.out.println(... la somme sur nsSeries
  }

  public static void main(String[] args) {
    ex2a();

    // *******************************************
    // ****** With internal static classes: ******
    // *******************************************

    IFunction<Double> f = new MySeriesFct();
    computeAll(f);

    // *************************************
    // ****** With anonymous classes: ******
    // *************************************

    f = new IFunction<Double>() {
      @Override
      public Double valueAt(Double i) {
        return i / Math.pow(2.0, i);
      }
    };
    computeAll(f);

    // **************************************
    // ****** With lambda expressions: ******
    // **************************************

    f = (i) -> i / Math.pow(2.0, i);
    computeAll(f);
  }
}
