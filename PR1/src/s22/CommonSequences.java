
package s22;
import s22.PlayWithGenericFunctions.IFunction;
import s22.PlayWithGenericFunctions.ISequence;

public class CommonSequences {
  // ***********************************
  // ****** With internal classes ******
  // ***********************************
  
  public static <T> ISequence<T> fromArray(T[] t) {
    return null; // TODO ... new ArraySequence...
  }
  
  public static ISequence<Double> fromSeries(IFunction<Double> f, int from, int to) {
    return null; // TODO ... return new SeriesNumberSequence...
  }
  
  
  // static class ArrayNumberSequence ... // TODO ...
  
  // static class SeriesNumberSequence ... // TODO ...
  
  
  
  // ************************************
  // ****** With anonymous classes ******
  // ************************************
  
  public static <T> ISequence<T> fromArray1(final T[] t) {
    return null; // TODO ...
  }

  public static ISequence<Double> fromSeries1(final IFunction<Double> f, 
                                              final int from, final int to) {
    return null; // TODO ...
  }
  
}
