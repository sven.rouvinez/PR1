//==============================================================================
//  HEIA-FR
//==============================================================================
package s18;

public class Ex1807 {
  static class A { // extends Object
    public A()      {}
    public A(int i) {}
    public String m() { return "A.m()"; }
    public String p() { return "A.p()"; }
  }
  //==========================================
  static class B extends A {
    public String m()      { return "B.m()"; }
    public String p(int i) { return "B.p()"; }
  }
  //==========================================
  public static void main(String[] args) {
    Object a = new A();
    Object b = new B();
    
//    System.out.println("7a " + ((B)a).m()   );
//    System.out.println("7b " + ((A)a).m()   );
//    System.out.println("7c " + ((B)b).m()   );
//    System.out.println("7d " + ((A)b).m()   );
//    System.out.println("7e " + b.m()        );
//    System.out.println("7f " + ((B)b).p(5)  );
//    System.out.println("7g " + ((A)b).p(5)  );
//    System.out.println("7h " + ((B)b).p()   );
//    System.out.println("7i " + ((A)b).p()   );
//    System.out.println("7j " + b.p(5)       );
//    System.out.println("7k " + new B(5).m() );
  }
}
