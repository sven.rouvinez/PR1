//==============================================================================
//  HEIA-FR
//==============================================================================
package s18;

public class Pencil {
  // state true if down
  private boolean state;

  public Pencil() {
    this.state = true;
  }
  
  public void raise() {
    this.state = false;
  }

  public void drop() {
    this.state = true;
  }

  public boolean isDown() {

    return state;
  }
}
