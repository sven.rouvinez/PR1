//==============================================================================
//  HEIA-FR
//==============================================================================
package s18;

public class Plotter extends Pencil {
  private int    x, y;
  private double totalLength;

  public Plotter() {
    this.x = 0;
    this.y = 0;
    this.totalLength = 0.0;
  }
  
  public void move(int dx, int dy) {
    if (isDown()) {
      this.totalLength += Math.abs(dx) + Math.abs(dy);
    }
    this.x += dx;
    this.y += dy;

  }

  public int xCoord() {

    return x;
  }

  public int yCoord() {

    return y;
  }

  public double strokeLength() {    // Total movements with pencil down
    return totalLength;
  }
}
