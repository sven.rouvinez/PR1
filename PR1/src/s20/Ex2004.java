package s20;

import java.io.File;

public class Ex2004 {

  public static void main(String[] args) {
    // retrieve all files if the end with jpg gif png
    String files[] = new File(".").list((dir, name) -> name.endsWith("jpg")
        || name.endsWith("gif") || name.endsWith("png"));
    

    for (String f : files)
      System.out.println(f);
 
  }
}
