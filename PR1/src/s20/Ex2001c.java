package s20;

public class Ex2001c {

  // For comments see Ex2001a
  // ======================================
  static class Entity {
    private String               value;
    private ValueChangedListener listener;

    public Entity() {
    }

    public String getValue() {
      return value;
    }

    public void setValue(String value) {
      this.value = value;

      if (this.listener != null) {
        this.listener.onValueChanged(this);
      }
    }

    public void setListener(ValueChangedListener listener) {
      this.listener = listener;
    }
  }

  // ======================================
  public interface ValueChangedListener {
    void onValueChanged(Entity e);
  }

  // ======================================
  public static void main(String[] args) {

    Entity entity = new Entity();
    ValueChangedListener listener = (e) -> System.out
        .println("An entity changed its value to: " + e.value.toString());

    entity.setValue("abcd");
    entity.setListener(listener);
    entity.setValue("toto");
    entity.setValue("titi");

  }

}
