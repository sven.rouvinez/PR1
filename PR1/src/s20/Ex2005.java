package s20;

import java.util.Random;
import java.util.function.DoubleSupplier;

public class Ex2005 {
  private static Random rnd = new Random();

  static double randomValue(double[] t) {
    return t[rnd.nextInt(t.length)];
  }

  static double randomValue(DoubleSupplier[] t) {
    // call the function by the reference method
    return t[rnd.nextInt(t.length)].getAsDouble();
  }

  static double hardComputation1() {
    double res = 0.0;
    for (int i = 1; i < 10_000_000; i++) {
      res += Math.sin(Math.sqrt(Math.sqrt(Math.PI / i)));
    }
    System.out.println("Called HardComputation1");
    return res;
  }

  static double hardComputation2() {
    double res = 0.0;
    for (int i = 1; i < 10_000_000; i++) {
      res += Math.atan(Math.pow(Math.PI / i, Math.sqrt(i)));
    }
    System.out.println("Called HardComputation2");
    return res;
  }

  static double hardComputation3() {
    System.out.println("Called HardComputation3");
    return Math.log(hardComputation1() + hardComputation2());
  }

  public static void main(String[] args) {
    System.out.println("start");

    // Use the interface doubleSupplier to create a reference
    // call the function only if there are chosen
    DoubleSupplier[] t = { Ex2005::hardComputation1, Ex2005::hardComputation2,
        Ex2005::hardComputation3 };
    

    double d = randomValue(t);


    System.out.println(d);
  }

}
