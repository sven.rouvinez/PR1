package s20;

import java.io.PrintStream;
import java.io.StringWriter;

public class Ex6WriteAgain {

  // Create an interface to use once the write method
  @FunctionalInterface
  public interface WriterInterface {
    public void write(int v);
  }

  static void writeItSeveralTimes(int howMany, int v, Object o) {
    while (howMany-- > 0) {
      if (o instanceof PrintStream)
        ((PrintStream) o).write(v);

      else if (o instanceof StringWriter)
        ((StringWriter) o).write(v);
    }
  }

  // arguments type WriterInterface to use functionnal interface
  static void callItSeveralTimes(int howMany, int v, WriterInterface obj) {

    while (howMany-- > 0) {
      // use the write method from the type of object who is given by the
      // parameters
      obj.write(v);
    }
  }

  public static void main(String[] args) {
    PrintStream ps = System.out;
    StringWriter pw = new StringWriter();
    writeItSeveralTimes(10, 65, ps);
    writeItSeveralTimes(10, 65, pw);
    System.out.println();
    System.out.println(pw);

    System.out.println("Reference methods");
    // create a new object of pw to erase value
    pw = new StringWriter();

    // reference the functionnal interface write and pass it to
    // callItSeveralTimes, the object gonna be independant from the type
    callItSeveralTimes(10, 65, pw::write);
    callItSeveralTimes(10, 65, ps::write);
    System.out.println();
    System.out.println(pw);
  }

}
