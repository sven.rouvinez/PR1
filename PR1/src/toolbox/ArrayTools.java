package toolbox;

public class ArrayTools {
  public static void printArray(int[][] array) {
    for (int i = 0; i < array.length; i++) {
      printArray(array[i]);
      System.out.println();
    }

  }

  // affiche un tableau 1D
  public static void printArray(int[] array) {
    for (int val : array) {
      System.out.print(val + " ");

    }
  }

  public static boolean isRectugular(boolean[][] array2D) {
    return array2D.length < array2D[0].length ? true : false;
  }
}
