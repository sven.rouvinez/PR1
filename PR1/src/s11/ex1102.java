package s11;

public class ex1102 {
  public static void main(String[] args) {
    int[][] t1 = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 } };
    int[][] t2 = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 } };

    printArray(sumTab2D(t1, t2));
  }

  public static void printArray(int[][] array) {
    for (int i = 0; i < array.length; i++) {
      printArray(array[i]);
      System.out.println();
    }

  }

  // affiche un tableau 1D
  public static void printArray(int[] array) {
    for (int val : array) {
      System.out.print(val + " ");

    }
  }

  public static int[] sumTab2D(int[][] t1, int[][] t2) {
    int dimension = t1.length * t1[0].length;
    int[] sumTab = new int[dimension];

    int indexTabSum = 0;

    for (int i = 0; i < t1.length; ++i) {

      for (int j = 0; j < t1[i].length; ++j) {
        sumTab[indexTabSum] = t1[i][j] + t2[i][j];
        indexTabSum++;
      }

    }

    return sumTab;
  }
}
