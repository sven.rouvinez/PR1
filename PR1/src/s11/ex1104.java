package s11;

public class ex1104 {
  public static void main(String[] args) {
    boolean[][] array1 = { { true, true, true, false, true, false },
        { true, true, false, true, true } };
    printArray(returnLine(array1, 1));
    System.out.println("---------------");
    printArray(returnCol(array1, 2));
  }

  public static boolean[] returnLine(boolean[][] array2D, int k) {
    if (isRectugular(array2D))
      return null;

    boolean[] array = new boolean[array2D[k].length];

    for (int i = 0; i < array2D[k].length; ++i) {
      array[i] = array2D[k][i];
    }
    return array;

  }

  public static boolean[] returnCol(boolean[][] array2D, int k) {
    if (isRectugular(array2D))
      return null;

    boolean[] array = new boolean[array2D.length];

    for (int i = 0; i < array2D.length; ++i) {
      array[i] = array2D[i][k];
    }
    return array;
  }

  public static boolean isRectugular(boolean[][] array2D) {
    return array2D.length < array2D[0].length ? false : true;
  }

  public static void printArray(boolean[][] array) {
    for (int i = 0; i < array.length; i++) {
      printArray(array[i]);
      System.out.println();
    }

  }

  // affiche un tableau 1D
  public static void printArray(boolean[] array) {
    for (boolean val : array) {
      System.out.print(val + " ");
    }
    System.out.println();
  }
}
