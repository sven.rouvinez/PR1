package s11;

public class ex1107 {
  public static void main(String[] args) {
    printArray(storeArray(1000));
  }

  public static int toOne(int n) {
    int opOdd = 0;
    int opPair = 0;

      if(n%2==1) {
        opOdd = 3 * n + 1;

        n = opOdd;
      } else {
        opPair = n / 2;

        n = opPair;
      }

    return n;
  }

  public static int[][] storeArray(int p) {
    int[][] nStore = new int[p][3];
    /*
     * nStore[0][0] = 1; nStore[0][1] = 0; nStore[0][2] = 1;
     */
    for (int i = 1; i <= nStore.length; ++i) {
      int nMax = 1;
      int counter = 0;



      for (int calcP = i; calcP > 1; calcP = toOne(calcP)) {
        nMax = max(calcP, nMax);
        counter++;
      }
      nStore[i - 1][0] = i;
      nStore[i - 1][1] = counter;
      nStore[i - 1][2] = nMax;

    }

    return nStore;

  }

  public static int max(int a, int b) {
    return a > b ? a : b;
  }

  public static void printArray(int[][] array) {
    for (int i = 0; i < array.length; i++) {
      printArray(array[i]);
      System.out.println();
    }

  }

  // affiche un tableau 1D
  public static void printArray(int[] array) {
    for (int val : array) {
      System.out.print(val + " ");

    }
  }
}
