package sandbox;

public class Ex2 {
  public interface IFunction {
    double functionOf(double x, int i);
  }

  public static double computerSerie(IFunction f, double x, int from, int to) {
    double val = 0.0;
    for (; from <= to; from++) {
      val += f.functionOf(x, from);
    }

    return val;
  }

  public static double factiorial(int n) {
    double val = 1.0;

    for (int i = 2; i <= n; ++i)
      val *= i;

    return val;
  }

  public static double e(double xValue, int n) {
    return computerSerie(new IFunction() {
      @Override
      public double functionOf(double x, int i) {
        return Math.pow(x, i) / factiorial(i);
      }
    }, xValue, 0, n);

  }

  public static double eLambdas(double xValue, int n) {
    return computerSerie((x, y) -> (Math.pow(x, y) / factiorial(y)),
        xValue, 0, n);

  }

  public static void main(String[] args) {

    System.out.println(e(1.02, 19));
    System.out.println(eLambdas(1.02, 19));

  }
}
