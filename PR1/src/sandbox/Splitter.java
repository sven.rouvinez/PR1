package sandbox;

public class Splitter {
  private int  returnAfter;
  private char symbol;

  public Splitter(int returnAfter) {
    this.returnAfter = returnAfter;
    this.symbol = ' ';
  }

  public Splitter(int returnAfter, char symbol) {
    this.returnAfter = returnAfter;
    this.symbol = symbol;
  }

public static Splitter normalSplitter(int width){
    return new Splitter(width);
  }

  public static Splitter prettySplitter(int widht, char c) {
  return new Splitter(widht,c);
}

  public String split(String s) {
    StringBuilder newString = new StringBuilder();
    if (symbol != ' ') {
      for (int i = 0; i < returnAfter; ++i) {
        newString.append(symbol);
      }
    }
    for (int i = 0; i < s.length(); ++i) {
      if (i % this.returnAfter == 0)
        newString.append("\n");

      newString.append(s.charAt(i));
    }
    if (symbol != ' ') {
      for (int i = 0; i < returnAfter; ++i) {
        newString.append(symbol);
      }
    }
    return newString.toString();

  }

  public static void main(String[] args) {
    Splitter s1, s2;
    s1 = Splitter.prettySplitter(5, '*');
    String m = "adbcdefghijkl";

    System.out.println(s1.split(m));
  }
}
