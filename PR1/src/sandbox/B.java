package sandbox;

public class B extends A {
  private boolean isGood;
  private int     size;

  B() {
    System.out.println("B()");
  }

  B(int size) {
    this();
    this.size = size;
    System.out.println("B(int)");

  }

  B(boolean isGood) {
    this.isGood = isGood;
    System.out.println("B(boolean)");

  }

  B(boolean isGood, int size) {
    this(size);
    this.isGood = isGood;
    System.out.println("B(boolean,int)");
  }

}
