package sandbox;

public class SockWorker {
  public static void washAllAndTidy(Sock[] t) {
    for (int i = 0; i < t.length; ++i) {
      if (!(t[i].isClean()))
        t[i].isClean();

      if (i + 1 == t.length)
        break;
      try {
        t[i].pairWithSocks(t[i + 1]);
      } catch (Exception e) {
        System.out.println(e);
      }
    }
  }

  public static void main(String[] args) {
    Sock[] t = { new Sock(), new Sock(), new Sock() };

    SockWorker.washAllAndTidy(t);

  }
}
