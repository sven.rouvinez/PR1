package sandbox;

public class Ex4 {
  private Object[] tab;

  Ex4(Object[] t, boolean deep) {
    this.tab = t;
    int n = t.length;
    t[0] = this;

    if (deep) {
      tab = new Object[n];
      for (int i = 0; i < n; ++i) {
        this.tab[i] = t[i];
      }
    }

  }

  public static void main(String[] args) {
    Object[] u = new Object[3];

    Ex4 a1 = new Ex4(u, false);
    Ex4 a2 = new Ex4(a1.tab, true);
    Ex4 a3 = new Ex4(a2.tab, false);

    a1.tab[2] = a1;
    a2.tab[2] = a2;
    a3.tab[2] = a3;

  }

}
