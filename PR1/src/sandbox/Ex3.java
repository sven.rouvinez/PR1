package sandbox;

public class Ex3 {
  public static void main(String[] args) {
    char[] c1 = { '2', '3', '4' };
    char[] c2 = { '2', '3', '4', '5' };
    char[] c3 = new char[] { '2', '3', '4' };
    String s1 = "234";
    String s2 = new String("2345");
    String res = "";

    if (c1 == c2)
      res += "A";
    if (c1 == c3)
      res += "B";
    if (s1.equals("23" + "4"))
      res += "C";
    c3 = c1;
    if (c1[2] == c2[2])
      res += "D";
    if (c1 == c3)
      res += "E";
    if (s1.length() == c1.length)
      res += "F";
    if (s2.charAt(1) == c1[1])
      res += "G";
    if (c2[2] - c2[0] == c2[3] - c2[1])
      res += "H";
    if (s1 == c1.toString())
      res += "I";
    String s3 = new String(s2);
    if (s2 == s3)
      res += "J";
    c3[2] = '6';
    if (c1[2] == '4')
      res += "K";
    System.out.println(res);
  }
}
