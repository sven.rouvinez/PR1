package sandbox;

public class Sock {
  private boolean clean;
  private Sock    pair;

  public Sock() {
    this.clean = true;
    this.pair = null;
  }

  public boolean isClean() {
    return this.clean;
  }

  public void use() {
    this.clean = false;
    if (this.clean && this.pair.clean) {
      this.pair.use();

    }
  }

  public void pairWithSocks(Sock s) throws Exception {
    if (this.pair != null)
      throw new Exception("already paired");
    this.pair = s;
  }

  public void unpair() throws Exception {
    if (this.pair == null)
      throw new Exception("no paired socks");
    this.pair = null;
  }

  public void wash() throws Exception {
    if (this.clean == true)
      throw new Exception("already cleaned");

    this.clean = true;
  }
}
