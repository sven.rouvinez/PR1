
package s21;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class Ex2100 {

  // ------ Ex. 1 -----------
  public static void printSomePowersOfTwo() {
    // generate a range from 0 to 10 and apply a power 2 on number and print
    // them
    LongStream.range(0, 11).mapToDouble((n) -> 1 << n)
        .forEach(System.out::println);

  }

  // ------ Ex. 2 -----------
  public static String macAddress(int[] t) {
    // from an array of int, apply on each object a conversion to hexa and join
    // them with a :
    return Arrays.stream(t).mapToObj((o) -> String.format("%02X", o))
        .collect(Collectors.joining(":"));
  }

  // ------ Ex. 3 -----------

  public static long[] divisors(long n) {
    // generate a range from 1 to n and test if the obj is a divisor of n, and
    // convert to an array
    return LongStream.range(1, n).filter((div) -> (n % div == 0)).toArray();
  }

  // ------ Ex. 4a -----------
  public static long sumOfDivisors(long n) {
    // sum the value in the array
    return Arrays.stream(divisors(n)).sum();
  }

  // ------ Ex. 4b -----------
  public static long[] perfectNumbers(long max) {
    // generate a range from 1 to max and test if the value is equals to the sum
    // of it divisors and convert it to an array
    return LongStream.range(1, max).filter((n) -> n == sumOfDivisors(n))
        .toArray();
  }

  // ------ Ex. 5 -----------
  public static void printMagicWord() throws IOException {
    // get the file
    Path path = FileSystems.getDefault().getPath("wordlist.txt");

    Files.lines(path)
        // affiche le mot s'il y a 11 lettres
        .filter(x -> x.length() == 11)
        // si caractère à la place 3 est 't'
        .filter(x -> x.charAt(2) == 't')
        // si caractère à la place 5 est 'l'
        .filter(x -> x.charAt(4) == 'l')
        // si le nombre de lettres distincts est égale à 6
        .filter(x -> x.chars().distinct().count() == 6)
        .forEach(System.out::println);
  }

  public static boolean isPalindrom(String str) {
    // if the string reversed is equals to the normal string
    return str.equals(new StringBuilder(str).reverse().toString());
  }

  public static void printPalindroms() throws IOException {
    Path path = FileSystems.getDefault().getPath("wordlist.txt");
    // test if the obj (word) is a palindrom from the file
    Files.lines(path).filter((str) -> isPalindrom(str))
        .forEach(System.out::println);
  }

  // ------ Ex. 6 -----------
  public static void moyennes() {
    double resultats[][] = { { 9, 10, 8, 5, 9 }, { 5, 9, 9, 8, 8 },
        { 4, 8, 10, 9, 5 }, { 8, 10, 8, 10, 7 }, { 8, 9, 7, 10, 6 }, };

    double[] notes = new double[resultats.length];
    for (int i = 0; i < resultats.length; i++) {
      double sum = 0;
      for (int j = 0; j < resultats[i].length; j++) {
        sum += resultats[i][j];
      }
      notes[i] = 1.0 + sum / 10.0;
    }
    double sum = 0;
    for (int i = 0; i < notes.length; i++) {
      sum += notes[i];
    }
    double moyenne = sum / notes.length;

    System.out.printf("Notes  : %s%n", Arrays.toString(notes));
    System.out.printf("Moyenne: %.2f%n", moyenne);
  }

  public static void moyennesWithStreams() {
    double resultats[][] = { { 9, 10, 8, 5, 9 }, { 5, 9, 9, 8, 8 },
        { 4, 8, 10, 9, 5 }, { 8, 10, 8, 10, 7 }, { 8, 9, 7, 10, 6 }, };

    // open a stream on the first dimension, and on the 2nd dimension apply the
    // formula and convert it to double and store it in a array
    double[] notes = Arrays.stream(resultats)
        .mapToDouble((i) -> (1.0 + Arrays.stream(i).sum() / 10.0)).toArray();
    // compute all the average notes from the students
    double moyenne = Arrays.stream(notes).average().getAsDouble();

    System.out.printf("Notes : %s%n", Arrays.toString(notes));
    System.out.printf("Moyenne: %.2f%n", moyenne);
  }

  // ------ Ex. 7 -----------
  public static void sapin(int n) {
    System.out.println(IntStream.range(0, n)
        .mapToObj(i -> IntStream.range(i, i + 4)
            .mapToObj(j -> new String(new char[n + 2 - j]).replace("\0", " ")
                + new String(new char[1 + 2 * j]).replace("\0", "*"))
            .collect(Collectors.joining("\n")))
        .collect(Collectors.joining("\n")));
  }

  public static void sapinWithoutStream(int n) {
    for (int i = 0; i < n; ++i) {
      for (int j = i; j < (i + 4); ++j) {
        for (int k = 0; k < (n + 2 - j); ++k) {
          System.out.print(" ");
        }

        for (int k = 0; k < (1 + 2 * j); ++k) {
          System.out.print("*");
        }
        System.out.println();
      }
    }
  }

  // -------------------------------------------------------
  public static void main(String[] args) {

    System.out.println("++++++ Ex. 1 ++++++");
    printSomePowersOfTwo();

    System.out.println("++++++ Ex. 2 ++++++");
    System.out.println(macAddress(new int[] { 78, 26, 253, 6, 240, 13 }));
    System.out.println("++++++ Ex. 3 +++++++++++");
    System.out.println(Arrays.toString(divisors(496L)));
    System.out.println("++++++ Ex. 4a +++++++++++");
    System.out.println(sumOfDivisors(496L));
    System.out.println("++++++ Ex. 4b +++++++++++");
    System.out.println(Arrays.toString(perfectNumbers(10000)));
    System.out.println("++++++ Ex. 4 facultatif +++++++++++");
    // System.out.println(Arrays.toString(perfectNumbers(33550337)));

    try {
      System.out.println("++++++ Ex. 5a +++++++++++");
      printMagicWord();
      System.out.println("++++++ Ex. 5b +++++++++++");
      printPalindroms();
    } catch (IOException e) {
      System.out.println("!! Problem when reading file... " + e.getMessage());
    }

    System.out.println("++++++ Ex. 6 +++++++++++");
    moyennes();
    System.out.println("++++++ With Stream +++++++++++");
    moyennesWithStreams();

    System.out.println("++++++ Ex. 7 Facultatif +++++++++++");
    sapinWithoutStream(4);
  }

}
