//==============================================================================
//  HEIA-FR / Jacques Bapst
//==============================================================================

package s03;

import proglib.SimpleIO;

public class Ex0304 {

 
  public static void main(String[] args) {
    int a, b, c;

    if (args.length == 3) {
      a = Integer.parseInt(args[0]);
      b = Integer.parseInt(args[1]);
      c = Integer.parseInt(args[2]);
    } else {
      a = SimpleIO.readInt("Valeur de a");
      b = SimpleIO.readInt("Valeur de b");
      c = SimpleIO.readInt("Valeur de c");
    }

    System.out.println(
        "Equation: (" + a + ")x\u00B2 + (" + b + ")x + (" + c + ") = 0");

    //si a = 0 fonction du 1er degré
    if (a == 0) {
      deg1(b, c);

    } else {
      deg2(a, b, c);
    }

  }

  public static void deg1(int b, int c) {

    if (b == 0 && c == 0) {
      System.out.println("Infinité de solution");

    } else if (b == 0 && c != 0) {
      System.out.println("Aucune solution");

    } else {
      //calcul fonction du 1er degré
      System.out.println(calcDeg1(b, c));
    }

  }

  public static double calcDeg1(int b, int c) {
    double solution = 0.0;
    //cast car solution peut être à rationnelle
    solution = (double)-b / (double)c;

    return solution;
  }

  public static void deg2(int a, int b, int c) {
    double x1 = 0;
    double x2 = 0;
    double xi = 0;
    double discriminant=Math.pow(b, 2) - 4 * a * c;


    if (discriminant > 0) {
      
      x1 = (((-(b) + (Math.sqrt(discriminant))) / (2 * a)));
      x2 = (((-b - (Math.sqrt(discriminant))) / (2 * a)));

      System.out.println("x1=" + x1 + " " + "x2=" + x2);
      
    } else if (discriminant == 0) {
      //discri. = 0 x1=x2
      x1 = x2 = -b / (2 * a);
      
      System.out.println("x1=x2=" + x1);
      
    } else if (discriminant < 0) {
      //calcul avec partie imaginaire
      //cast car peut donner des nombres rationnels
      x2 = x1 = (double)-b / (double)(2 * a);
      xi = Math.sqrt(-discriminant) / (2 * a);
      
      System.out.println("x1=(" + x1 + ")+(" + xi + ")i " + "x2=(" + x2 + ")-(" + xi+")i");
    }
  }
}
