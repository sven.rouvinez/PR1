package s03;

import proglib.SimpleIO;

public class Ex0305 {
  public static void main(String[] args) {
//Demander comment faire avec switch
    
    int n = SimpleIO.readInt("Nombre");

    boolean divi5 = (n % 5 == 0);
    boolean divi6 = (n % 6 == 0);
    
    System.out.println(n % 5);
    
    if (divi5 && divi6) {
      System.out.println(n + " divisible par 5 et 6");
    } else if (divi6) {
      System.out.println(n + " divisible par 6");
    } else if (divi5) {
      System.out.println(n + " divisible par 5");
    } else
      System.out.println(n + " non divisible par 5 et 6");

  }
}
