package s03;

import proglib.SimpleIO;

public class Ex0307 {

  public static void main(String[] args) {
    //Afin de simplifier le code 2 méthodes ont été créées
    System.out.println(WeekNameSwitch(SimpleIO.readInt("Switch Number of day week")));
    System.out.println(WeekNameIf(SimpleIO.readInt("If Number of day week")));
  }

  public static String WeekNameSwitch(int DayNumber) {
    String DayName = "";

    //Pas obligé de mettre des break pour chaque case car les jours de 1 à 5 et 6 à 7 sont tous dans le même "intervalle"
    switch (DayNumber) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
        DayName = "Work day";
        break;
      case 6:
      case 7:
        DayName = "Week-end";
        break;
      default:
        DayName = "Invalid number";
        break;
    }
    return DayName;
  }

  public static String WeekNameIf(int DayNumber) {
    String DayName = "";

    //Test des bornes
    if (DayNumber >= 1 && DayNumber <= 5) {
      DayName = "Work day";
    } else if (DayNumber >= 6 && DayNumber <= 7) {
      DayName = "Week-end";
    } else {
      DayName = "Invalid";
    }

    return DayName;
  }

}
