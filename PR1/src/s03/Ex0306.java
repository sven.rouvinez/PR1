package s03;

import proglib.SimpleIO;

public class Ex0306 {
  public static void main(String[] args) {

    float a, b, c;
    a = b = c = 0F;

    a = SimpleIO.readFloat("A");
    b = SimpleIO.readFloat("B");
    c = SimpleIO.readFloat("C");

    float r = a; // définition de r avec une variable (arbitraire)

    // si r < b alors r n'est pas la valeur max
    if (r < b) {
      // si b<c alors r devra être c sinon b est la valeur max
      if (b < c) {
        r = c;
      } else {
        r = b;
      }
    }
    System.out.println("Va: " + r);
    System.out.println("Valeur max: " + maxFunction(a, b, c));

  }

  public static float maxFunction(float n1, float n2, float n3) {
    float max = 0F;
    if (n1 >= n2 && n1 >= n3) {
      max = n1;
    } else if (n2 >= n3) {
      max = n2;
    } else {
      max = n3;
    }

    return max;
  }
}
