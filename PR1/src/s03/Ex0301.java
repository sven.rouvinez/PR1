package s03;

import proglib.SimpleIO;

public class Ex0301 {
  public static void main(String[] args) {
    Double cote1;
    Double cote2;
    Double cote3;
    
    

    // ask for the values
    cote1 = SimpleIO.readDouble("cote 1");
    cote2 = SimpleIO.readDouble("cote 2");
    cote3 = SimpleIO.readDouble("cote 3");

    double perimetre = cote1 + cote2 + cote3;

    double coteMax = maxCote(cote1, cote2, cote3);

    if (coteMax < (perimetre - coteMax)) {
      System.out.println("le perim�tre est de " + perimetre);
    } else {
      System.out.println("ce n'est pas un triangle");
    }

  }

  public static double maxCote(double cote1, double cote2, double cote3) {
    double maxCote = 0.0;

    maxCote = cote1;

    if (cote1 >= cote2 && cote1 >= cote3) {
      maxCote = cote1;
    } else if (cote2 >= cote3) {
      maxCote = cote2;
    } else {
      maxCote = cote3;
    }

    return maxCote;
  }

}