//==============================================================================
//  HEIA-FR / Jacques Bapst
//==============================================================================
package s13;

public class Account {

  // private because nobody except the class can have the rights to modify the
  // account
  private int dollars;

  public Account(int money) {
    dollars = money;
  }

  public Account() {
    this(0);
  }

  public int consult() {
    return dollars;
  }

  public void deposit(int x) {
    dollars += x;
  }

  public void withdraw(int x) {
    dollars -= x;
  }

  public void moveTo(Account dest) {
    // have to test if if the account destination is not the start account
    if (dest != this) {
      dest.dollars += this.dollars;
      this.dollars = 0;
    } else {
      throw new IllegalArgumentException("Source is equal to destination");
    }

  }
}