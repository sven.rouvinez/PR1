package s13;

public class TestComplex {

  public static void main(String[] args) {

    Complex c1 = new Complex(3.0, 4.0);
    Complex c2 = new Complex(-1.0, 6.0);
    Complex c5 = new Complex(0.0, -1);
    Complex c3, c4;

    System.out.println(String.format("Argument: %.2f degrés", c5.argument()));
    c3 = c1.addOp(c2); // c3 = c1 + c2
    c4 = c3.multiplyOp(c1); // c4 = c3 * c1

    System.out.println("Module: " + c1.modulus());
    c3.display();
    c4.display();

    c1.add(c2); // c1 += c2
    c3.multiply(c1); // c3 *= c1

    System.out.println("Module: " + c1.modulus());
    c1.display();
    c3.display();
  }

}
