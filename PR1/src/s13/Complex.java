package s13;

public class Complex {
  private double re, im;

  // constructor if parameters are given
  public Complex(double reArgs, double imArgs) {
    re = reArgs;
    im = imArgs;
  }

  // compute the modulus
  public double modulus() {
    return Math.sqrt(re * re + im * im);
  }

  // compute the argument (optional)
  public double argument() {

    /*
     * compute the argument and convert the arctan in degrees if (re > 0) {
     * return Math.toDegrees(Math.atan(div)); } else if (re < 0) { return
     * Math.toDegrees(Math.atan(div) + Math.PI); } else if (re == 0 && im > 0) {
     * return Math.toDegrees(Math.PI / 2); }else if(re==0 && im <0) { return
     * Math.toDegrees(-(Math.PI / 2)); }
     */

    // math.atan2 handles special cases described above
    return Math.toDegrees(Math.atan2(im, re));

  }

  // addition 2 complex number, the object who call the func will store the
  // result
  public void add(Complex z) {
    re += z.re;
    im += z.im;

  }

  // multiply 2 complex number, the object who call the func will store the
  // result
  public void multiply(Complex z) {
    double newRe = re * z.re - im * z.im;
    double newIm = re * z.im + im * z.re;

    re = newRe;
    im = newIm;

  }

  // display im et re parts with 1 number after the point
  public void display() {
    System.out.println(String.format("(%.1f)+(%.1f)j", re, im));
  }

  // addition 2 number and return the result
  public Complex addOp(Complex z) {
    return new Complex(z.re + re, z.im + im);

  }

  // multiply 2 number and return the result
  public Complex multiplyOp(Complex z) {
    double newRe = re * z.re - im * z.im;
    double newIm = re * z.im + im * z.re;

    return new Complex(newRe, newIm);
  }

}
