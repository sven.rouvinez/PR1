package s13;

public class B {
  public int        b;
  public int        d = 2;
  public char       f;
  public double[][] c = new double[10][10];
  public B          h;

  public B(int bargs) {
    b = bargs;
  }

  public B() {

  }

  public int e() {
    return 2;
  }

  public String e(B args) {
    return "testtesttesttesttest";
  }

  public String g(String string) {
    return "coucou";
  }

  public int[] i(int arg) {

    return new int[3];
  }

}
