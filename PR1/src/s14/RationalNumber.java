//==============================================================================
//  EIA-FR
//==============================================================================
package s14;

public class RationalNumber {
  private int n, d;

  public RationalNumber(int n, int d) throws Exception {
    if (d == 0)
      throw new Exception("division by 0 not defined");

    // simplify the fraction and put it in the data_members
    simplify(this);
    this.n = n;
    this.d = d;
  }

  public void multiply(RationalNumber a) {
    n *= a.n;
    d *= a.d;
    simplify(this);
  }

  private void simplify(RationalNumber number) {
    // max number from a and the object is the max common divisor
    for (int i = 2; i <= Math.max(number.n, number.d); ++i) {
      // when a common divisor is found, divide the nominator and denominator by
      // it
      while (number.n % i == 0 && number.d % i == 0) {
        number.n /= i;
        number.d /= i;
      }
    }

  }

  public void divide(RationalNumber a) {
    // division is the multiplication of inverse
    n *= a.d;
    d *= a.n;
    simplify(this);
  }

  private void sameD(RationalNumber a) {
    // multiply the numerator with the object parameters
    a.n *= d;
    n *= a.d;
    d *= a.d;

  }

  public void add(RationalNumber a) {
    this.sameD(a);

    n += a.n;

    simplify(this);

  }

  public void subtract(RationalNumber a) {
    this.sameD(a);

    n -= a.n;

    simplify(this);
  }

  public void multiply(int i) {
    n *= i;
    simplify(this);
  }

  public void add(int i) {
    i *= d;
    n += i;
    simplify(this);
  }

  // return true if the object who call the func is lessThan the parameter
  public boolean lessThan(RationalNumber a) {
    if (this.toFloat() < a.toFloat())
      return true;

    return false;
  }

  // return true if the object who call the func is equals to the parameter
  public boolean equals(RationalNumber a) {
    return n == a.n && d == a.d;
  }

  public float toFloat() {
    return ((float) n) / ((float) d);
  }

  public String toString() {
    return String.format("%d/%d", n, d);
  }
}