package s14.test;

import s14.va.Date;

public class DateTestA {

  public static void main(String[] args) {
    try {

      Date date6 = new Date(1, 12);
      System.out.println(String.format("Month is %d and day is %d",
          date6.month(), date6.dayOfMonth()));
      Date date = new Date(0, 1);
      System.out.println(String.format("Month is %d and day is %d",
          date.month(), date.dayOfMonth()));
      Date date1 = new Date(1, 0);
      System.out.println(String.format("Month is %d and day is %d",
          date1.month(), date1.dayOfMonth()));
      Date date2 = new Date(32, 1);
      System.out.println(String.format("Month is %d and day is %d",
          date2.month(), date2.dayOfMonth()));
      Date date3 = new Date(30, 2);
      System.out.println(String.format("Month is %d and day is %d",
          date3.month(), date3.dayOfMonth()));
      Date date4 = new Date(31, 4);
      System.out.println(String.format("Month is %d and day is %d",
          date4.month(), date4.dayOfMonth()));
      Date date5 = new Date(1, 13);
      System.out.println(String.format("Month is %d and day is %d",
          date5.month(), date5.dayOfMonth()));
    } catch (Exception e) {
      System.out.println(e);
    }
  }

}
