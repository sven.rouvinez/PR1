package s14.vb;

public class Date {

  // min and max values for a date
  private final static int DAY_MIN            = 1;
  private final static int MIN_MONTH          = 1;
  private final static int MAX_MONTH          = 12;

  // declare number of day in month JANUAR to DECEMBER
  private final static int NUMBER_DAY_MONTH[] = { 31, 28, 31, 30, 31, 30, 31,
      31, 30, 31, 30, 31 };

  private int              numberDay;

  public Date(int day, int month) throws Exception {
    if (month < MIN_MONTH || month > MAX_MONTH) {
      throw new Exception("false month");
    }

    if (day > NUMBER_DAY_MONTH[month - 1] || day < DAY_MIN) {
      throw new Exception("false day");
    }

    this.numberDay = day;
    for (int i = 0; i < month - 1; ++i) {
      numberDay += NUMBER_DAY_MONTH[i];
    }

  }

  // compute the value of the day month from numberDay
  public int dayOfMonth() {
    int numberDayToDayMonth = numberDay;
    // stop when we iterate over all months
    for (int i = 0; i < NUMBER_DAY_MONTH.length; ++i) {

      // decrement the value of date of the current month
      while (numberDayToDayMonth > NUMBER_DAY_MONTH[i]) {
        numberDayToDayMonth -= NUMBER_DAY_MONTH[i++];
      }

    }
    return numberDayToDayMonth;
  }

  // compute the value of the month from the numberDay
  public int month() {
    int month = 1;
    int numberDayToDayMonth = numberDay;

    for (int i = 0; i < NUMBER_DAY_MONTH.length; ++i) {
      while (numberDayToDayMonth > NUMBER_DAY_MONTH[i]) {
        numberDayToDayMonth -= NUMBER_DAY_MONTH[i++];
        month++;
      }

    }
    return month;
  }

}
