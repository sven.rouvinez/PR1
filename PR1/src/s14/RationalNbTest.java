//==============================================================================
//  EIA-FR
//==============================================================================
package s14;

public class RationalNbTest {

  public static void main(String[] args) {
    RationalNumber x, y, x1, y1, x2, y2, z1, z2;
    try {
      x = new RationalNumber(3, 4);
      y = new RationalNumber(9, 7);

      x.multiply(y);
      y.add(4);

      System.out.println("x as float : " + x.toFloat()); // 0.9642
      System.out.println("x as string: " + x.toString());       // "27/28"
      System.out.println(x + " = " + y + " ? " + x.equals(y));
      System.out.println(x + " < " + y + " ? " + x.lessThan(y));

      x1 = new RationalNumber(2, 8);
      y1 = new RationalNumber(3, 8);
      x2 = new RationalNumber(1, 8);
      y2 = new RationalNumber(3, 8);
      z1 = new RationalNumber(36, 3);
      z2 = new RationalNumber(40, 20);

      x1.add(y1);
      x2.subtract(y2);
      z1.divide(z2);

      System.out.println("x1 as float : " + x1.toFloat());
      System.out.println("x1 as string: " + x1.toString());
      System.out.println("x2 as float : " + x2.toFloat());
      System.out.println("x2 as string: " + x2.toString());
      System.out.println("z1 as string: " + z1.toString());

      new RationalNumber(3, 0);

    } catch (Exception e) {
      System.out.println(e);
    }

  }
}
