package s14.va;

public class Date {
  private final static int MIN_DAY            = 1;
  private final static int MIN_MONTH          = 1;
  private final static int MAX_MONTH          = 12;

  // declare number of day in month JANUAR to DECEMBER
  private final static int NUMBER_DAY_MONTH[] = { 31, 28, 31, 30, 31, 30, 31,
      31, 30, 31, 30, 31 };

  private int              day;
  private int              month;

  public Date(int day, int month) throws Exception {
    if (month < MIN_MONTH || month > MAX_MONTH) {
      throw new Exception("false month");
    }

    if (day > NUMBER_DAY_MONTH[month - 1] || day < MIN_DAY) {
      throw new Exception("false day");
    }

    this.day = day;
    this.month = month;
  }

  public int dayOfMonth() {
    return day;
  }

  public int month() {
    return month;
  }

}
