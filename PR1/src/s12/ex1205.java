package s12;

import java.io.*;
import java.util.StringTokenizer;

public class ex1205 {

  public static void main(String[] args) {
    String filename = "";

    if (args.length == 0) {
      // ask to the user the filepath if there is no argument
      System.out.print("Enter file path: ");
      BufferedReader keyboard = new BufferedReader(
          new InputStreamReader(System.in));
      try {
        filename = keyboard.readLine();
      } catch (FileNotFoundException e) {
        errorFile(filename);
      } catch (IOException e) {
        errorFile(filename);
      }

    } else if (args.length == 1) {
      filename = args[0];
    } else {
      System.out.println("Too much arguments");
      return;
    }
    // open the file in reading mode
    try (BufferedReader inStream = new BufferedReader(
        new FileReader(filename));) {

      wc(inStream);

    } catch (FileNotFoundException e) {
      errorFile(filename);
    } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
      System.out.println(e);
    } catch (IOException e) {
      errorFile(filename);

    }
  }

  public static void wc(BufferedReader inStream)
      throws ArrayIndexOutOfBoundsException, NullPointerException, IOException {

    int counterWords = 0;
    int counterChar = 0;
    int counterLines = 0;

    String line;

    // iterate on the file
    while ((line = inStream.readLine()) != null) {
      // count number of lines
      counterLines++;

      StringTokenizer t = new StringTokenizer(line);
      counterWords += countWords(t);
      counterChar += countChar(t);

    }

    System.out.println(String.format(
        "Number of words: %d \nNumber of caracters: %d \nNumber of lines: %d",
        counterWords, counterChar, counterLines));

  }

  public static int countWords(StringTokenizer t) {

    int count = 0;
    // get the number of words by line
    count += t.countTokens();

    return count;
  }

  public static int countChar(StringTokenizer t) {

    int count = 0;

    //
    while (t.hasMoreTokens()) {
      // get the size of each words
      count += t.nextToken().length();

    }

    return count;
  }

  public static void errorFile(String filename) {

    System.out.println(String.format("File %s not found", filename));
  }
}
