package s12;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class ex1205a {

  public static void main(String[] args) {
    String filename = "";

    if (args.length == 0) {
      // ask to the user the filepath if there is no argument
      System.out.print("Enter file path: ");
      filename = new Scanner(System.in).nextLine();

    } else if (args.length == 1) {
      filename = args[0];
    } else {
      System.out.println("Too much arguments");
      return;
    }
    try (Scanner scr = new Scanner(
        new BufferedReader(new FileReader(filename)));) {

      wc(scr);

    } catch (FileNotFoundException e) {
      errorFile(filename);
    } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
      System.out.println(e);
    } catch (IOException e) {
      errorFile(filename);

    }
  }

  public static void wc(Scanner inStream)
      throws ArrayIndexOutOfBoundsException, NullPointerException, IOException {

    int counterWords = 0;
    int counterChar = 0;
    int counterLines = 0;

    String line;

    // iterate on the file
    while (inStream.hasNextLine()) {
      line = inStream.nextLine();
      // count number of lines
      counterLines++;

      // return a String array with every words in the line
      String[] splittedWords = splitWords(line);

      counterWords += countWords(splittedWords);
      counterChar += countChar(splittedWords);

    }

    System.out.println(String.format(
        "Number of words: %d \nNumber of caracters: %d \nNumber of lines: %d",
        counterWords, counterChar, counterLines));

  }

  public static String[] splitWords(String line) {
    // split each words by space
    return line.split("[\\s]+");
  }

  public static int countWords(String[] splittedWords) {
    // check if there is an empty return line
    if (splittedWords[0].matches(""))
      return 0;
    // return length of the splittedWords == number of words in the line
    return splittedWords.length;
  }

  public static int countChar(String[] splittedWords) {
    int count = 0;

    // return the number of characters in each word
    for (int i = 0; i < splittedWords.length; ++i) {
      count += splittedWords[i].length();
    }

    return count;
  }

  public static void errorFile(String filename) {

    System.out.println(String.format("File %s not found", filename));
  }
}
