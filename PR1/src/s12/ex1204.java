//==============================================================================
//  HEIA-FR 
//==============================================================================
package s12;

import java.io.*;

//------------------------------------------------------------------------------
//  Key used: "JAVA"
//    Test coding with file "codeWithVigenere.txt"
//    Test decoding with file "decodeWithVigenere.vig"
//  Key used: "E"
//    Test coding with file "codeWithCesar.txt"
//    Test decoding with file "decodeWithCesar.vig"
//------------------------------------------------------------------------------

public class ex1204 {

  // ------------------------------------------------------------------------------
  public static void main(String[] args) {
    String filenameIn = "";
    String key = "";

    // if no arguments are given, print message
    if (args.length == 0) {
      System.out.println(
          "No parameter given... Trying with " + filenameIn + " " + key);
    } else if (args.length == 2) {
      // if there are 2 arguments, aroument at position is the filepath and key
      // is the element on the second position
      filenameIn = args[0];
      key = args[1];
    } else {
      // print a message to tell there are too much arguments
      System.out.println("Error: two arguments expected, "
          + "i.e. filename and uppercase cipher key word.");
      return;
    }
    // prepare the out filename
    String filenameOut = filenameIn.substring(0, filenameIn.length() - 4);
    boolean decode;
    // check if we have to decode or encode the text by the extentsion of the
    // input file
    if (filenameIn.endsWith(".vig")) {
      filenameOut += ".txt"; // add the extension at the end of the out file
      decode = true;
    } else if (filenameIn.endsWith(".txt")) {
      filenameOut += ".vig";
      decode = false;
    } else {
      System.out.println("ERROR: filename should end with '.txt' or '.vig'");
      return;
    }

    try (
        BufferedReader inputStream = new BufferedReader(
            new FileReader(filenameIn));
        PrintWriter outputStream = new PrintWriter(
            new FileWriter(filenameOut))) {

      vigenereCypher(inputStream, outputStream, key, decode);
    } catch (FileNotFoundException e) {
      System.out.println(e);
    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println(e);
    } catch (NullPointerException e) {
      System.out.println(e);
    } catch (IOException e) {
      System.out.println(e);
    }

  }

  // ----------------------------------------------------------------------------
  // Encrypts/decrypts input using Vigenere's cipher algorithm with given key
  // ----------------------------------------------------------------------------
  public static void vigenereCypher(BufferedReader in, PrintWriter out,
      String key, boolean decode)
      throws IOException, ArrayIndexOutOfBoundsException, NullPointerException {
    // create and initialize a variable to save the encoded or decoded string
    // Use of the StringBuilder class because it's nice on memory access
    StringBuilder cipherText = new StringBuilder();

    String line;
    //while we are not at the end of the text.
    while ((line = in.readLine()) != null) {
      char charToAdd = ' ';
      int indexK = 0;
      cipherText.delete(0, cipherText.capacity()); // empty the cipherText if
                                                   // there are multiple line

        // the loop to pass through all the char of the text
      for (int i = 0; i < line.length(); i++) {
        // begin at the start of the key it we iterate on all the elements
        if (indexK > key.length() - 1)
          indexK = 0;
        
        if (decode) {//check if we have to encode or decode the text
          // use the metho decodeChar to decode each char with the loop
          charToAdd = decodeChar(key.charAt(indexK++), line.charAt(i));
        } else {
            //if the decode boolean is false it mean we have to encode the text with the method enodeChar.
          charToAdd = encodeChar(key.charAt(indexK++), line.charAt(i));
        }
        //add each char of the loop in the cipherText builder.
        cipherText.append(charToAdd);

      }
      // print all the text in the out file
      out.println(cipherText);
    }

    System.out.println("Finished.");
  }

  public static char encodeChar(char key, char caracter) {
    //-A to see the diff with the key ( exemple E-A = 4 ) + the number of the caractere.
    char charEncoded = (char) ((key - 'A') + caracter);
    // to stay in the range of the letters
    if (charEncoded > 90)
      charEncoded -= 26;

    return charEncoded;

  }

  public static char decodeChar(char key, char caracter) {
    //take the number of the car , add the value of 'A' (65) and take off the number of the key to get the original value.
    char charDecoded = (char) ((caracter + 'A') - key);
    //to stay in the range of the letters
    if (charDecoded < 64)
      charDecoded += 26;

    return charDecoded;

  }

}