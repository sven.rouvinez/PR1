package s12;

public class ex1203 {

  public static void main(String[] args) {
    System.out.println(sumASCII(args));
  }

  public static int sumASCII(String[] args) {
    int sum = 0;

    // sum all the ASCII value from the elements
    for (int i = 0; i < args[0].length(); ++i) {
      sum += args[0].charAt(i);
    }

    return sum;
  }
}
