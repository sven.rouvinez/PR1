package s12;

public class ex1202 {

  public static void main(String[] args) {

    double moyenne, sum = 0.0;
    for (String elem : args)
      // sum all elements
      sum += Double.parseDouble(elem);

    //Divide by the size of the number of elements in args array
    moyenne = sum / args.length;
    System.out.println(String.format("Moyenne %f", moyenne));

  }

}
