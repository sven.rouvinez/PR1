
package s23;

import java.util.Random;

public class CharGrid {
  private char[][] grid;
  
  public CharGrid(char[][] grid) {
    this.grid = grid;
  }
  
  public CharGrid(int n, int m) throws IllegalArgumentException {
    if (n <= 0 || m <= 0 || n > m)
      throw new IllegalArgumentException("illegal parameters");
    grid = new char[n][m];
    
    Random rnd = new Random(m);
    
    for(int i =0;i<grid.length;++i) {
      for(int j = 0;j<grid[i].length;++j) {
        grid[i][j] = (char) ((char) rnd.nextInt(((int) 'Z') - ((int) 'A') + 1)
            + ((int) 'A'));
      }
    }
  }
  
  public boolean isPresent(String word) {
    int index = 0;

    for (int i = 0; i < grid.length; ++i) {
      for (int j = 0; j < grid[i].length; ++j) {
         while(grid[i][j] == word.charAt(index)) {

           if(word.length() == j+1) {
            return true;
           }
          index++;
          j++;
         }
      }

    }

    return false;
  }

  public static void main (String[] args) {
    char[][] ele = { { 'A', 'A' }, { 'S', 'A', 'L', 'U', 'T' } };

    CharGrid grid = new CharGrid(ele);
    if (grid.isPresent("SALUT"))
      System.out.println("trouvé");

  }
}