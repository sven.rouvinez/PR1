
package s23;

import s23.GuessGame.STATE;

public class ArtificialIntelligence {
  // prints to the console the sequence of guesses
  // (the last one is the correct one)
  public static void solve(GuessGame g) {
    STATE stateGame;
    int input = 0;

    do {
      System.out.println("Trying with " + input);
      stateGame = g.tryToGuess(input);

      switch (stateGame) {
        case TOO_BIG:
          input--;
          break;
        case TOO_SMALL:
          input++;
          break;
        case FOUND:
          System.out.println("Well done. The number was: " + input);
          break;
        default:
          System.out.println("Invalid input");
      }
    } while (stateGame != STATE.FOUND);
  }

  public static void main(String[] args) {
    GuessGame game = new GuessGame();
    ArtificialIntelligence.solve(game);
  }
}