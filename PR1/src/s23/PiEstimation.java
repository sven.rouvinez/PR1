
package s23;

import java.awt.geom.Point2D;
import java.util.Random;

public class PiEstimation {
  private int nbOfPoints;

  public PiEstimation(int nbOfPoints) {
    this.nbOfPoints = nbOfPoints;
  }

  public double piApproximation() {
    int counter = 0;
    Point2D.Double[] shots = new Point2D.Double[nbOfPoints];
    Random rnd = new Random();

    for (int i = 0; i < shots.length; i++) {

      shots[i] = new Point2D.Double(-1.0 + (rnd.nextDouble() * (1.0 - (-1.0))),
          -1.0 + (rnd.nextDouble() * (1.0 - (-1.0))));
    }

    for (int i = 0; i < shots.length; ++i) {
      double x = shots[i].getX();
      double y = shots[i].getY();

      if ((x * x + y * y) < 1.0)
        counter++;

    }

    return 4.0 * ((double) counter / (double) nbOfPoints);
  }

  public static void main(String[] args) {
    int number = 1;

    while (new PiEstimation(number).piApproximation() <= 3.4) {
      number++;
    }

    System.out.println(number);

    System.out.println(new PiEstimation(500).piApproximation());
  }
}
