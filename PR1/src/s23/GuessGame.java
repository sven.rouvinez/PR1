
package s23;

import java.util.Random;

import proglib.SimpleIO;

public class GuessGame {
  public enum STATE {
    TOO_SMALL, TOO_BIG, FOUND
  };

  private int number;
  
  public GuessGame() {
    this.number = new Random().nextInt(1001);
  }
  
  public STATE tryToGuess(int guess) {
    if (guess > number)
      return STATE.TOO_BIG;
    if (guess < number)
      return STATE.TOO_SMALL;

      return STATE.FOUND;
  }

  public static void main(String[] args) {
    GuessGame newGame = new GuessGame();
    STATE stateGame;
    
    int input;
    do {
      input = SimpleIO.readInt("Entrez le int");
      System.out.println("Trying with " + input);
      stateGame = newGame.tryToGuess(input);
      
      switch (stateGame){
        case TOO_BIG:
          System.out.println("Number too big");
      break;
        case TOO_SMALL:
          System.out.println("Number too small");
          break;
        case FOUND:
          System.out.println("Well done. The number was: "+input);
          break;
          default:
            System.out.println("Invalid input");
      }
    } while (stateGame != STATE.FOUND);
    
  }
}