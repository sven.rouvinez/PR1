
package s23;

public class HexNumber extends BasedNumber {
  private final static int BASE = 16;

  
  public HexNumber(String s) {
    super(s, BASE);
  }
  
  public long asLong() {
    return super.asLong();
  }
  
  public void add(HexNumber n) {
    super.stringNumber = String.format("%d", (super.asLong() + n.asLong()));
  }
  
  public void subtract(HexNumber n) {
    super.stringNumber = String.format("%d", (super.asLong() - n.asLong()));
  }
  
  public void multiply(HexNumber n) {
    super.stringNumber = String.format("%d", (super.asLong() * n.asLong()));
  }
  
  public String toString() {
    return super.inBase(BASE);
  }

  public static void main(String[] args) {
    HexNumber a;
    a = new HexNumber("1F4B");
    a.add(new HexNumber("A8C7"));
    System.out.print(a);

  }
}

