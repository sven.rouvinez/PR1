
package s23;

import java.awt.Point;
import java.util.Arrays;

public class TriangleBrainTeaser {

  // gives all solutions cyclicly
  public static int[] nextSolution() {
    int[][] triangle = new int[4][4];
    int[] solution = new int[10];
    int value = 5;
    Point[] pp = new Point[2];

    for (int i = 0; i < triangle.length; ++i) {
      for (int j = 0; j < triangle[i].length; ++j) {
        if (i == 1 && j == 0) {
          triangle[i][j++] = triangle[0][0];
        }
        if (i == 2 && j == 0) {
          triangle[i][j++] = triangle[0][3];
        }
        if (i == 2 && j == 1) {
          triangle[i][j++] = triangle[1][3];
        }

        if (value > 9) {
          value = 1;
        }

        triangle[i][j] = value++;

      }

    }

    int sumL0 = Arrays.stream(triangle[0]).sum();
    int sumL1 = Arrays.stream(triangle[1]).sum();
    int sumL2 = Arrays.stream(triangle[1]).sum();

    if (sumL0 == sumL1 && sumL0 == sumL2 && sumL1 == sumL2) {
      int index = 0;
      for (int i = 0; i < triangle.length; ++i) {
        for (int j = 0; j < triangle[i].length; ++j) {
          solution[index++] = triangle[i][j];
        }

      }
      return solution;
    }

    return null;
  }

  public static void main(String[] args) {
    new TriangleBrainTeaser();
    System.out
        .println(Arrays.toString(TriangleBrainTeaser.nextSolution()));
  }
}