
package s23;

public class NumbersInLine {
  private int lineLength;

  public NumbersInLine(int lineLength) throws RuntimeException {
    if (lineLength <= 0)
      throw new RuntimeException("lineLength > 0");

    this.lineLength = lineLength;

  }

  public String format(int from, int to) throws RuntimeException {
    if (from > to)
      throw new RuntimeException("to must be > from");
    StringBuilder string = new StringBuilder();

    for (; from <= to; from++) {

      string.append(String.format("[%d]", from));

    }

    return string.toString();
  }

  public static void main(String[] args) {
    try {
      NumbersInLine x = new NumbersInLine(2);
      System.out.println(x.format(14, 15));
    } catch (RuntimeException e) {
      System.out.println(e);
    }

  }

}
