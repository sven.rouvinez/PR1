
package s23;

public class BasedNumber {
  protected String stringNumber;
  
  public BasedNumber(String s, int base) {
    this.stringNumber = Integer.valueOf(s, base).toString();
  }
  
  public long asLong() {
    return Integer.valueOf(stringNumber);
  }
  
  public String inBase(int base) {
    return Long.toString(this.asLong(), base);
  }

  public static void main(String[] args) {
    BasedNumber n1 = new BasedNumber("1F", 16);
    System.out.println(n1.asLong());
    System.out.println(n1.inBase(2));

  }
}