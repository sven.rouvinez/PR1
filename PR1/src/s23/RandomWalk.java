
package s23;

import java.awt.Point;
import java.util.Random;

public class RandomWalk {
  private final static Point ORIGIN = new Point(0, 0);
  private int                kStep;
  private Point              coordinate;
  
  public RandomWalk(int k) {
    this.kStep = k;
    this.coordinate = ORIGIN;
    
  }
  
  public void walkOneStep() {
    Random rnd = new Random();
    
    switch (rnd.nextInt(3)) {
      case 0:
        coordinate.y++;
        break;
      case 1:
        coordinate.y--;
        break;
      case 2:
        coordinate.x++;
        break;
      case 3:
        coordinate.x--;
        break;
    }
  }
  
  public long crtPositionX() {
    return 0;
  }
  
  public long crtPositionY() {
    return 0;
  }
  
  public boolean isAtOrigin() {
    return coordinate.getLocation() == ORIGIN;
  }


public static void main(String[] args) {
  
    RandomWalk test = new RandomWalk(10);
  
  }
}