
package s23;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BenfordReader extends BenfordAnalyzer {

  
  public void addNumbersFoundInString(String s) {

    Pattern p = Pattern.compile("(\\d+(?:\\.\\d+))");
    Matcher m = p.matcher(s);
    while (m.find()) {
      System.out.println(Double.parseDouble(m.group(1)));
      addNumber(Double.parseDouble(m.group(1)));
    }
    
    /*
     * int start = 0;
     * 
     * for (int i = 0; i < s.length(); ++i) { start = 0;
     * 
     * if (Character.isDigit(s.charAt(i)) || s.charAt(i) == '.') { start = i; if
     * (i + 1 >= s.length()) break; while (Character.isDigit(s.charAt(i + 1)) ||
     * s.charAt(i + 1) == '.') {
     * 
     * i++; if (i + 1 >= s.length()) break; }
     */

    // addNumber(Double.parseDouble(s.substring(start, i + 1)));

  }

  public void addNumbersFoundInFile(String filename) throws IOException {
    String line;
    try (BufferedReader inStream = new BufferedReader(
        new FileReader(filename))) {
      while ((line = inStream.readLine()) != null) {
        addNumbersFoundInString(line);
      }
    }
  }

  /**
   * returns an array r with r.length==10; r[0] is not used, "r[d]==f" means
   * that digit d appears as first significant digit with frequency f (0 <= f <=
   * 1) It considers all numbers added so far.
   */
  public double[] firstDigitDistribution() {
    return super.firstDigitDistribution();
  }

  // ------------------------------------------------------------
  public static void main(String... args) {
    BenfordReader ba = new BenfordReader();
    String s = "Rule 8 - Here is a 25.9 mark on a 0.04 box; it costs $ 8.5 now.";
    ba.addNumbersFoundInString(s);
    System.out.println(Arrays.toString(ba.firstDigitDistribution()));

    ba = new BenfordReader();
    try {
      ba.addNumbersFoundInFile(
          "/home/wagner/Downloads/KANTON_ZUERICH_199.csv");
      System.out.println(Arrays.toString(ba.firstDigitDistribution()));
    } catch (IOException e) {
      System.err.println(e);
    }

  }

}