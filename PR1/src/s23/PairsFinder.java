
package s23;

import java.util.Arrays;

public class PairsFinder {
  int m;

  public PairsFinder(int m) {  
    this.m = m;

  }
  
  // returns null eventually
  public int[] nextPair() {
    System.out.println("Start");
    int[] pairs = new int[628];
    int index = 0;
    for (int i = 1; i < m; ++i) {
      for (int j = i + 1; j < m; ++j) {
        if ((j * j) - (i * i) == Math.pow(j - i, 3)) {
          pairs[index++] = i;
          pairs[index] = j;
        }

      }
    }
    return pairs;
  }
  
  
  public static void main(String[] args) {

    System.out.println(Arrays.toString(new PairsFinder(50000).nextPair()));

    }
}