
package s17;

public class Bidon {
  private final int capacity;
  private int       contents;

  /** PRE-condition: cap>0 */
  public Bidon(int cap) {
    /*
     * if (cap <= 0) throw new Exception("Capacity > 0");
     */

    capacity = cap;
  }

  /** POST-condition: it must be full */
  public void fill() {
    contents = capacity;
  }

  /** POST-condition: it must be empty */
  public void empty() {
    contents = 0;
  }

  public void transferFrom(Bidon b) {
    // Avoid transform from the same bidon
    if (this != b) {

      int spaceLeft = this.capacity - this.contents;
      int fill = 0;

      if (spaceLeft > 0) {

        if (spaceLeft == this.capacity) {
          fill = b.contents;

          // fill with the left space in the object
          if (this.capacity < b.capacity) {
            fill = this.capacity - this.contents;
          }

          //
          if (b.contents < fill) {
            fill = b.contents;
          }


        } else {
          // calculate the space left
          fill = this.capacity - this.contents;

          // if there's more contents in b, transfer the left fill from b
          if (this.contents > b.contents) {
            fill = this.capacity - b.contents;
          }

        }
        // fill and unfill bucket from the fill
        this.contents += fill;
        b.contents -= fill;
      }
    }

  }

  public int contents() {
    return contents;
  }

  @Override
  public String toString() {
    return "Capacity: " + capacity + ", Contents: " + contents();
  }

  // =======================================
  public static void main(String[] args) {

    Bidon a = new Bidon(8);
    Bidon b = new Bidon(5);

    b.fill();
    a.transferFrom(b);
    System.out.println(a.contents() + " " + b.contents() + " should be: 5 0");
    b.fill();
    a.transferFrom(b);
    System.out.println(a.contents() + " " + b.contents() + " should be: 8 2");

  }
}
