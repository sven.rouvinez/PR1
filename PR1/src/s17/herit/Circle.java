package s17.herit;

public class Circle extends Point {

  private double radius;

  public Circle(double xCenter, double yCenter, double radius) {
    // construct the center of the circle
    super(xCenter, yCenter);
    this.radius = radius;
  }

  public void ChangeRadius(double radius) {
    this.radius = radius;
  }

  public void moveCenter(double x, double y) {
    this.move(x, y);
  }

  public Point center() {
    // return new Point
    return new Point(this.getX(), this.getY());
  }

  @Override
  public String toString() {
    return String.format(
        "Coordinate center (%.2f,%.2f), Radius %.2f",
        this.center().getX(), this.center().getY(),
        this.radius);

  }

}
