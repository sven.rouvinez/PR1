
package s17;

public class EcoBidon extends Bidon {
  private final Bidon tank;

  /** PRE-condition: capacity>0 && tank!=null */
  public EcoBidon(int capacity, Bidon tank) {
    super(capacity);
    this.tank = tank;
  }

  @Override
  // fill the tank with the bucket
  public void empty() {
    tank.transferFrom(this);
    super.empty();
  }

  @Override
  public void fill() {

    // fille the tank until it's full
    while (true) {
      this.transferFrom(tank);
      if (tank.contents() == 0) {
        tank.fill();
      } else {
        break;
      }
    }
  }

  // =======================================
  public static void main(String[] args) {

    Bidon tank = new Bidon(9);
    EcoBidon a = new EcoBidon(8, tank);
    EcoBidon b = new EcoBidon(5, tank);

    printState(a, b, tank, "a=0, b=0, tank=0");
    tank.fill();
    System.out.println("...tank.fill()...");
    printState(a, b, tank, "a=0, b=0, tank=9");
    b.fill();
    System.out.println("...b.fill()...");
    printState(a, b, tank, "a=0, b=5, tank=4");
    a.transferFrom(b);
    System.out.println("...a.transferFrom(b)...");
    printState(a, b, tank, "a=5, b=0, tank=4");
    b.fill();
    System.out.println("...b.fill()...");
    printState(a, b, tank, "a=5, b=5, tank=8");
    a.transferFrom(b);
    System.out.println("...a.transferFrom(b)...");
    printState(a, b, tank, "a=8, b=2, tank=8");
    a.empty();
    System.out.println("...a.empty()...");
    printState(a, b, tank, "a=0, b=2, tank=9");
    a.fill();
    System.out.println("...a.fill()...");
    printState(a, b, tank, "a=8, b=2, tank=1");
    a = new EcoBidon(1, tank);
    System.out.println("...a=new EcoBidon(1, tank)...");
    printState(a, b, tank, "a=0, b=2, tank=1");
    a.fill();
    System.out.println("...a.fill()...");
    printState(a, b, tank, "a=1, b=2, tank=9");

  }

  private static void printState(Bidon a, Bidon b, Bidon c, String expected) {
    System.out.printf("Observed: a=%d, b=%d, tank=%d.\nExpected: %s\n\n",
        a.contents(), b.contents(), c.contents(), expected);
  }
}
