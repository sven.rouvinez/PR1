package s17.compo;

public class Circle {
  private Point  center;
  private double radius;

  public Circle(double xCenter, double yCenter, double radius) {
    // construct the center of the circle
    this.center = new Point(xCenter, yCenter);
    this.radius = radius;
  }

  public void moveCenter(double x, double y) {
    center.move(x, y);
  }

  public void changeRadius(double radius) {
    this.radius = radius;
  }

  public Point center() {
    // cannot return this.center because it can be modified
    // so must return a new point
    return new Point(this.center.getX(), this.center.getY());
  }

  @Override
  public String toString() {
    return String.format("Coordinate center (%.2f,%.2f) \n Radius %.2f",
        this.center().getX(), this.center().getY(), radius);
  }

}
