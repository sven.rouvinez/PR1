package s17.compo;

import s17.herit.Circle;

public class CircleTest {
  public static void main(String[] args) {
    Circle c1 = new Circle(2.0, 3.0, 7.6);
    System.out.println(c1.toString() + "\n");

    c1.moveCenter(3, 4);
    System.out.println("After moveCenter");
    System.out.println(c1 + "\n");

    System.out.println("Center: " + c1.center());

    c1.ChangeRadius(4);
    System.out.println("\nAfter changeRadius");
    System.out.println(c1);

  }

}