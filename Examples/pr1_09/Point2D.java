package pr1_09;

public class Point2D {
  // data members
  private double x;
  private double y;
  
  // static data members
  // nbrOfActiveInstances is incremented upon each instance creation
  // it is decremented 
  private static long nbrOfActiveInstances = 0;
  
  // constants
  public static final Point2D ORIGIN = new Point2D();
  
  // constructors
  public Point2D() {
    // overload
    this(0.0, 0.0);
  }
  
  public Point2D(double x, double y) {
    this.x = x;
    this.y = y;
    
    // increment the number of active instances
    nbrOfActiveInstances++;
  }
  
  // copy constructor
  public Point2D(Point2D otherPoint) {
    // overload 
    this(otherPoint.x, otherPoint.y);
  }
  
  // finalize method
  public void finalize() {
    // decrement the number of active instances
    nbrOfActiveInstances--;
  }
  
  // static methods
  public static Point2D getOrigin() {
    return ORIGIN;
  }
  
  public static long getNbrOfActiveInstances() {
    return nbrOfActiveInstances;
  }
  
  // accessor methods
  public double getX() {
    return this.x;    
  }
  
  public double getY() {
    return this.y;
  }
  
  // methods
  public void move(double dx, double dy) {
    this.x += dx;
    this.y += dy;
  }
  
  public String toString() {
    return new String("x: " + this.x + " y:" + this.y);
  }
  
  public boolean isAtLeftOf(Point2D other) {
    // the private data members of other can be accessed here (within the class)
    return this.x < other.x;
  }
}
