package pr1_09;

import pr1_09.Point2D;

public class Line2D {
  // data members
  // a line is defined by two points
  private Point2D pt1;
  private Point2D pt2;
  // or alternatively it can be defined by one point and a slope
  //private Point2D pt1;
  //private double slope;
  
  // static data members
  public static final Line2D HORIZONTAL_LINE_THROUGH_ORIGIN = new Line2D(Point2D.ORIGIN, new Point2D(1.0, 0.0));
  public static final Line2D VERTICAL_LINE_THROUGH_ORIGIN = new Line2D(Point2D.ORIGIN, new Point2D(0.0, 1.0));
  public static final int NBR_OF_LINES_IN_PLANE = 8;
  public static final Line2D[] LINES_IN_PLANE = new Line2D[NBR_OF_LINES_IN_PLANE];
  
  // static initializer block
  static {
    try {
      // the equation of a line is y = a * x + b
      // with a = tan(alpha) being the slope
      // alpha is the angle with respect to the x-axis      
      double angle = 0.0;
      double angleIncrement = (2 * Math.PI) / NBR_OF_LINES_IN_PLANE;
      for (int i = 0; i < NBR_OF_LINES_IN_PLANE; i++) {
        LINES_IN_PLANE[i] = new Line2D(Math.tan(angle));
        angle += angleIncrement;
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  // constructors
  // default constructor is made private for preventing its use
  private Line2D() throws Exception {
    throw new IllegalAccessException();
  }
  
  // constructor from two points
  public Line2D(Point2D pt1, Point2D pt2) {
    // create the points representing the line from the arguments
    this.pt1 = pt1;
    this.pt2 = pt2;
    // alternative initialization
    // this.pt1 = new Point2D(pt1);
    // this.slope = (pt2.getY() - pt1.getY()) / (pt2.getX() - pt1.getX());
  }
  
  // constructor for a line going through the origin with a given slope
  public Line2D(double slope) {
    // make pt1 refer to the origin (static member)
    this.pt1 = Point2D.ORIGIN;
 
    // remember that the slope is (y2 - y1) / (x2 - x1) with (x1, y1) = (0, 0)
    // we choose x2 to be 1
    // so y2 = slope;
    double x2 = 1.0;
    double y2 = slope;
    this.pt2 = new Point2D(x2, y2); 
  }
  
  // constructor from one point and slope
  public Line2D(Point2D pt, double slope) {
    // do not use overloading here
    // because overloading must be specified as the first line of code !
    
    // store first point
    this.pt1 = new Point2D(pt);
    
    // compute the coordinate of the second point
    
    // remember that the slope is (y2 - y1) / (x2 - x1)
    // we choose x2 - x1 to be 1
    // so y2 = slope + y1;
    double x2 = pt.getX() + 1.0;
    double y2 = pt.getY() + slope;
    this.pt2 = new Point2D(x2, y2); 
    
    // alternate initialization
    // this.slope = slope;
  }
  
  // methods
  public double getSlope() {
    // the slope of a line going through pt1 and pt2 is
    double slope = (this.pt2.getY() - this.pt1.getY()) / (this.pt2.getX() - this.pt1.getX());
    return slope;
  }
}
