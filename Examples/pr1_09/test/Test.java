// define our package
package pr1_09.test;

// define imports
import pr1_09.Point2D;
import pr1_09.Line2D;

// define static imports
import static pr1_09.Point2D.getOrigin;
import static java.lang.System.out;

public class Test {

  public static void main(String[] args) {

    // TEST the Point2D class
    
    // create an object
    Point2D pt1 = new Point2D();
    Point2D pt2 = new Point2D(1.0, 1.0);
    Point2D pt3 = new Point2D(pt2);

    out.println(pt3);    
    pt3.move(1.0, 1.0);
    out.println(pt3);
    
    // display the origin point in the console
    out.println(getOrigin());
    out.println(Point2D.ORIGIN);
    
    // data members are not visible, so the statements below are invalid
    // pt3.x += 1.0;
    // pt3.y -= 1.0;
    
    // release the pt3 instance
    System.out.println("Number of active points is " + Point2D.getNbrOfActiveInstances());
    pt3 = null;
    
    // invoke the garbage collector (is not a good practice)
    // sleep the current thread for giving a chance to the gc to do its job
    Runtime r = Runtime.getRuntime();
    r.gc();
    try {
      Thread.sleep(5000);
    }
    catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("Number of active points is " + Point2D.getNbrOfActiveInstances());
    
    // TEST the Line2D class
    
    // default contructor is private so we cannot create an "empty" line
    // Line2D l1 = new Line2D();
    
    try {
      // create a line with pt1 and pt2
      Line2D l2 = new Line2D(pt1, pt2);
    
      // create another line
      Line2D l3 = new Line2D(pt1, 1.0);
    
      double slope = l3.getSlope();
      out.println("Line slope is " + slope);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}
