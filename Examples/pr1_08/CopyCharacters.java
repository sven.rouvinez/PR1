package pr1_08;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CopyCharacters {

  public static void main(String[] args) {
    // we expect the names of the input file and of the output file as arguments
    if (args.length != 2) {
      System.out.println("Please specify the input file and the output file as arguments");
      return;
    }
    
    String inputPath = args[0];
    String outputPath = args[1];
    
    // open the input and output file using try-with-resources
    try (FileReader inputStream = new FileReader(inputPath);
         FileWriter outputStream = new FileWriter(outputPath)) {
      // read and write an array of characters 
      char[] charArray = new char[3];
      while (inputStream.read(charArray) != -1) {
        outputStream.write(charArray);
      }
    }
    catch (FileNotFoundException e) {
      System.out.println(e); 
    }
    catch (IOException e) {
      System.out.println(e);
    }
  }

}
