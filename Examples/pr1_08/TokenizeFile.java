package pr1_08;

import java.io.*;
import java.util.StringTokenizer;

public class TokenizeFile {

  public static void main(String[] args) {
    // we expect the names of the input file and of the output file as arguments
    if (args.length != 2) {
      System.out.println("Please specify the input file and the output file as arguments");
      return;
    }
    
    String inputPath = args[0];
    String outputPath = args[1];
    
    // open the input and output file using try-with-resources
    try (BufferedReader reader = new BufferedReader(new FileReader(inputPath));
         PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(outputPath)))) {
      
      // read all double values and write them back in the output file
      String line;      
      while ((line = reader.readLine()) != null) {
        // the delimiter is space only
        String delimiter = " ";
        StringTokenizer st = new StringTokenizer(line, delimiter);
        while (st.hasMoreTokens()) {
          String s = st.nextToken();
          double d;
          try {
            d = Double.valueOf(s).doubleValue();
          }
          catch (NumberFormatException e) {
            // invalid token -> continue
            System.out.println("Non double expression found: " + s);
            continue;
          }
          printWriter.printf("%05f ", d);
          System.out.println("Double found: " + d);
        }        
      }        
    }
    catch (FileNotFoundException e) {
      System.out.println(e); 
    }
    catch (IOException e) {
      System.out.println(e);
    }
  }

}
