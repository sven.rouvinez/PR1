package pr1_08;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ScanConsole {

  public static void main(String[] args) {
    // we expect the name of the output file as arguments
    if (args.length != 1) {
      System.out.println("Please specify the output file as argument");
      return;
    }
    
    String outputPath = args[0];

    // create a scanner object that reads from console (System.in)
    try (PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(outputPath))); 
         Scanner scanner = new Scanner(System.in)) {     
      System.out.println("Enter one or more lines with integers (type 'x' at line start to exit): ");
      done:
      while (scanner.hasNext()) { 
        while (scanner.hasNext() && ! scanner.hasNextInt()) {
          String s = scanner.next();
          if (s.equals("x")) {
            break done;
          }
        }
        while (scanner.hasNextInt()) {
          int i = scanner.nextInt();
          printWriter.printf("%d ", i);   
        }        
      }
    }
    catch (IOException e) {
      System.out.println(e);
    }
  }

}
