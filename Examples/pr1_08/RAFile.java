package pr1_08;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class RAFile {

  public static void main(String[] args) {
    // we expect the name of the output file as arguments
    if (args.length != 1) {
      System.out.println("Please specify the output file as argument");
      return;
    }

    String outputPath = args[0];
    
    RandomAccessFile file = null;
    try {
      // create a new RandomAccessFile with path being outputPath
      // note that RandomAccessFile does not implement Closeable and cannot be used
      // in a try-with-resources construction
      // open the file for read and write
      file = new RandomAccessFile(outputPath, "rw");
      
      // write something to the file
      file.writeUTF("Hello World");

      // set the file pointer at 0 position
      file.seek(0);

      // print the line that we just wrote to the file
      System.out.println(file.readUTF());

      // set the file pointer at 0 position again
      file.seek(0);

      // write something else to the file
      file.writeUTF("This is another example");

      // set the file pointer at 0 position again
      file.seek(0);

      // print the two lines that we just wrote to the file
      System.out.println(file.readUTF());
    
    }
    catch (FileNotFoundException e) {
      System.out.println("Cannot create file with path: " + outputPath);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    finally {
      try {
        if (file != null) {
          file.close();  
        }        
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
