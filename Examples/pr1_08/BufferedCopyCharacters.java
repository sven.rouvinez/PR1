package pr1_08;

import java.io.*;

public class BufferedCopyCharacters {

  public static void main(String[] args) {
    // we expect the names of the input file and of the output file as arguments
    if (args.length != 2) {
      System.out.println("Please specify the input file and the output file as arguments");
      return;
    }
    
    String inputPath = args[0];
    String outputPath = args[1];
    
    // open the input and output file using try-with-resources
    try (BufferedReader inputStream = new BufferedReader(new FileReader(inputPath));
         BufferedWriter outputStream = new BufferedWriter(new FileWriter(outputPath))) {
      // read and write line by line
      String line;
      while ((line = inputStream.readLine()) != null) {
        outputStream.write(line);
      }
    }
    catch (FileNotFoundException e) {
      System.out.println(e); 
    }
    catch (IOException e) {
      System.out.println(e);
    }

  }

}
