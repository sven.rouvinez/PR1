package pr1_08;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyBytes {

  public static void main(String[] args) {
    // we expect the names of the input file and of the output file as arguments
    if (args.length != 2) {
      System.out.println("Please specify the input file and the output file as arguments");
      return;
    }
    
    String inputPath = args[0];
    String outputPath = args[1];
    
    // open the input and output file using try-with-resources
    try (FileInputStream in = new FileInputStream(inputPath);
         FileOutputStream out = new FileOutputStream(outputPath)) {
      int c;

      // we simply, blindly copy each byte from the input file to the output file
      while ((c = in.read()) != -1) {
        out.write(c);
      }
    }
    catch (FileNotFoundException e) {
      System.out.println(e); 
    }
    catch (IOException e) {
      System.out.println(e);
    }
  }
}
