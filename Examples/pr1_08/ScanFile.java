package pr1_08;

import java.io.*;
import java.util.Scanner;

public class ScanFile {

  public static void main(String[] args) {
    // we expect the names of the input file and of the output file as arguments
    if (args.length != 2) {
      System.out.println("Please specify the input file and the output file as arguments");
      return;
    }
    
    String inputPath = args[0];
    String outputPath = args[1];
    
    // open the input and output file using try-with-resources
    try (Scanner scanner = new Scanner(new BufferedReader(new FileReader(inputPath)));
         PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(outputPath)))) {
      
      // read all double values and write them back in the output file

      // the delimiter is space or new line
      String lineSeparator = System.getProperty("line.separator");
      String delimiter = " |" + lineSeparator;
      scanner.useDelimiter(delimiter);
      
      while (scanner.hasNext()) {
        while (scanner.hasNext() && ! scanner.hasNextDouble()) {
          String s = scanner.next();          
          System.out.println("Non double expression found: " + s);
        }
        while (scanner.hasNextDouble()) {
          double d = scanner.nextDouble();
          printWriter.printf("%05f ", d);
          System.out.println("Double found: " + d);
        }        
      }        
    }
    catch (FileNotFoundException e) {
      System.out.println(e); 
    }
    catch (IOException e) {
      System.out.println(e);
    }
  }

}
