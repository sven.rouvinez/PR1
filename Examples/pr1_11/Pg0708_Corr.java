//==============================================================================
//HEIA-FR / Jacques Bapst
//==============================================================================
package corr.s11;

public class Pg0708_Corr {

  public static void main(String[] args) {
    dispChange(6, 5, 2, 1);
  }
  
  //----------------------------------------------------------------------------
  // Affiche toutes les mani�res possibles de changer N francs
  // avec des pi�ces de ka, kb et kc francs
  //----------------------------------------------------------------------------
  public static int dispChange(int N, int ka, int kb, int kc) {
    int nka, nkb, nkc;
    int nkaMax=N/ka, nkbMax=N/kb, nkcMax=N/kc;
    int nb = 0;                               // Compteur du nombre de solutions
    for (nka = 0; nka <= nkaMax; nka++)
      for (nkb = 0; nkb <= nkbMax; nkb++)
        for (nkc = 0; nkc <= nkcMax; nkc++)
          if (kc * nkc + kb * nkb + ka * nka == N) {        // Le compte est bon
            nb++;
            System.out.print(N + " Fr = ");
            if (nkc != 0)
              System.out.print(nkc + " x " + kc + " Fr");
            if (nkb != 0) {
              if (nkc != 0)
                System.out.print("  +  ");
              System.out.print(nkb + " x " + kb + " Fr");
            }
            if (nka != 0) {
              if ((nkc != 0) || (nkb != 0))
                System.out.print("  +  ");
              System.out.print(nka + " x " + ka + " Fr");
            }
            System.out.println();
          }
    System.out.println();
    System.out.println("Il y a " + nb + " possibilit�s de faire " + N + " Fr");
    return nb;
  }
}
