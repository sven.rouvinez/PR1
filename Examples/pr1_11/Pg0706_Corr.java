//==============================================================================
//HEIA-FR / Jacques Bapst
//==============================================================================
package corr.s11;

public class Pg0706_Corr {

  public static void main(String[] args) {
    System.out.println(fA (10, 6));
    System.out.println(fAa(10, 6));
    System.out.println(fAb(10, 6));
    System.out.println(fA (10, 0));
    System.out.println(fAa(10, 0));
    System.out.println(fAb(10, 0));
  }
  
  //----------------------------------------------------------------------------
  // Calcule la somme (n-k) + (n-(k-1)) + (n-(k-2)) + ... + (n-1)
  //                  =  kn - k(k+1)/2  =  k(n - (k+1)/2)
  //----------------------------------------------------------------------------
  public static int fA(int n, int k) {
    int som = 0;
    for (int i=k ; i>0 ; i--)
      som += (n-i);
    return som;
  }
  
  public static int fAa(int n, int k) {
    int som = 0;
    int i=k; 
    while(i>0) {
      som += (n-i);
      i--;
    }
    return som;
  }
  
  public static int fAb(int n, int k) {
    int som = 0;
    int i=k;
    if(i>0) {
      do {
        som += (n-i);
        i--;
      } while(i>0);
    }
    return som;
  }
}
