//==============================================================================
// HEIA-FR / Jacques Bapst
//==============================================================================
package corr.s11;

public class Pg0701b_Corr {

  public static void main(String[] args) {
    System.out.println(np2(0));
    System.out.println(np2(1));
    System.out.println(np2(2));
    System.out.println(np2(3));
    System.out.println(np2(12));
    System.out.println(np2(15));
    System.out.println(np2(16));
  }
  
  //----------------------------------------------------------------------------
  // Calcule la prochaine puissance de deux sup�rieure � k   np2(12) -> 16
  //                                                         np2(16) -> 32
  //----------------------------------------------------------------------------
  public static int np2(int k) {
    int i, s, n=0;

    while (k>1) {
      k /= 2;
      n++;
    }
    s=1;
    for (i=0 ; i<=n ; i++) {
      s *= 2;
    }
    return s;
  }
}
