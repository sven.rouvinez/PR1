//==============================================================================
// HEIA-FR / Jacques Bapst
//==============================================================================
package corr.s11;

public class Pg0701a_Corr {

  public static void main(String[] args) {
    disp(4);
  }
  //----------------------------------------------------------------------------
  // Affiche un triangle d'�toiles de hauteur nL sous la forme       |   *
  //                                                                 |  ***
  //                                              Exemple avec nL=4  | *****
  //                                                                 |*******
  //----------------------------------------------------------------------------
   public static void disp(int nL) {
    char c = '*';
    int nbl, nbe, j;

    for (nbl=0 ; nbl<nL ; nbl++) {
      nbe = nL - nbl - 1;
      for (j=0; j<nbe; j++)
        System.out.print(' ') ;
      for (j=0; j<2*nbl+1; j++)
        System.out.print(c) ;
      System.out.println() ;
    }
  }
}
