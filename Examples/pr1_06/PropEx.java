package pr1_06;

public class PropEx {

  public static void main(String[] args) {
    System.out.println("main starts");
    int[] intArray = { 0, 1, 2 };
    b(intArray, 1);
    b(intArray, 3);
    b(intArray, 0);
    System.out.println("main ends");
  }

  //---------------------------------------------
  //  b
  //---------------------------------------------
  public static void b(int[] intArray, int index) {
    System.out.println("b starts");
    try {
      System.out.println("b step 1");
      c(intArray, index);
      System.out.println("b step 2");
    }
    catch (ArithmeticException e) {
      System.out.println("b catches ArithmeticException " + e);
    }
    catch (Exception e) {
      System.out.println("b catches Exception " + e);
    }
    finally {
      System.out.println("this code is executed in any case - also when the exception is not caught");
    }
    System.out.println("b ends");
  }

  //---------------------------------------------
  //  c
  //---------------------------------------------
  public static void c(int[] intArray, int index) throws Exception //ArithmeticException, IndexOutOfBoundsException
  {
    System.out.println("c starts");
    d(intArray, index);
    System.out.println("c ends");
  }

  //---------------------------------------------
  //  d
  //---------------------------------------------
  // by declaring that the method throws ArithmeticException
  // we do not force the caller to handle the exception  
  // by declaring that the method throws Exception
  // we do  force the caller to handle the exception
  public static void d(int[] intArray, int index) throws Exception //ArithmeticException, IndexOutOfBoundsException 
  {
    System.out.println("d starts");
    int a=10/intArray[index];   // Cette instruction peut g�n�rer une exception
    System.out.println("d ends");
  }
}
