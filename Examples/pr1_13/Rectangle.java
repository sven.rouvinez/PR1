package pr1_13;

import pr1_09.Point2D;

public class Rectangle extends Polygon {
  // protected data members
  protected final double width;
  protected final double height;
 
  // constructor
  public Rectangle(Point2D pt1, double width, double height) {
    // call the super class constructor (as first statement)
    super(new Point2D[] { pt1, new Point2D(pt1.getX(), pt1.getY() + height), 
                          new Point2D(pt1.getX() + height, pt1.getY()),
                          new Point2D(pt1.getX() + height, pt1.getY() + height) });
   
    // store width and height
    this.width = width;
    this.height = height;
  }  
 
  // Drawable implementation
  @Override  
  public void draw() {
    
  }
  
  // redefine abstract methods
 
  @Override
  public double perimeter() {
    return 2 * this.width + 2 * this.height;
  }
  
  @Override
  public double area() {
    return this.width * this.height;
  }
  
  @Override
  public boolean isConvex() {
    // rectangles are always convex
    return true;
  }
  
  @Override
  public boolean areSidesCongruent() {
    return false;
  }
 
  // methods for getting the rectangle dimensions
  public double getWidth() {
    return this.width;
  }
 
  public double getHeight() {
    return this.height;
  }   
}
