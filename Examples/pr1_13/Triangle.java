package pr1_13;

import pr1_09.Point2D;

public class Triangle extends Polygon {
  // constructor
  public Triangle(Point2D pt1, Point2D pt2, Point2D pt3) {
    // call the super class constructor (as first statement)
    super(new Point2D[] { pt1, pt2, pt3 });   
  }
  
  // Drawable implementation
  @Override
  public void draw() {
    
  }
  
  // redefine abstract methods
  
  @Override
  public double perimeter() {
    // not implemented
    return 0.0;
  }
 
  @Override
  public double area() {
    // not implemented
    return 0.0;
  }
  
  @Override
  public boolean isConvex() {
    // triangles are always convex
    return true;
  }
 
  @Override
  public boolean areSidesCongruent() {
    return false;
  }
}
