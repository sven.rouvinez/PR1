package pr1_13;

import pr1_09.Point2D;

public final class Square extends Rectangle {  
  // constructor
  public Square(Point2D pt1, double width) {
    // call the super class constructor (as first statement)
    super(pt1, width, width);
  }  
 
  @Override
  public boolean areSidesCongruent() {
    // the square sides are congruent
    return true;
  }
}
