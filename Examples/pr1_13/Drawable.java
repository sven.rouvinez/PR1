package pr1_13;

public interface Drawable {
    public void draw();
}