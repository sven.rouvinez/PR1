package pr1_13;

import pr1_09.Point2D;

public final class Circle extends Shape {
  // private data members
  private final Point2D center;
  private final double radius;
  
  // constructor
  public Circle(Point2D center, double radius) {
    // call the super class constructor (as first statement)
    // if this statement is omitted, it will be added by default
    super();
  
    // store the radius and center
    this.center = new Point2D(center);
    this.radius = radius;        
  }  

  // Drawable implementation
  @Override  
  public void draw() {
   
  }
 
  // redefine abstract methods

  @Override
  public double perimeter() {
    return 2 * this.radius * Math.PI;
  }
 
  @Override
  public double area() {
    return this.radius * this.radius * Math.PI;
  }
 
}
