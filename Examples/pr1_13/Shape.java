package pr1_13;

// The Shape class is an abstract class.
// It cannot be instantiated and it defines the expected behavior for all 
// Shape subclasses.
// The Shape class does not define any data members since there is no common
// way of representing different shapes. However, all shapes have a perimeter
// and an area, so all subclasses must define method for getting both the 
// perimeter and the area.
public abstract class Shape implements Drawable, Cloneable {
  // abstract methods
  public abstract double perimeter();
  public abstract double area();
}
