package pr1_13;

import pr1_09.Point2D;

// The Polygon class is an abstract class.
// It cannot be instantiated and it defines the expected behavior for all 
// Polygon subclasses.
// It also defines the way vertices are stored for all polygons.
public abstract class Polygon extends Shape {
  // protected data members
  // the vertices of the polygon
  protected final Point2D[] vertices;
    
  // constructor
  protected Polygon(Point2D[] vertices) {
    // deep copy of the vertices
    this.vertices = new Point2D[vertices.length];
    for (int i = 0; i < vertices.length; i++) {
      this.vertices[i] = new Point2D(vertices[i]);
    }
  }
  
  // return the number of vertices
  public int nbrOfVertices() {
    if (vertices == null) {
      return 0;
    }
    return vertices.length;
  }
  
  // define abstract methods that must be implemented by all concrete subclasses

  // returns true if the polygon is convex
  public abstract boolean isConvex();
  
  // return true if the polygon sides are congruent
  public abstract boolean areSidesCongruent();
}
