package pr1_05;

import java.awt.Point;

import proglib.SimpleIO;

public class Array {

  public static void main(String[] args) {
    
    // DECLARATION
    
    // declaration of an array of integer values (primitive type)
    // intArray itself is the reference to the array
    // every entry in the array is an integer
    int[] intArray;
    // note that this syntax is also possible (but int[] intArray is preferred)
    // int intArrayAlternate[];
    
    // declaration of an array of points (a point is an object - reference type)
    // constellation is the reference to the array
    // every entry in the array is a reference to a object of type Point
    Point[] constellation;
    
    // declaration of a matrix of dimension 2 of bytes
    // the first index is the index of the row of the matrix
    // the second index is the index of the column in the row
    // every entry in the matrix is a byte (primitive type)
    byte[][] matrix;
    
    // at this stage, all references to arrays are not initialized
    // they may be initialized to null
    intArray = null;
    constellation = null;
    matrix = null;
    
    // CREATION 
    
    // ARRAY OF PRIMITIVE TYPE ELEMENTS
    
    // arrays must be created before they can be used
    // so you can't use array before it is created
    try {
      System.out.println(intArray[0]);
    }
    catch (NullPointerException e) {
      // we get here because we accessed an intArray element before it was created
      System.out.println("Exception caught: " + e.getCause());
    }
    
    // at creation time, the dimension of the array must be known    
    // int dimension = SimpleIO.readInt("Enter array size");
    int dimension = 3;
    intArray = new int[dimension];
    // modifying dimension does not change the dimension of the array !
    dimension = 4;
    
    // now one can access an element of intArray
    System.out.println("The value of element 0 of intArray is " + intArray[0]);
    
    // simultaneous declaration and initialization
    // without new
    int[] anotherArray = { 1, 2, 3, 4 };
    
    // the following statement is invalid since it can only be used as initializer
    // anotherArray = { 1, 2, 3, 4 };
    
    // another shortcut syntax for declaring and initializing an array simultaneously
    int[] yetAnotherArray = new int[] { 1, 2, 3, 4 };

    // note that it is possible to create arrays without names (anonymous)
    // and to pass them for instance as parameters to a method e.g.
    // f(new int[]{1, 2, 3, 4})
    
    // you can use the length property to determine the size of an array
    // note the use of the [] array index operator for accessing/assigning 
    // a specific element in the array
    // the index must be an int (or a byte, short or char), it cannot be a long
    // or another type
    // the index starts at 0 and is strictly smaller than the length of the array
    for (int i = 0; i < intArray.length; i++) {
      System.out.print(intArray[i] + " ");
    }
    System.out.println();
    
    // accessing an element that is not existing generates an exception
    try {
      System.out.print(intArray[4]);
    }
    catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("Exception caught: error while accessing element " + e.getMessage());
    }
    
    // ARRAY OF REFERENCE TYPE ELEMENTS
    
    // create a array of 4 Point2D 
    constellation = new Point[4];
    
    // now the array of Point2D is created but the elements of the array are
    // NOT yet created so if you access an element you will get an exception
    try {
      System.out.println(constellation[0].getX());
    }
    catch (NullPointerException e) {
      // we get here because we accessed an element before it was created
      System.out.println("Exception caught: " + e.getCause());
    }

    // so what is still needed is to allocate the Point2D themselves
    for (int i = 0; i < constellation.length; i++) {
      constellation[i] = new Point(1, 1); 
    }
    // now you can access a specific element
    System.out.println(constellation[0].getX());

    // declaration and initialization of an array of strings
    String[] stringArray = new String[5];
    // the same applies as for any object, the array of strings is created but the
    // elements (string) of the array are NOT yet created
    try {
      System.out.println(stringArray[0].length());
    }
    catch (NullPointerException e) {
      // we get here because we accessed an element before it was created
      System.out.println("Exception caught: " + e.getCause());
    }
    // create the string elements
    for (int i = 0; i < constellation.length; i++) {
      stringArray[i] = new String(); 
    }
    // now you can access a specific element
    System.out.println(stringArray[0].length());
    
    // ARRAY OF MULTIPLE DIMENSIONS
    
    // create a matrix of (2 x 3) bytes
    matrix = new byte[2][3];
    
    // alternatively, one can use the shortcut syntax to create and initialize an array
    matrix = new byte[][] { {1, 2, 3}, {4, 5, 6}};
            
    // on a two-dimensional array, you can also use the length property
    // the length property determines the number of rows
    // the length property can also be applied to each row
    System.out.println("Number of rows is " + matrix.length);
    for (int i = 0; i < matrix.length; i++) {
      System.out.println("Number of elements in row " + i + " is " + matrix[i].length);
      for (int j = 0; j < matrix[i].length; j++) {        
        System.out.print(matrix[i][j] + " ");
      }
      System.out.println();
    }
         
    // in fact, a multi-dimensional array is not a matrix
    // it is rather an array of array, meaning that 
    // each "row" can have a different dimension
    matrix = new byte[][] { {1}, {2, 3}, {4, 5, 6} };
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        System.out.print(matrix[i][j] + " ");
      }
      System.out.println();
    }
    
    // example of an array with 3 dimensions
    // it is enough if the first dimension is set at initialization time
    // if more dimensions are set at initialization, they must be defined
    // in the left to right order
    int[][][] dim3Array = new int[3][][];
    dim3Array = new int[3][2][];
    // then the array corresponding to the "last" dimension can be defined dynamically
    for (int i = 0; i < dim3Array.length; i++) {
      for (int j = 0; j < dim3Array[i].length; j++) {
        dim3Array[i][j] = new int[1];
      }
    }
    
    // PASSING ARRAYS AS ARGUMENTS
    
    // without side effect, intArray is left unchanged 
    intArray = new int[]{1, 5, 3, 2};
    int max = max(intArray);
    System.out.println("Max value is " + max);
    
    // with side effect, intArray is modified 
    modify(intArray);
    
    // note that you may also change references in multidimensional arrays
    int[][] dim2Array = new int[3][];
    allocate(dim2Array);
    
    // COPYING AN ARRAY
    
    // copying an array is not the same as copying the reference to the array
    
    // intArray2 contains the reference to the same array as intArray
    int[] intArray2 = intArray;
    
    // for copying an array, you must create an array and call the arraycopy method
    // which is a system method used for copying efficiently data from one array to another one
    intArray2 = new int[intArray.length];
    System.arraycopy(intArray, 0, intArray2, 0, intArray.length);
    
    // ARRAYS CLASS
    // the java.util.Arrays class provides various static helper methods for
    // searching, sorting, comparing, and filling arrays
    // examples: binarySearch, equals, copy, fill, sort, etc..
    
    // other array manipulations
    // copying only a range of the array, in this example the two first elements
    int[] intArray3  = java.util.Arrays.copyOfRange(intArray, 1, 2);
    
    // comparing two arrays using the equals() method
    System.out.println(java.util.Arrays.equals(intArray, intArray2));
    System.out.println(java.util.Arrays.equals(intArray, intArray3));
   
  }

  // method without side effect - the array passed as argument is not modified
  private static int max(int[] array) {
    int max = Integer.MIN_VALUE;    
    for (int value : array) {
      if (value > max) {
        max = value;
      }
    }
    return max;
  }
  
  // method with side effect - the array passed as argument is modified
  private static void modify(int[] array) {
    for (int i = 0; i < array.length; i++) {
      array[i]--;
    }
  }
  
  // method with side effect - the array passed as argument is modified
  // in particular some references are modified (allocated)
  private static void allocate(int[][] dim2Array) {
    for (int i = 0; i < dim2Array.length; i++) {
      dim2Array[i] = new int[2];
    }
  }
  
}
