package pr1_14;

class OuterWithInnerClass {
  private int instanceDataMember = 0;
  private static int classDataMember = 0;
  
  // inner class
  class InnerClass {
    // inner class cannot have static data members ! unless they are final
    // static int innerClassDataMember = 0;   
    static final int innerClassConstant = 0;
    
    public void publicMethod() {
      // here we can access the outer class data members
      int number = instanceDataMember + 1;
      instanceDataMember++;
      number = classDataMember + 1;
      System.out.println("This is a public method of the inner class: " + number);
    }
    
    private void privateMethod() {
      int number = instanceDataMember + 1;
      instanceDataMember++;
      System.out.println("This is a private method of the inner class: " + number);
    }
  }
  
  // another inner class
  class OtherInnerClass {
    public void publicMethod() {
      // here we can access the outer class and other inner class data members
      int number = InnerClass.innerClassConstant;
      System.out.println("This is a public method of another inner class: " + number);
    }
  }
  
  // Accessing the inner class from within an outer class method
  void displayInnerClass() {
    // both public and private data members can be accessed
    InnerClass innerInstance = this.new InnerClass();
    innerInstance.publicMethod();
    innerInstance.privateMethod();
  }
}
  
public class OuterWithInnerClassTest {
  
  public static void main(String args[]) {
    // Instantiate the outer class 
    OuterWithInnerClass outer = new OuterWithInnerClass();

    // Accessing the displayInnerClass() method
    outer.displayInnerClass();
    
    // instantiate the inner class from the outer class instance
    OuterWithInnerClass.InnerClass inner1 = outer.new InnerClass();
    inner1.publicMethod();
    
    OuterWithInnerClass.InnerClass inner2 = outer.new InnerClass();
    inner2.publicMethod();
    
    // creating an instance of the inner class without an instance of 
    // the outer class is not possible
    // OuterWithInnerClass.InnerClass inner3 = new OuterWithInnerClass.InnerClass();
    
    // here access to private members is not allowed
    // inner1.privateMethod();
  }
}
