package pr1_14;

public class OuterWithLocalInnerClass {
  // the instance data members from the outer class can be accessed
  // in the local inner class (even if they are not final)
  int outerInstanceVariable = 1;
  
  // instance method of the outer class
  // outerParameter and localVariable must be final or effectively final 
  // for being usable in MethodInnerClass
  protected void protectedMethod(int outerParameter) {
     final int localVariable = outerInstanceVariable;

     // method-local inner class
     // access modifiers are not allowed for local inner classes
     class MethodInnerClass {
       // static data members are not allowed
       // static int staticDataMember = 0;
       
       public void publicMethod(int parameter) {
         // here we can access the final local variable and parameter of protectedMethod
         // and the instance variable of the outer class
         System.out.println("This is a public method local inner class: instance variable is " 
                            + outerInstanceVariable + " and local is " + localVariable + 
                            " with parameter " + outerParameter);
         parameter++;
       }
       
       private void privateMethod(final int parameter) {
         // here we can access the final local variable and parameter of protectedMethod
         // and the instance variable of the outer class
         System.out.println("This is private method local inner class: instance variable is " 
                            + outerInstanceVariable + " and local is " + localVariable + 
                            " with parameter " + outerParameter);
       }
     } // end of inner class
     
     // the statement below makes localVariable effectively non final and thus
     // creates a compilation error for both publicMethod() and privateMethod() above
     // localVariable++;
     
     // the same applies to outerParameter
     // outerParameter++;
     
     // Accessing the local inner class
     MethodInnerClass localInner = new MethodInnerClass();
     localInner.publicMethod(2);
     localInner.privateMethod(3);
  }
  
  public static void main(String[] args) {
    // instantiate the outer class    
    OuterWithLocalInnerClass outer = new OuterWithLocalInnerClass();
    outer.outerInstanceVariable++;
    outer.protectedMethod(2);
    outer.outerInstanceVariable++;
    outer.protectedMethod(3);
    
    // here the MethodInnerClass methods are not accessible
    
    // be careful, accessing non final data members is allowed but produced 
    
  }

}
