package pr1_14;

// the syntax for using anonymous classes is
// when using an (abstract) class
// new (abstract)_class_name (<arguments>) { <anonymous class body> }
//
// when using an interface
// new interface_name { <anonymous class body> }
//
// in both cases, it means that the declaration and the instantiation 
// of the anonymous class are combined
//
// anonymous classes act like local classes with the same restrictions
// in addition:
//  - no constructor other than the default one (no name !)
//   -> use instance initializer
//
// to be used when only a single instance of the class will required
//
// an important difference as compared to named classes is that anonymous
// classes cannot have constructors (well they are not named...)
// but they can have an initialization 
abstract class AbstractClassForOuter {
  public abstract void method();
}

interface InterfaceForOuter {
  public abstract void method();
}

class ConcreteClassA extends AbstractClassForOuter {
  @Override
  public void method() {
    System.out.println("This is an example of anonymous inner class (abstract)");
  }
}

class ConcreteClassB implements InterfaceForOuter {
  @Override
  public void method() {
    System.out.println("This is an example of anonymous inner class (abstract)");
  }
}

public class OuterWithAnonymousInner {

  // define the IntHolder interface
  public static interface IntHolder {
    public int getValue();
  }
  
  public static void main(String[] args) {
    // create an instance of the concrete class
    ConcreteClassA concreteClassInstanceA = new ConcreteClassA();
    concreteClassInstanceA.method();
    ConcreteClassB concreteClassInstanceB = new ConcreteClassB();
    concreteClassInstanceB.method();
    
    // create an anonymous class instance (using abstract class)
    AbstractClassForOuter anonymousIter1 = new AbstractClassForOuter() {
      public void method() {
        System.out.println("This is an example of anonymous inner class (abstract)");
      }
    };
    anonymousIter1.method();
    // using a second instance of the same anonymous class is not optimal
    AbstractClassForOuter anonymousIter2 = new AbstractClassForOuter() {
      public void method() {
        System.out.println("This is an example of anonymous inner class (abstract)");
      }
    };    
    anonymousIter2.method();
    
    // create another anonymous class instance (using interface)
    InterfaceForOuter anonymousIter3 = new InterfaceForOuter() {
      public void method() {
        System.out.println("This is another example of anonymous inner class (interface)");
      }
    };    
    anonymousIter3.method();
    
    // call the outer method with an instance of the interface
    outerMethod(concreteClassInstanceB);
    outerMethod(new InterfaceForOuter() {
      public void method() {
        System.out.println("This is an example of anonymous inner class (as argument)");
      } 
    });

    // initialize an array to anonymous class instances
    final int NBR_OF_ITERATIONS = 10;
    IntHolder[] intHolders = new IntHolder[NBR_OF_ITERATIONS];
    
    for (int i = 0; i < NBR_OF_ITERATIONS; i++) {
      final int fi = i;
      intHolders[i] = new IntHolder() {
        public int getValue() { return fi; }
      };
    }
    
    // call the anonymous class instance in another loop
    for (int i = 0; i < NBR_OF_ITERATIONS; i++) {
      System.out.println("IntHolder[" + i +"] returns " + intHolders[i].getValue());
    }
    
    // a local (anonymous) class can only access local variables if they are
    // final or effectively final
    // the code below is not allowed because "i" is neither final nor effectively final
    // for (int i=0; i<10; i++) {
    //  intHolders[i] = new IntHolder() {
    //    public int getValue() { return i; }
    //  };
    //}
    
    // this code is allowed because "i" is effectively final in the scope where
    // it is declared (it is not modified after its creation) 
    {
      int i = 0;
      intHolders[0] = new IntHolder() {
        public int getValue() { return i; }
      };
    }
    // this code is not allowed because "i" is not effectively final
    // in the scope where it is declared
    //{
    //  int i = 0;
    //  intHolders[0] = new IntHolder() {
    //    public int getValue() { return i; }
    //  };
    //  i = 1;
    //}  
  }
  
  // declare a method taking the interface as argument
  public static void outerMethod(InterfaceForOuter f) {
    f.method();
  }
}
