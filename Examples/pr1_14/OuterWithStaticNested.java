package pr1_14;

public class OuterWithStaticNested {
  private final int instanceDataMember = 0;
  private static int classDataMember = 0;
  
  // static nested class
  private static class StaticNested { 
    private int instanceInnerDataMember = 0;
    private static int classInnerDataMember = 0;
    
    public void publicMethod() {
      // from the inner class, we can access the outer class static data members
      int number = classDataMember + 1;
      classDataMember++;
      // on the contrary, we cannot access any instance variable of the outer class
      // number = instanceDataMember + 1;
      System.out.println("This is a public method of the static nested class: " + number);
    }
    
    private void privateMethod() {
      // from the inner class, we can access the outer class static data members
      int number = classDataMember + 1;
      classDataMember++;
      // on the contrary, we cannot access any instance variable of the outer class
      // number = instanceDataMember + 1;
      System.out.println("This is a private method of the static nested class: " + number);
    }
    
    private static void privateStaticMethod() {
      // from the inner class, we can access the outer class static data members
      int number = classDataMember + 1;
      classDataMember++;
      // on the contrary, we cannot access any instance variable of the outer class
      // number = instanceDataMember + 1;
      System.out.println("This is a private method of the static nested class: " + number);
    } 
  }
  
  static class OtherStaticNested {
    public void publicMethod() {
      // here we can access the other static nested classes
      StaticNested.privateStaticMethod();
    }    
  } 
  
  public static void main(String args[]) {
    // the static nested class can be created without an instance of the outer class
    OuterWithStaticNested.StaticNested nested = new OuterWithStaticNested.StaticNested();
    // from the outer class, we can access both private and public member
    // of the inner class (both static and non static)
    nested.publicMethod();
    nested.privateMethod();
    int number = StaticNested.classInnerDataMember;
    number = nested.instanceInnerDataMember;
    System.out.println(number);
    StaticNested.privateStaticMethod();
    
    // using a static nested class allows improved encapsulation (access to static data members)
    // this should be balanced against lisibility and other considerations
  }
}
