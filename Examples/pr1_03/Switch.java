package pr1_03;

public class Switch {

  public static void main(String[] args) {
    // switch may be used for making if/else-if
    // the following if/else-if statements may be replaced by the switch statement below
    {
      int x = 3;
      if(x == 1) {
        System.out.println("x == 1");
      }
      else if(x == 2) {
        System.out.println("x == 2");
      }
      else {
        // default value
        System.out.println("x default value");
      }
    }
    
    {
      int x = 3;
      switch(x) {
        case 1 :     
        {
          System.out.println("x == 1");
        }
        break;
       
        case 2 :
        {
          System.out.println("x == 2");
        }
        break;
       
        default :
        {
          System.out.println("x default value");
        }
      }
    }
    
    // some remarks about the switch statement
    
    // remark 1:
    // the constants used in the case must be of the same type as the one in the switch expression
    // remark 2:
    // one can only test the equality with a switch/case statement
    // remark 3:
    // unless one has a very good reason for doing it differently, one should always add breaks after each
    // case and avoid "fallthrough'
    // remark 4:
    // for handling multiple cases as one, one can use multiple case statements 
    // remark 5:
    // each case value must be unique
    
    {
      byte x = 3;
      switch (x) {
        case 2: // this is ok
        {
          System.out.println("x == 2");
        }
        break;
          
        // case 'b': // remark 1: not accepted, this would cause a compilation 
                     // error since 'b' is not a 'byte'
        
        // case 512: // remark 2: not accepted, this would cause a compilation 
                     // error since '512' is too large for a 'byte'
        
        case 3:
          System.out.println("x == 3");
          // remark 3: no break !
          
        case 4:
        case 5:
          // remark 4: x == 4 and x == 5 are handled in the same way
          // when x == 3, this statement will also be executed
          System.out.println("x == 3 || x == 4 || x == 5");
          break;
          
        // case 5: // remark 5: not accepted, this would cause a compilation
                   // error since the 'case 5' is already used
        
        default:
          System.out.println("x default value");
      }
    } 
    
    // use a switch statement for implementing the following behavior
    // Problem statement:
    // Given an integer x, perform the following conditional actions:
    //  If x is odd, print "Case1"
    //  If x is even and in the inclusive range of 2 to 6, print "Case2"
    //  If x is even and in the inclusive range of 7 to 21, print "Case3"
    //  If x is even and greater than 21, print "Case4"

  }
}
