package pr1_03;

public class While {

  public static void main(String[] args) {
    // while loops
    // to be used preferably when the number of iterations is not known 
    // but rather based on a conditional expression
    
    // some remarks about while loops
    // remark 1: declaration statements are not allowed as part of the conditional
    //           expression
    //           as for any other block, the scope of the statements
    //           within the while loop is limited to this block, variables used in 
    //           the conditional expression must thus be declared outside of the loop
    // remark 2: if the conditional expression evaluates to false on the first call
    //           the statements in the while block are never executed
    // remark 3: one can implement an infinite loop with a while (true) statement
    //           (of course, there should be a break condition within the loop)
    
    //while (boolean cond = true) {} // remark 1: not accepted since cond is not 
                                     // declared prior to the while loop
    
    boolean cond = false;
    while (cond)
    {
      // remark 2: this statement is never executed
      System.out.println("This will not print on the console");
    }
    
    int x = 0;
    while (true)
    {
      x++;
      if (x > 10)
      {
        break;
      }
      System.out.println("Value of x is " + x);
    }
    
    // it is possible to use while loops similarly to for loops
    {
      int intArray[] = {2, 11, 45, 9};
      // i starts with 0 as array index starts with 0 too
      int i = 0;
      while (i < 4)
      {
        System.out.println(intArray[i]);
        i++;
      }
    }
    
    // do..while() loops
    
    // difference as compared to while() loop through an example
    // you want to have a user enter a character repeatedly until it enters a specific character
    // in this scenario, you need the user to enter at least ONE character
    
    // remark 1: str is used in the conditional expression and must be declared outside of the loop
    String str = "";
    java.util.Scanner s = new java.util.Scanner(System.in);
    do {
      // read a character from the keyboard
      str = s.nextLine();
      
      // instructions
      
      
    } while (str.charAt(0) != 'n');

    // writing this as a while loop is more complex
    // read a character from the keyboard
    str = s.nextLine();
    // instructions
    
    while (str.charAt(0) != 'n') {
      // read a character from the keyboard
      str = s.nextLine();
      
      // instructions
            
    } 
    s.close();
  }
}
