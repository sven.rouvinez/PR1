package pr1_03;

import java.util.Scanner;

public class Continue {

  public static void main(String[] args) {
    // continue statement in for() loop
    // remark 1: the ++i statement is executed also in the case of the
    //           continue statement
    for (int i = 1; i <= 10; ++i) {      
      if (i > 4 && i < 9) {
         continue;
      }      
      System.out.println(i);
    } 

    // example with a for() loop
    // the loop sums numbers entered by the user except when the number is negative
    double number, sum = 0.0;
    Scanner input = new Scanner(System.in);
   
    for (int i = 1; i < 6; ++i) {
      System.out.print("Enter a number: ");
      number = input.nextDouble();
       
      if (number < 0.0) {
        continue;
      }
     
      sum += number;
    }
    System.out.println("Sum = " + sum);
    input.close();
    
    // in case of nested loops, continue skips the execution of remaining statement(s)
    // of innermost for, while and do..while loop.
    // There is another form of continue statement, labeled continue, 
    // that can be used to skip the execution of statement(s) that lies inside the outer loop.
  
    // Remark 1: In this case, only the conclusion part of the outer loop is executed
  
    // The use of labeled continue is often discouraged as it makes your code hard to understand.
    // If you are in a situation where you have to use labeled continue, refactor your code and 
    // try to solve it in a different way to make it more readable
    {
      int i = 1;
      int j = 1;
      label:
      for ( ; i < 6; ++i) {
        for (j = 1; j < 5; ++j) {
          if (i == 3 || j == 2) {
            continue label;
          }
          System.out.println("i: " + i + " j: " + j); 
        }
      }
    }
  }
}
