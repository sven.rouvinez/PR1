package pr1_03;

public class If {

  public static void main(String[] args) {
    // use if/if-else statements for implementing the following behavior
    // Problem statement:
    // Given an integer x, perform the following conditional actions:
    //  If x is odd, print "Case1"
    //  If x is even and in the inclusive range of 2 to 6, print "Case2"
    //  If x is even and in the inclusive range of 7 to 21, print "Case3"
    //  If x is even and greater than 21, print "Case4"

  }

}
