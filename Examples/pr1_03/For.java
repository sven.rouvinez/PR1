package pr1_03;

public class For {

  public static void main(String[] args) {
    // for loops are most useful when the number of iterations is known
    // or can be computed easily
    
    // the syntax is for (initialisation; condition; conclusion){}
    // every part (initialization, condition, or conclusion) is optional, but semi-colons are mandatory
    // every part is independant from each other
    
    // remark 1: in contrast to while loops, for loops have an initialization section
    //           several variables of the same type can be declared in this initialization section
    //
    // remark 2: the conclusion part can also contain multiple statements separated by a comma
    //           the statements in the conclusion part are executed 
    //
    // remark 3: the condition part must be written as a single conditional expression (evaluated to
    //           a boolean value
    //
    
    // simple example
    for (int i = 0, j = 0; i < 10; i++, j++)
    {
      System.out.println("Value of i and j is " + i + ", " + j);
    }
    
    // while and for: the two loops below are identical 
    {
      int i = 0;
      while (i < 10) {
        System.out.println(i);
        i++;
      }
    }
    {
      int i = 0;
      for ( ; i < 10; ) {
        System.out.println(i);
        i++;
      }
    }
    
    // infinite loop for reading a character (to be compared to versions using while() loops)
    java.util.Scanner s = new java.util.Scanner(System.in);
    for (;;) 
    {
      // read a character from the keyboard
      String str = s.nextLine();
      if (str.charAt(0) == 'n')
      {
        break;
      }
    }
    s.close();
  
    // nested loops
    // A for loop can contain any kind of statement in its body,
    // including another for loop (called inner loop).
    // The inner loop must have a different name for its loop counter
    // variable so that it will not conflict with the outer loop
  
    // simple example: 
    //  question 1: find what it does before executing the code !
    //  question 2: is there a way to write the same code without the inner loop ?
    for (int i = 1; i <= 4; i++) {
      for (int j = 1; j <= 5; j++) {
        System.out.print("*");
      }
      System.out.println();
    }
  
    // example where the inner loop has a dependancy on the outer loop counter variable
    // question 1: find what it does before executing the code !
    // question 2: is there a way to write the same code without the inner loop ?
    for (int i = 1; i <= 6; i++) {
      for (int j = 1; j <= i; j++) {
        System.out.print("*");
      }
      System.out.println();
    }
    
    // be careful when using loop counters, small errors may produce unexpected results !
    // question 1: what is the behavior and output of the code below ?
    for (int i = 1; i <= 4; i++) {
      for (int j = 1; i <= 5; j++) {
        System.out.print("*");
      }
      System.out.println();
    }
    
    // write a code with nested loops that produce the following output
    // ....1
    // ...22
    // ..333
    // .4444
    // 55555
    // 
    // hint: you may need to use more than one inner loop
    
    
    // write a code that given a string prints first the characters of the string 
    // at even positions, then prints a space, and finally prints the characters
    // of the string at odd positions
    
  }
}
