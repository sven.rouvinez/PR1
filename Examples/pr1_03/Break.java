package pr1_03;

public class Break {

  public static void main(String[] args) {
    
    // example for break statement in a infinite while loop
    // the loop sums numbers entered by the user until one number is negative
    Double number, sum = 0.0;
    java.util.Scanner input = new java.util.Scanner(System.in);
    
    while (true) {
      System.out.print("Enter a number: ");
      number = input.nextDouble();
       
      if (number < 0.0) {
        break;
      }
       
      sum += number;
    }
    System.out.println("Sum = " + sum);
    input.close();
    
    // in case of nested loops, break terminates the innermost loop.
    // in the following example, break the break statement terminates the innermost 
    // for loop, and control jumps to the outer loop.
    for (int i = 0; i < 10; i++) {
      for (int j = 0; j < 10; j++) {
        System.out.println("i: " + i + " j: " + j);
        if (j > 5) {
          break;
        }          
      }
    }
    
    // The break statements above are unlabeled form of break statement, 
    // which terminates the innermost for, while, do..while and switch statement. 
    // There is another form of break statement, labeled break, 
    // that can be used to terminate any of the outer loops
    // When break "label" executes, control jumps to the statement following 
    // the labeled statement "label".
    {
      first:
      for (int i = 1; i < 5; i++) {
        second:
        for (int j = 1; j < 3; j ++ ) {
          System.out.println("i: " + i + " j: " + j);
       
          if (i > 2)
          {
            // jump to the statement following the "first" label
            // so exit the two loops (inner and outer)
            
            // hint: modify the label and observe the difference
            break first;
          }
        }
      }
    }
    
    
  }
}
