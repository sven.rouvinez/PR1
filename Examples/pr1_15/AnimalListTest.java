package pr1_15;

public class AnimalListTest {

  public static void main(String[] args) {
    // create some Animal instances
    Animal a1 = new Cat(3, 5, Animal.Sex.MALE);
    Animal a2 = new Dog("Medor", 4, 6, Animal.Sex.FEMALE);
    Animal a3 = new Cat(5, 4, Animal.Sex.MALE);
    Animal a4 = new Dog("Milou", 11, 7, Animal.Sex.MALE);
    Animal a5 = new Reptile(2, 1, Animal.Sex.FEMALE);
    Animal a6 = new Human("Alexandre", 25, 65, Animal.Sex.MALE);
    Animal a7 = new Human("Nathalie", 35, 52, Animal.Sex.FEMALE);
    
    // create an animal list
    AnimalList animalList = new AnimalList(4);
    animalList.add(a1);
    animalList.add(a2);
    animalList.add(a3);
    animalList.add(a4);
    animalList.add(a5);
    animalList.add(a6);
    animalList.add(a7);
    
    // Print all dogs
    System.out.println("-------------- test=dogs");
    AnimalList foundAnimalList0 = AnimalList.findDogs(animalList);
    for (int i = 0; i < foundAnimalList0.size(); i++) {
      System.out.println(foundAnimalList0.get(i));
    }

    // Print members of the list older than 5
    System.out.println("-------------- test=older than 5");
    AnimalList foundAnimalList1 = AnimalList.findAnimalsOlderThan(animalList, 5);
    for (int i = 0; i < foundAnimalList1.size(); i++) {
      System.out.println(foundAnimalList1.get(i));
    }

    // Now use static inner classes for implementing search criteria
    System.out.println("-------------- test=male between 2 and 5");
    AnimalList.CheckAnimal check2 = new AnimalList.CheckMaleBetween2And5();
    AnimalList foundAnimalList2 = AnimalList.findAnimals(animalList, check2);
    for (int i = 0; i < foundAnimalList2.size(); i++) {
      System.out.println(foundAnimalList2.get(i));
    }
    System.out.println("-------------- test=female");
    AnimalList.CheckAnimal check3 = new AnimalList.CheckFemale();
    AnimalList foundAnimalList3 = AnimalList.findAnimals(animalList, check3);
    for (int i = 0; i < foundAnimalList3.size(); i++) {
      System.out.println(foundAnimalList3.get(i));
    }
    
    // A more elegant and flexible solution is the use of anonymous classes
    // for finding persons "on the fly"
    System.out.println("-------------- test=younger than 4");
    AnimalList.CheckAnimal check4 = new AnimalList.CheckAnimal() {
      public boolean test(Animal a) {
        return a.age() <= 4;
      }
    };
    AnimalList foundAnimalList4 = AnimalList.findAnimals(animalList, check4);
    for (int i = 0; i < foundAnimalList4.size(); i++) {
      System.out.println(foundAnimalList4.get(i));
    }
    
    // An even more powerful solution is to use lambda expressions.
    // This is possible because the CheckPerson interface is a functional interface
    // (a functional interface is any interface that contains only one abstract method
    //  and possibly one or more default or static methods).
    // In this case, we may omit the name of the method that we want to implement and
    // simply implement the abstract method as a lambda expression.
    System.out.println("-------------- test=younger than 5");
    AnimalList foundAnimalList5 = AnimalList.findAnimals(animalList, (Animal a) -> a.age() <= 5);
    for (int i = 0; i < foundAnimalList5.size(); i++) {
      System.out.println(foundAnimalList5.get(i));
    }
    
    // Yet another even simpler solution is to omit the definition of the interface
    // and to use one of the standard functional interfaces defined by the Java API.
    // In our case, the method that we need to implement takes one argument and
    // returns a boolean value, which is implemented using the Predicate<T> 
    // generic interface defined as
    // interface Predicate<T> {
    //   boolean test(T t);
    // }
    System.out.println("-------------- test=Male");
    AnimalList foundAnimalList6 = AnimalList.findAnimalsWithPredicate(animalList, (Animal a) -> a.gender() == Animal.Sex.MALE);
    for (int i = 0; i < foundAnimalList6.size(); i++) {
      System.out.println(foundAnimalList6.get(i));
    }
    
    // Now say that you have an existing method that you want to use as  
    // as implementation of the abstract method of the functional interface Predicate
    // In this case, we may refer to the method as instance of the Predicate.
    // We may use class or instance methods.
    System.out.println("-------------- test=Cat");
    AnimalList foundAnimalList7 = AnimalList.findAnimalsWithPredicate(animalList, AnimalListTest::isCat);
    for (int i = 0; i < foundAnimalList7.size(); i++) {
      System.out.println(foundAnimalList7.get(i));
    }    
  }
  
  
  static boolean isCat(Animal a) {
    return a instanceof Cat;
  }

}
