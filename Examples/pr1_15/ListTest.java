package pr1_15;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class ListTest {

  public static void main(String[] args) {
    // create some Animal instances
    Animal a1 = new Cat(3, 5, Animal.Sex.MALE);
    Animal a2 = new Dog("Medor", 4, 6, Animal.Sex.FEMALE);
    Animal a3 = new Cat(5, 4, Animal.Sex.MALE);
    Animal a4 = new Dog("Milou", 11, 7, Animal.Sex.MALE);
    Animal a5 = new Human("Alexandre", 25, 65, Animal.Sex.MALE);
    Animal a6 = new Human("Nathalie", 35, 52, Animal.Sex.FEMALE);
    Animal a7 = new Reptile(2, 1, Animal.Sex.FEMALE);
    
    // create a list of animals
    List<Animal> animalList = new List<Animal>(4);
    animalList.add(a1);
    animalList.add(a2);
    animalList.add(a3);
    animalList.add(a4);
    animalList.add(a5);
    animalList.add(a6);
    animalList.add(a7);
    
    // create a list of mammals (valid since a1..a6 are all mammals)
    List<Mammal> mammalList = new List<Mammal>(4);
    mammalList.add((Mammal) a1);
    mammalList.add((Mammal) a2);
    mammalList.add((Mammal) a3);
    mammalList.add((Mammal) a4);
    mammalList.add((Mammal) a5);
    mammalList.add((Mammal) a6);
    
    // create a list of dogs
    List<Dog> dogList = new List<Dog>(2);
    dogList.add((Dog) a2);
    dogList.add((Dog) a4);
    
    // Use the processElements method, which is defined as a generic method with
    // type parameter T
    // Since T is not bounded, the type argument can be anything !
    // process animals with predicate
    System.out.println("-------------- test:younger than 20");
    // in this call, the type argument T is defined to be Animal
    processElements(animalList, p -> p.age() <= 20, p -> System.out.println(p));
    
    System.out.println("-------------- test:male mammals");
    // in this call, T is defined to be Mammal 
    processElements(mammalList, p -> p.gender() == Animal.Sex.MALE, p -> System.out.println(p));
    
    // use method with specific type parameters, where the type parameter is
    // specified to be a specific type (here Mammal)
    System.out.println("-------------- Test 3");
    // in this call, we expect the type argument to be Mammal
    // so this call is acceptable
    processMammalsOnly(mammalList, p -> p.gender() == Animal.Sex.MALE, p -> System.out.println(p));
    // if T is not Mammal, then the call is not accepted 
    // this is true even if T inherits from Mammal since List<Dog> is NOT a subclass of List<Mammal>
    // processMammalsSpecific(dogList, p -> p.gender() == Animal.Sex.MALE, p -> System.out.println(p));
        
    // Use bounded type parameters
    
    // This is useful for restricting the types that can be used 
    // as type arguments in a parameterized type. So for instance, if we design a method that
    // applies only to Mammal and sub-classes, the definition of the method should use a bounded type.
    
    System.out.println("-------------- Test 4");
    // in this call, T is defined to be Mammal
    processMammalsExtended(mammalList, p -> p.nbrOfLegs() == 4, p -> System.out.println(p));
    
    System.out.println("-------------- Test 5");
    // the following call is with T defined as Dog (which is a sub-class of Mammal)
    // so the method name() may be used
    processMammalsExtended(dogList, p -> p.name.equals("Medor"), p -> System.out.println(p));
    
    // so the following call would produce a compilation error since the name() method 
    // is not defined for the type Mammal
    // processMammalsExtended(mammalList, p -> p.name().equals("Medor"), p -> System.out.println(p));
    // also Mammal is defined to be the upper bound for the call to processMammals
    // so the following call would not compile
    // processMammalsExtended(animalList, p -> p.gender() == Animal.Sex.MALE, p -> System.out.println(p));
    
    // Wildcards
    // Methods defined with a wildcard argument can be called with any type of type argument
    processElementsOfAnyType(animalList);
    processElementsOfAnyType(dogList);
    
    // Upper Bounded Wildcards
    // This is useful to relax the restrictions on a variable. 
    // For example, say you want to write a method that takes an argument that can be
    // on List<Dog>, List<Cat>, and List<Mammal> 
    // You can achieve this by using an upper bounded wildcard.
    processDogs(dogList);    
  }

  // static method used for processing a list of any type T elements
  // the Predicate functional interface takes one element of type T as argument
  //  and returns a boolean value
  // the Consumer functional interface takes one element of type T as argument
  //  and returns void
  // In this method, we may use all methods defined by the T (upper-bound) class
  public static<T> void processElements(List<T> list, 
                                        Predicate<T> check, 
                                        Consumer<T> block) {
    for (int i = 0; i < list.size(); i++) {
      // Here item is an untyped item of the list
      // We cannot use any method of the T type that 
      // would for instance be Animal or Mammal.
      // However the functional interface method can make use of the type argument,
      // so they can use the method age() when the type argument T is Animal or 
      // the method name() when the type argument is Dog
      T item = list.get(i);
      if (check.test(item)) {        
        block.accept(item);
      }
    }
  }
  
  // If one would like to write a method with arguments of a specified type,
  // you may specify specific type parameters.
  public static void processMammalsOnly(List<Mammal> list, 
                                        Predicate<Mammal> check, 
                                        Consumer<Mammal> block) {
    for (int i = 0; i < list.size(); i++) {
      // The type parameter is specified to be Mammal.
      // So we can use any method on item defined in the Mammal class 
      // and in its super-classes.
      Mammal item = list.get(i);
      if (check.test(item)) {
        block.accept(item);
      }
    }
  }
  
  // The restriction with the specific type parameter is that the 
  // type argument needs to be exactly the specified type parameter - 
  // it cannot be a subclass of it since List<Dog> does not inherit from List<Mammal>.
  // For allowing the use of subclasses as type argument, 
  // we may use bounded type parameters.
  // We may for instance define a method for processing all mammals
  // (and specialized mammals - class inherited from Mammal)
  public static<T extends Mammal> void processMammalsExtended(List<T> list, 
                                                              Predicate<T> check, 
                                                              Consumer<T> block) {
    for (int i = 0; i < list.size(); i++) {
      // T is specified by its upper bound, which is Mammal
      // so we can use any method on item defined in the Mammal class
      // and in its super-classes
      T item = list.get(i);
      if (check.test(item)) {
        block.accept(item);
      }
    }
  }  
  
  // If you write a method that does not depend on the type T, you may use
  // an unbounded wildcard ?, that means any type. In this case, you may only
  // use the methods of the Object class in the method.
  // Using OBject instead of ? is not the same as seen above (List<Animal> does not
  // inherit from List<Object>)
  public static void processElementsOfAnyType(List<?> list) {
    for (int i = 0; i < list.size(); i++) {
      // T is specified by its upper bound, which is Mammal
      // so we can use any method on item defined in the Mammal class
      // and in its super-classes
      Object item = list.get(i);
      System.out.println(item);
    }
  }
  
  // If we want to relax the restrictions on a variable, we may use 
  // upper bounded wildcards. 
  // For example, say you want to write a method that takes an argument that can be
  // on List<Chihuahua> and List<Dog>
  // You can achieve this by using the upper bounded wildcard <? extends Dog>
  public static void processDogs(List<? extends Dog> list) {
    for (int i = 0; i < list.size(); i++) {
      // the element in the list is specified by the upper bounded wildcard, which is Dog
      // so we can use any method on item defined in the Dog class and in its super-classes
      Dog dog = list.get(i);
      if (dog.name().equals("Medor")) {
        System.out.println(dog);
      }
    }
  }
}
