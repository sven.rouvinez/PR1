package pr1_15;

import pr1_15.Animal.Sex;

public class Cat extends Mammal {
  public Cat(int age, int weight, Sex sex) {
    super(age, weight, sex, 4);
  }
}
 