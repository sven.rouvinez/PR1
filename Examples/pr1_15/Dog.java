package pr1_15;

public class Dog extends Mammal {
  // only dogs and human have a name !
  final String name;
  
  public Dog(String name, int age, int weight, Sex sex) {
    super(age, weight, sex, 4);    
    this.name = name;
  }
  
  String name() {
    return this.name;
  }
  
  @Override
  public String toString() {
    return super.toString() + ", name " + this.name;
  }
}
