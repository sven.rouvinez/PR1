package pr1_15;

public abstract class Mammal extends Animal {
  private final int nbrOfLegs;
  public Mammal(int age, int weight, Sex sex, int nbrOfLegs) {
    super(age, weight, sex);
    this.nbrOfLegs = nbrOfLegs;
  }
  
  public int nbrOfLegs() {
    return this.nbrOfLegs;
  }
}
