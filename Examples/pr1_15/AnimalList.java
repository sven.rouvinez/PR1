package pr1_15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Predicate;

// the limitation with this approach is that one would need to
// build a XXXList class for each type of objects that one want to handle in a list
// ultimately, the only option for storing a list of any object is to define
// an ObjectList class -> weakly typed
// the solution is to use generic classes (see the List class)

public class AnimalList {
  private Animal[] animalArray;
  private int size;
  
  public AnimalList(int minCapacity) {
    animalArray = new Animal[minCapacity];
  }
  
  public void add(Animal animal) {
    ensureCapacity(size + 1);
    animalArray[size++] = animal;
  }

  public Animal get(int index) {
    if (index >= size) {
      throw new IllegalArgumentException();
    }
    return animalArray[index];
  }
  
  public int size() {
    return size;
  }

  // for finding instances of sub-classes, one must use typecasting
  // this is a bad construction, since a super-class is not supposed to know about
  // its sub-classes
  public static AnimalList findDogs(AnimalList animalList) {
    AnimalList foundAnimalList = new AnimalList(animalList.size);
    for (int i = 0; i < animalList.size(); i++) {
      Animal a = animalList.get(i);
      if (a instanceof Dog) {
        foundAnimalList.add(a);
      }
    }
    return foundAnimalList;
  }
  
  // The limitation with this approach is that a method needs to be 
  // created for any single type of search that we want to perform
  // for instance on age range, on weight, etc..
  public static AnimalList findAnimalsOlderThan(AnimalList animalList, int age) {
    AnimalList foundAnimalList = new AnimalList(animalList.size);
    for (int i = 0; i < animalList.size(); i++) {
      Animal a = animalList.get(i);
      if (a.age() >= age) {
        foundAnimalList.add(a);
      }
    }
    return foundAnimalList;
  }
  
  // A first improved solution to this problem is to let the caller specify the 
  // search criteria. The search criteria is specified through a local class
  // implementing a specific interface.
  // This solution is better but one still have additional code: a new interface
  // and a local class for each search you plan to perform in your application.
  // Because Checkanimal1 implements an interface, one can use an anonymous 
  // class instead of a local class and bypass the need to declare a new
  // named class for each search.
  @FunctionalInterface
  interface CheckAnimal {
    boolean test(Animal a);
  }
    
  // declare one inner static class for each specific search 
  public static class CheckMaleBetween2And5 implements CheckAnimal {
    public boolean test(Animal a) {
      return a.gender() == Animal.Sex.MALE && a.age() >= 2 && a.age() <= 5;
    }
  }
  public static class CheckFemale implements CheckAnimal {
    public boolean test(Animal a) {
      return a.gender() == Animal.Sex.FEMALE;
    }
  }
  
  public static AnimalList findAnimals(AnimalList animalList, CheckAnimal check) {
    AnimalList foundAnimalList = new AnimalList(animalList.size);
    for (int i = 0; i < animalList.size(); i++) {
      Animal a = animalList.get(i);
      if (check.test(a)) {
        foundAnimalList.add(a);
      }
    }
    return foundAnimalList;
  }
      
  // So the next solution is to the solution using anonymous classes.
  // In this case, only the interface is defined and the class instance is 
  // created anonymously at the time the printanimals method is called.
  // This approach reduces the amount of code required because you don't 
  // have to create a new class for each search that you want to perform.
  // However, the syntax of anonymous classes is bulky considering that 
  // the Checkanimal interface contains only one method. 
  // In this case, you can use a lambda expression instead of an anonymous class.
  // And one can use the standard functional interface Predicate<T>
  // as shown below.
  public static AnimalList findAnimalsWithPredicate(AnimalList animalList, Predicate<Animal> check) {
    AnimalList foundAnimalList = new AnimalList(animalList.size);
    for (int i = 0; i < animalList.size(); i++) {
      Animal a = animalList.get(i);
      if (check.test(a)) {
        foundAnimalList.add(a);
      }
    }
    return foundAnimalList;
  }
  
  // And we can proceed one step further by 
  
  // private methods
  private void ensureCapacity(int minCapacity) {
    int oldCapacity = animalArray.length;
    if (minCapacity > oldCapacity) {
      Animal[] oldArray = animalArray;
      int newCapacity = (oldCapacity * 3)/2 + 1;
      if (newCapacity < minCapacity) {
        newCapacity = minCapacity;
      }
      animalArray = Arrays.copyOf(animalArray, newCapacity);
    }
  }
}
