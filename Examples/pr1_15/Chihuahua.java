package pr1_15;

import pr1_15.Animal.Sex;

public class Chihuahua extends Dog {
  public Chihuahua(String name, int age, int weight, Sex sex) {
    super(name, age, weight, sex);
  }
}
