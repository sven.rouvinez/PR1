package pr1_15;

import pr1_15.Animal.Sex;

public class Human extends Mammal {
  // only dogs and human have a name !
  final String name;
  
  public Human(String name, int age, int weight, Sex sex) {
    super(age, weight, sex, 2);
    this.name = name;
  }
  
  String name() {
    return this.name;
  }
  
  @Override
  public String toString() {
    return super.toString() + ", name " + this.name;
  }
}
