package pr1_15;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Predicate;

// Define a generic List class. T specified the type of the objects stored in the list.
// Genericity adds stability to your code by making more of the bugs detectable
// at compile time. The use of generics allows:
//   - stronger type checks at compile time
//   - elimination of casts
//   - programming of generic algorithms made possible

// Note that in the implementation below, we assume that the T objects are 
// immutable objects and that we may store only references to these object
// and return references to an object in the array.

public class List<T> {
  // we store our array of T objects as an array of Object
  private Object[] oArray;
  private int size;
    
  public List(int minCapacity) {
    // allocate a new object array
    oArray = new Object[minCapacity];
  }
  
  public void add(T item) {
    ensureCapacity(size + 1);
    oArray[size++] = item;
  }

  @SuppressWarnings("unchecked")
  public T get(int index) {
    if (index >= size) {
      throw new IllegalArgumentException();
    }
    return (T) oArray[index];
  }
    
  public int size() {
    return size;
  }

  // private methods
  private void ensureCapacity(int minCapacity) {
    int oldCapacity = oArray.length;
    if (minCapacity > oldCapacity) {
      Object[] oldArray = oArray;
      int newCapacity = (oldCapacity * 3)/2 + 1;
      if (newCapacity < minCapacity) {
        newCapacity = minCapacity;
      }
      oArray = Arrays.copyOf(oArray, newCapacity);
    }
  }

}
