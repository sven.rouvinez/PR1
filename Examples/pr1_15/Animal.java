package pr1_15;

public abstract class Animal {
  private final int age;
  private final int weight;
  private final Sex sex;
  
  public enum Sex { MALE, FEMALE };
  
  public Animal(int age, int weight, Sex sex) {
    this.age = age;
    this.weight = weight;
    this.sex = sex;
  }
  
  public Animal(Animal p) {
    this(p.age, p.weight, p.sex);
  }
  
  public int age() {
    return age;
  }
  
  public int weight() {
    return weight;
  }
  
  public Sex gender() {
    return sex;
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName() + " age: " + age + " with weight: " + weight + " and sex: " + sex;
  }
}
