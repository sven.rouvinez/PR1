package pr1_04;

public class Recursion {

  public static void main(String[] args) {
    final int n = 5;
    final int fib = fibonacci(n);
    System.out.println("F(" + n + ") = " + fib); 

    String word = "let";
    printAnagrams("", word);
  }
  
  // RECURSIVE FUNCTION
  
  // FIBONACCI
  // F(0) = 0
  // F(1) = 1
  // n > 1, F(n) = F(n-1) + F(n-2)
  private static int fibonacci(int n) {
    if (n <= 1) {
      return n;
    } 
    else {
      return fibonacci(n - 1) + fibonacci(n - 2);
    }
  }
  
  // ANAGRAM
  private static void printAnagrams(String prefix, String word) {
    if (word.length() <= 1) {
      System.out.println(prefix + word);
    } 
    else {
      for (int i = 0; i < word.length(); i++) {
        String cur = word.substring(i, i + 1);
        String before = word.substring(0, i); // letters before cur
        String after = word.substring(i + 1); // letters after cur
        printAnagrams(prefix + cur, before + after);
      }
    }
  }
}