package pr1_04;

public class Method {

  public static void main(String[] args) {
    // create two local variables
    int argForCall1 = 0;
    int argForCall2 = 1;
    
    // call the private method callByValue() 
    // (method defined for two integer arguments) 
    // with the effective arguments argForCall1 and argForCall2
    callByValue(argForCall1, argForCall2);
    
    // create two local String variables
    String str1 = new String("Original string1");
    String str2 = new String("Original string2");

    // call the private method callByValue() 
    // (method defined for two String arguments) 
    // with the effective arguments argForCall1 and argForCall2
    // remember that Strings are immutable in Java, meaning that
    // when you modify the string a new String object is built
    // so the strings modified in callByValueForStringType() are not visible here
    callByValue(str1, str2);
    
    // call the private method callByValue()
    // (method defined for two integer array arguments)
    // you can modify the object referenced by obj1 or obj2 but modifying
    // the reference itselt won't affect obj1 and obj2 here
    int[] intArray1 = new int[]{0, 0, 0};
    int[] intArray2 = new int[]{0, 0, 0};
    callByValue(intArray1, intArray2);    
  }
  
  // example showing what argument by value means for primitive types
  private static void callByValue(int arg1, int arg2)
  {
    // the variables used by the caller as effective arguments are not visible here
    
    // at this stage the arg1 and arg2 arguments may be considered as local variables
    // so one can modify their value
    arg1 = 2;
    arg2 = 3;
    
    // here we leave the scope for the arg1 and arg2 local variables
    // so their modified value will not be visible by the caller
  }
  
  // example showing what argument by value means for string types
  // method overloaded for two string parameters
  private static void callByValue(String arg1, String arg2)
  {
    // at this stage arg1 and arg2 store a copy of the reference to the strings
    
    // String objects are immutable
    // by modifying them, you create a new string and you store the new reference in arg1 and arg2
    arg1 = "Modified string1";
    arg2 = "Modified string2";
    
    // the modified strings will be dereferenced here and not visible from the caller
  }
  
  // example showing what argument by value means for object types
  // method overloaded for two int[] arguments
  private static void callByValue(int[] arg1, int[] arg2)
  {
    // here arg1 and arg2 are local variables that store the reference to the array
    
    // so you can access an array element and modify it
    arg1[0] = 1;
    arg2[0] = 2;
    
    // the array elements have been modified and the caller will see the changes
  }
  

  // remark 1: one cannot declare two methods with the same signature 
  //           even if they have a different return type
  private static int calculateAnswer(int value) {
    return 0;
  }
    
  // so the declaration below would produce a compilation error
  // public double calculateAnswer(int value) {
  //   return 0.0;
  // }
}
