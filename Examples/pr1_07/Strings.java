package pr1_07;

public class Strings {

  public static void main(String[] args) {
    
    // STRING CREATION
    
    // String is a reference type
    
    // A string object can be created using new or simply with a "" literal
    // in both cases, a string object is created with the corresponding value
    String str1 = "This is a string";
    String str2 = new String("This is another string");

    // The string object has many constructors (13 !), so there are many ways
    // of creating a string object. Some examples
    // creating an empty string is possible
    String str3 = new String();
    
    // creation from an array of characters
    char[] charArray = { 'a', 'n', 'o', 't', 'h', 'e', 'r', ' ', 'w', 'a', 'y' };
    String str4 = new String(charArray);    

    // creating from another string
    // in this case, a new object is created with the same value 
    // as the other string object
    String str5 = new String(str4);
        
    // A String object is immutable, meaning that it cannot be modified after
    // its creation
    // A new object can be assigned to a string variable, meaning that the 
    // old object is released and that the variable stores the reference to
    // the new object
    // (observe the object id changing in the debugger)
    str1 = "This is a modified string";
    
    // ACCESSOR METHODS
    
    // methods for accessing string properties like length, characters, etc.
    System.out.println("length of string \"" + str1 + "\" is " + str1.length());
    
    // storing all characters of a string in an array of characters
    char[] charArrayFromString = new char[str1.length()];
    for (int i = 0; i < str1.length(); i++) {
      charArrayFromString[i] = str1.charAt(i);
    }
    // or in a single line of code
    str1.getChars(0 /* srcBegin */, str1.length() /* srcEnd */, 
                  charArrayFromString /* dst */, 0 /* dstBegin */);
    
    // CONCATENATING STRINGS
    
    // strings can be concatenated with the "+" operator or with the concat() method
    // note that since strings are immutable, concatenating strings implies
    // the creation of new objects and is an expensive operation
    str3 = str1 + str2;
    str3 = str1.concat(str2);
    
    // one can use string literals while concatenating
    str3 = str1 + " in between " + str2;
    // or any other object that can be converted to a string
    // for any object that is not a String, its toString() method is called to
    // convert it to a String (this implies autoboxing for primitive types)
    // in this case the primitive type is first converted to its wrapper type
    // and then the toString() method of the wrapper object is called
    int i = 123;
    str3 = "int " + i + " is inserted before " + str2;
    System.out.println(str3);
    
    // FORMATTING STRINGS
    // similar to format in printf() syntax in c-like languages
    // this is done using the format() method
    // the format() method is a static method that returns a new formatted
    // string object
    float f = 12.3f;
    // String str6 = "The value of i is " + i + " and the value of f is " + f;
    String str6 = String.format("The value of i is %d 0x%x and the value of f is %f", i, i, f);
    System.out.println(str6);
        
    // CONVERSIONS
    // a string may be converted to a number (useful when the value is entered
    // by the user)
    // the conversion is usually accomplished using the wrapper classes
    String userInput = "123.1";
    float fUser = Float.valueOf(userInput).floatValue();
    
    // what happens if the user input is not a number
    // in this case, a NumberFormatException is thrown and this exception
    // should be handled by your program (a user may enter an invalid number...)
    // (note that NumberFormatException is a RuntimeException so it 
    //  may not be handled as above)
    userInput = "1r4.2";
    try {
      fUser = Float.valueOf(userInput).floatValue();
    }
    catch (NumberFormatException e) {
      System.out.println("A invalid number has been entered: " + e);
    }
    
    // a number may also be converted to a string
    
    // through concatenation (note that concatenation is required)
    str1 = "" + i;
    // or through valueOf() method
    str1 = String.valueOf(i);
    // or through wrapper's static toString() method
    str1 = Integer.toString(i);
    
    // COMPARISON METHODS
    
    // equals() returns true when the string in argument represents the same 
    // sequence of characters as this object
    str1 = "This is the same string";
    str2 = str1;
    str3 = new String("This is the same string");
    System.out.println("Result of == is " + (str1 == str2) + " " + (str1 == str3) + " " + (str2 == str3));
    System.out.println("Result of equals() is " + str1.equals(str2) + " " + str1.equals(str3) + " " + str2.equals(str3));
        
    // compareTo() compares the string in argument lexicographically with this object
    str1 = "ABCD";
    str2 = "abcd";
    str3 = "ABCDE";
    System.out.println("Result of compareTo() is " + str1.compareTo(str2) + " " + str1.compareTo(str3) + " " + str2.compareTo(str3));
    System.out.println("Result of compareToIgnoreCase() is " + str1.compareToIgnoreCase(str2) + " " + str1.compareToIgnoreCase(str3) + " " + str2.compareToIgnoreCase(str3));    
    
    // startsWith(), endsWith() check whether a string starts/ends with a specified substring
    System.out.println("Result of startsWith(\"AB\") is " + str1.startsWith("AB"));
    System.out.println("Result of endsWith(\"CD\") is " + str1.endsWith("CD"));
    
    // note that some methods have a ...IgnoreCase() version that executes the
    // operation ignoring differences in case
        
    // HELPER METHODS
    
    // indexOf() finds the occurrence of a character in a string
    double d = 123.45;
    str1 = Double.toString(d);
    
    int dot = str1.indexOf('.');
    System.out.println(dot + " digits " + "before decimal point.");
    // note the use of parentheses for printing the value of an expression
    System.out.println( (str1.length() - dot - 1) + " digits after decimal point.");
    
    // substring() returns a new string that is a substring of a given string
    str1 = "this is a full string";        
    String subString = str1.substring(0 /* beginIndex */, 4 /* endIndex */);
    System.out.println("substring is " + subString);
    
    // split allows to split a string using a regular expression
    // the result is an array of strings
    str1 = "this_is-a_string-to_be-split";
    String[] stringArray = str1.split("[_-]");
    System.out.println("Number of strings in array is " + stringArray.length);
    
    // example for dealing with file paths
    String fullPath = "/home/user/directory/file.ext";
    // find the extension
    String ext = fullPath.substring(fullPath.lastIndexOf('.') + 1);
    // find the filename
    String filename = fullPath.substring(fullPath.lastIndexOf('/'), fullPath.lastIndexOf('.'));
    // find the path
    String path = fullPath.substring(0, fullPath.lastIndexOf('/'));
    
    System.out.println("full path is " + fullPath);
    System.out.println("full path is " + path + filename + "." + ext);
    
    // StringBuilder/StringBuffer CLASSES
    // StringBuilder and StringBuffer are like String objects, except that they
    // can be modified after they are created
    // These objects are treated like variable-length arrays that contain a
    // sequence of characters. At any time, the length and the content of the
    // sequence of characters cab be changed through method invocations
    
    // StringBuilder/StringBuffer classes should be used when performance is an issue
    
    // A StringBuilder object has an internal capacity, which is the length of
    // array of characters that is allocated. Thus, the capacity is always 
    // greater or equal to the length of the contained string. The capacity will
    // be automatically increased as necessary to accommodate the string length.
    
    // creates an empty StringBuilder object with a default capacity of 16
    StringBuilder stringBuilder1 = new StringBuilder();
    System.out.println("StringBuilder: length is " + stringBuilder1.length() + " with capacity of " + stringBuilder1.capacity());
    
    // creates an empty StringBuilder object with a given capacity
    StringBuilder stringBuilder2 = new StringBuilder(32);
    System.out.println("StringBuilder: length is " + stringBuilder2.length() + " with capacity of " + stringBuilder2.capacity());
    
    // creates a StringBuilder object from a String
    StringBuilder stringBuilder3 = new StringBuilder(str1);
    System.out.println("StringBuilder: length is " + stringBuilder3.length() + " with capacity of " + stringBuilder3.capacity());
    
    // possible operations on StringBuilder objects are append(), delete()
    // insert(), replace(), setCharAt(), reverse() and reverse (not exhaustive)
    
    // small palindrome example
    String palindrome = "Anna saw I was Anna";
       
    StringBuilder sb = new StringBuilder(palindrome);
    sb.reverse();  // reverse it      
    System.out.println(sb);
    System.out.println(sb.toString().equalsIgnoreCase(palindrome));
    
    // conversion from StringBuilder to String
    String fromStringBuilder = sb.toString();
    
    // conversion from String to StringBuilder
    // construction (not really a conversion)
    StringBuilder fromString = new StringBuilder(str1);
    
    // or alternatively (not really a conversion)
    fromString.insert(0, str1);
    fromString.setLength(str1.length());
        
    // StringBuffer is the same as StringBuilder, but synchronized (multi-thread safe)
  }

}
