package s19;

public class PlayWithSeries {

  public static void main(String[] args) {
    // create a SeriesB1 instance
    Series sb1 = new SeriesB1();
    // evaluate the result from 0 to 20
    System.out.println("sb1 (from 0 to 20) is " + sb1.evaluateBetween(0, 20));
    
    // create a SeriesB2 instance
    Series sb2 = new SeriesB2();
    // evaluate the result from 0 to 20
    System.out.println("sb2 (from 0 to 20) is " + sb2.evaluateBetween(0, 20));
    
    // solution with anonymous classes
    Series sb1a = new Series() {
      @Override
      public double function(int i) {
        return i / Math.pow(2.0, i);
      } 
    };
    System.out.println("sb1a (from 0 to 20) is " + sb1a.evaluateBetween(0, 20));
    Series sb2a = new Series() {
      @Override
      public double function(int i) {
        return (i * i) / Math.pow(2.0, i);
      } 
    };
    System.out.println("sb2a (from 0 to 20) is " + sb2a.evaluateBetween(0, 20));
  }

  private static class SeriesB1 extends Series {
    @Override
    public double function(int i) {
      return i / Math.pow(2.0, i);
    }
  }
  
  private static class SeriesB2 extends Series {
    @Override
    public double function(int i) {
      return (i * i) / Math.pow(2.0, i);
    }
  }
}
