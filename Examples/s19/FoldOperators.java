package s19;

public class FoldOperators {
  // declare static operators through the IFoldableOperation
  // operators are defined as private static classes below
  public static final IFoldableOperation SUM = new SumOperation();
  public static final IFoldableOperation PROD = new ProdOperation();
  public static final IFoldableOperation MAX = new MaxOperation();

  // implementation using static classes
  private static class SumOperation implements IFoldableOperation {
    public double initialValue() {
      return 0.0;
    }
    public double combine(double accumulated, double newValue) {
      return accumulated + newValue;
    }
  }
 
  private static class ProdOperation implements IFoldableOperation {
    public double initialValue() {
      return 1.0;
    }
    public double combine(double accumulated, double newValue) {
      return accumulated * newValue;
    }
  }
 
  private static class MaxOperation implements IFoldableOperation {
    public double initialValue() {
      return Double.NEGATIVE_INFINITY;
    }
    public double combine(double accumulated, double newValue) {
      return Math.max(accumulated, newValue);
    }
  }

  // declare static operators through the IFoldableOperation
  // operators use anonymous classes
  public static final IFoldableOperation SUMA = new IFoldableOperation() {
    public double initialValue() {
      return 0.0;
    }
    public double combine(double accumulated, double newValue) {
      return accumulated + newValue;
    }
  };
  public static final IFoldableOperation PRODA = new IFoldableOperation() {
    public double initialValue() {
      return 1.0;
    }
    public double combine(double accumulated, double newValue) {
      return accumulated * newValue;
    }
  };
  public static final IFoldableOperation MAXA = new IFoldableOperation() {
    public double initialValue() {
      return Double.NEGATIVE_INFINITY;
    }
    public double combine(double accumulated, double newValue) {
      return Math.max(accumulated, newValue);
    }
  };
  
}
