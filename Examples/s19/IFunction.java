package s19;

public interface IFunction {
  double valueAt(double x);
}
