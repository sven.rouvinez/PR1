package s19;

public abstract class Series {
  public abstract double function(int i);
  public double evaluateBetween(int from, int to) {
    double sum = 0.0;
    for (int i = from; i < to; i++) {
      sum += function(i);
    }
    return sum;
  }

}
