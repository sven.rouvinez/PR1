package s19;

public class PlayWithFunction {

  public static void main(String[] args) {
    // test fromArray sequence
    double[] doubleArray = new double[] { 1.0, 1.5, 2.0, 2.5, 3.0 };
    double sumArray = foldLeft(CommonSequences.fromArray(doubleArray), FoldOperators.SUM);
    System.out.println("Sum of elements in array is " + sumArray + (" vs expected 10.0"));
    double prodArray = foldLeft(CommonSequences.fromArray(doubleArray), FoldOperators.PROD);
    System.out.println("Product of elements in array is " + prodArray + (" vs expected 22.5"));
    double maxArray = foldLeft(CommonSequences.fromArray(doubleArray), FoldOperators.MAX);
    System.out.println("Max of elements in array is " + maxArray + (" vs expected 3.0"));
    
    // test fromSeries sequence
    IFunction identityFunction = new IFunction() {
      public double valueAt(double x) { 
        return x; 
      } 
    };
    double sumSeries = foldLeft(CommonSequences.fromSeries(identityFunction, 1, 5), FoldOperators.SUMA);
    System.out.println("Sum of elements in series is " + sumSeries + (" vs expected 15.0"));
    double prodSeries = foldLeft(CommonSequences.fromSeries(identityFunction, 1, 5), FoldOperators.PRODA);
    System.out.println("Product of elements in series is " + prodSeries + (" vs expected 120.0"));
    double maxSeries = foldLeft(CommonSequences.fromSeries(identityFunction, 1, 5), FoldOperators.MAXA);
    System.out.println("Max of elements in series is " + maxSeries + (" vs expected 5.0"));
    
    IFunction fa = new IFunction() {
      public double valueAt(double x) { 
        return x / Math.pow(2.0, x); 
      } 
    };
    double sumSeries1 = foldLeft(CommonSequences.fromSeries(fa, 0, 20), FoldOperators.SUMA);
    System.out.println("Sum of elements in series is " + sumSeries1);
    
    // test fromSampledFunction
    IFunction fb = new IFunction() {
      public double valueAt(double x) {
        return Math.sin(x) * Math.sin(x) * Math.cos(x);
      }
    };
    double sumSampled = foldLeft(CommonSequences.fromSampledFunctionA(fb, 0, Math.PI, 1002), FoldOperators.SUM);
    System.out.println("Sum of elements in sampled function is " + sumSampled);
    double prodSampled = foldLeft(CommonSequences.fromSampledFunctionA(fb, 0, Math.PI, 1002), FoldOperators.PROD);
    System.out.println("Product of elements in sampled function is " + prodSampled);
    double maxSampled = foldLeft(CommonSequences.fromSampledFunctionA(fb, 0, Math.PI, 1002), FoldOperators.MAX);
    System.out.println("Max of elements in sampled function is " + maxSampled);
  }

  public static double foldLeft(INumberSequence ns, IFoldableOperation op) {
    double res = op.initialValue();
    while (ns.hasMoreNumbers()) {
      res = op.combine(res, ns.nextNumber());
    }
    return res;
  }
}
