package s19;

import java.util.Arrays;

public class CommonSequences {
  // declare static sequences through the IFunction interface
  // sequences are defined as private static classes below
  public static INumberSequence fromArray(double[] t) {
    return new ArrayNumberSequence(t);
  }
  public static INumberSequence fromSeries(IFunction f, int from, int to) {
    return new SeriesNumberSequence(f, from, to);
  }
  public static INumberSequence fromSampledFunction(IFunction f, double from, double to, int nSamples) {
    return new SampledFunctionNumberSequence(f, from, to, nSamples);
  }
  
  // static class definitions
  private static class ArrayNumberSequence implements INumberSequence {
    private int readIndex = 0;
    private final double[] doubleArray;
    public ArrayNumberSequence(double[] t) {
      doubleArray = t;
    }    
    public boolean hasMoreNumbers() {
      return readIndex < doubleArray.length;
    }
    public double nextNumber() {
      return doubleArray[readIndex++];
    }
  }
  
  private static class SeriesNumberSequence implements INumberSequence {
    private final IFunction function;
    private final int from;
    private final int to;
    private int seriesIndex = 0;
    public SeriesNumberSequence(IFunction f, int from, int to) {
      this.function = f;
      this.from = from;
      this.to = to;
      this.seriesIndex = from;      
    }
    
    public boolean hasMoreNumbers() {
      return seriesIndex <= to;
    }
    
    public double nextNumber() {
      return this.function.valueAt(seriesIndex++);
    }
  }
  
  private static class SampledFunctionNumberSequence implements INumberSequence {
    private final IFunction function;
    private final double from;
    private final double to;
    private final int nSamples;
    private int sampleIndex = 0;
    private double step;
    public SampledFunctionNumberSequence(IFunction f, double from, double to, int nSamples) {
      // nSamples is the number of samples between from and to, from and to are also sampled
      this.function = f;
      this.from = from;
      this.to = to;
      this.nSamples = nSamples + 2;
      this.step = (to - from) / (nSamples + 1);
    }
    
    public boolean hasMoreNumbers() {
      return sampleIndex <= nSamples;
    }
    
    public double nextNumber() {
      double x = from + step * sampleIndex;
      sampleIndex++;
      return this.function.valueAt(x);
    }
  }
  
  // declare static sequences through the IFunction interface
  // sequences are defined using anonymous classes
  public static INumberSequence fromArrayA(double[] t) {
    return new INumberSequence() {
      private int readIndex = 0;
      public boolean hasMoreNumbers() {
        return readIndex < t.length;
      }
      public double nextNumber() {
        return t[readIndex++];
      }
    };
  }
  public static INumberSequence fromSeriesA(IFunction f, int from, int to) {
    return new INumberSequence() {
      private int seriesIndex = from;
      
      public boolean hasMoreNumbers() {
        return seriesIndex <= to;
      }
      
      public double nextNumber() {
        return f.valueAt(seriesIndex++);
      }
    };    
  }
  public static INumberSequence fromSampledFunctionA(IFunction f, double from, double to, int nSamples) {
    return new INumberSequence() {
      private final int nTotalSamples = nSamples + 2;
      private int sampleIndex = 0;
      private double step = (to - from) / (nSamples + 1);
      
      public boolean hasMoreNumbers() {
        return sampleIndex <= nTotalSamples;
      }
      
      public double nextNumber() {
        double x = from + step * sampleIndex;
        sampleIndex++;
        return f.valueAt(x);
      }
    };
  }

}
