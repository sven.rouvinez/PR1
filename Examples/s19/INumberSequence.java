package s19;

public interface INumberSequence {
  boolean hasMoreNumbers();
  double nextNumber();
}
