package pr1_01;

public class StringType {

  public static void main(String[] args) {
    // initialization of strings with literals
    int number = 1;
    String string1 = "Value of number is " + number + ";";
    System.out.println(string1);

    // initialization with a char literal is not possible
    // string1 = 'a';

    // initialization with a int literal is not possible
    // string1 = 4;

    // conversion using wrappers
    string1 = "342";
    number = Integer.parseInt(string1);
    System.out.println("Value of number is " + number);
    
    string1 = Integer.toString(number);
    System.out.println("String is " + string1);
  }

}
