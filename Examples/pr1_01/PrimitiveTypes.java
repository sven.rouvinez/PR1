package pr1_01;

public class PrimitiveTypes {
  public static void main(String[] args) {
    // BOOLEAN type
    // cannot convert to any type
    /* boolean boolValue = (boolean) 1; */ // not possible
    /* int boolAsInt = (int) boolValue; */ // not possible
    // int intValue = 1;
    // boolean boolValue = intValue == 1 ? true : false; 
    
    // CHAR type
    // initialization with literal
    char char1 = 'A';
    char char2 = '\t';
    System.out.println(char1 + " followed by tab" + char2 + " followed by " + char1);
    
    // do not mix char and String literals
    /* char char3 = "A"; */ // not possible
    
    // conversions to int
    int charAsInt = char1;
    System.out.println("char1 is " + char1 + " while charAsInt is " + charAsInt);
    // conversions from int
    char1 = (char) charAsInt; // not possible without type casting
    
    // INTEGER types
    
    // conversion from a smaller to a larger type
    byte intAsByte = 8;
    int intAsInt = intAsByte;
    
    // conversion from a larger type to a smaller type
    intAsByte = (byte) intAsInt; // not possible without type casting - range is different
    System.out.println("Value of intAsByte is " + intAsByte);
        
    // example where value is too large
    intAsInt = 512;
    intAsByte = (byte) intAsInt;
    System.out.println("Value of intAsByte is " + intAsByte);
    
    // operations with multiple types
    intAsByte = 10;
    short intAsShort = 15;
    intAsInt = 20;
    int sumAsInt = intAsByte + intAsShort + intAsInt; // sum is converted to int
    short sumAsShort = (short) (intAsByte + intAsShort + intAsInt); // not possible without type casting - range is different
    System.out.println("Value of sumAsInt is " + sumAsInt);
    System.out.println("Value of sumAsShort is " + sumAsShort);
    
    // FLOATING POINT types
    
    // conversion from a smaller to a larger type
    float floatAsFloat = 10f;
    double floatAsDouble = floatAsFloat;
    
    // conversion from a larger type to a smaller type
    floatAsFloat = (float) floatAsDouble; // not possible without a cast - range is different
    
    // conversion from an integer type to floating point types
    long longValue = 0L;
    floatAsFloat = longValue;
    floatAsDouble = intAsInt;
    System.out.println("Value of floatAsDouble is " + floatAsDouble);
    
    // conversion from a floating type to an integer
    // the decimal part is truncated
    floatAsDouble = 10.6;
    intAsInt = (int) floatAsDouble; // not possible without a cast - truncation happens
    System.out.println("Value of intAsInt is " + intAsInt);
    
    // float to integer conversion
    // example where conversion creates a loss because the range is different
    // maxInt is 2^31 - 1 = 2'147'483'647 (so around 2.147e9f)
    // let's store something like 2.2e9 as a float and try to convert it to an int
    // the difference between 2.2e9 and the maximal integer value is 52'516'352 = 5.25e7 which is lost 
    floatAsFloat = 2.2e9f;
    intAsInt = (int) floatAsFloat;
    System.out.println("Value of floatAsFloat is " + floatAsFloat + " (as compared to maxInt = " + Integer.MAX_VALUE + ")" );
    System.out.println("Value of floatAsFloat is " + floatAsFloat + " (as compared to intAsInt = " + intAsInt + ")" );
    floatAsFloat = floatAsFloat - intAsInt;
    System.out.println(" we lost " + floatAsFloat);
    
    // example where conversion creates a loss of precision due to number of bicimals
    // converting an int to a float back to an int
    // for representing (2^y - 1), y > 0, you need y bits (all 1's)
    // so (2^y - 1) in normalized floating point is 1.111... 2^(y-1) with (y-1) 1's in the mantissa
    // in IEEE 754-1985, the mantissa contains 23 digits - for a float
    // it means we can represent (2^y - 1) without loss for y <= 24
    // this is true for any integer number <= 2^24 - 1 = 16'777'215 
    // beyond 2^24 - 1 = 16'777'215, some integers cannot be represented without loss of precision as floating point
    // illustration for y = 25, the floating point representation will be 2^25 = 33'554'432 and not 2^25 - 1 = 33'554'431
    int originalInt = (1 << 25) - 1; 
    floatAsFloat = (float) originalInt;
    intAsInt = (int) floatAsFloat;
    int diffAsInt = intAsInt - originalInt;
    System.out.println(" we lost " + diffAsInt);
  }
}
