package pr1_12;

import pr1_09.Point2D;

public class Square extends Rectangle {  
  // constructor
  public Square(Point2D pt1, double width) {
    // call the super class constructor (as first statement)
    // as the super default constructor does not exist then another constructor
    // must be invoked
    super(pt1, width, width);
    
    // initialize the vertices
    // missing here  
  }  
 
  // the square sides are congruent
  @Override
  public boolean areSidesCongruent() {
    return true;
  }
}
