package pr1_12;

import pr1_09.Point2D;

// a triangle is a polygon
public class Triangle extends Polygon {
  // constructor
  public Triangle(Point2D pt1, Point2D pt2, Point2D pt3) {
    // call the super class constructor (as first statement)
    // if this statement is omitted, it will be added by default
    super();
    
    // initialize the vertices
    vertices = new Point2D[]{ pt1, pt2, pt3 };
    
    // here we should compute the area of the triangle given its vertices    
    
  }
  
  // define abstract methods
  @Override
  public boolean isConvex() {
    return true;
  }
  
  
}
