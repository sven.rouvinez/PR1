package pr1_12;

import pr1_09.Point2D;

// a rectangle is a polygon
public class Rectangle extends Polygon {
  // protected data members
  protected double width = 0.0;
  protected double height = 0.0;
  
  // constructor
  public Rectangle(Point2D pt1, double width, double height) {
    // call the super class constructor (as first statement)
    // if this statement is omitted, it will be added by default
    // super();
    
    // store width and height
    this.width = width;
    this.height = height;
   
    // initialize the vertices
    // missing here
   
    // compute the area
    area = this.width * this.height;
  }  
  
  // redefine the isConvex() method and extend its visibility
  // rectangles are always convex
  @Override
  public boolean isConvex() {
    return true;
  }
  
  // methods for getting the rectangle dimensions
  public double getWidth() {
    return this.width;
  }
  
  public double getHeight() {
    return this.height;
  }  
}
