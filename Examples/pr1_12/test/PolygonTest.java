package pr1_12.test;

import pr1_12.Polygon;
import pr1_12.Triangle;
import pr1_12.Rectangle;
import pr1_12.Square;

import pr1_09.Point2D;

public class PolygonTest {

  public static void main(String[] args) {
    // creating a polygon which is not a triangle or rectangle is not possible
    // Polygon polygon = new Polygon();

    // create a triangle
    Triangle t1 = new Triangle(new Point2D(0.0, 0.0), new Point2D(0.0, 1.0), new Point2D(1.0, 0.0));
    
    // create a rectangle
    Rectangle r1 = new Rectangle(new Point2D(0.0, 0.0), 1.0, 2.0);
    
    // create a square
    Square s1 = new Square(new Point2D(0.0, 0.0), 1.0);
    
    // print the surface of the triangle, rectangle and square
    System.out.println("The area of the triangle is " + t1.area());
    System.out.println("The area of the rectangle is " + r1.area());
    System.out.println("The area of the square is " + s1.area());
    
    // use a generalized reference to t1
    Polygon p1_t1 = t1;
    
    // use a generalized reference to r1
    Polygon p2_r1 = r1;
    
    // use a generalized reference to s1
    Rectangle r2_s1 = s1;
    
    if (r1 instanceof Square) {
      Square s2_r1 = (Square) r1;
    }
    
    // check the nature of each object
    System.out.println("t1 refers to a Triangle: " + (t1 instanceof Triangle) + " and to a Polygon: " + (t1 instanceof Polygon));
    System.out.println("r1 refers to a Rectangle: " + (r1 instanceof Rectangle) + " and to a Polygon: " + (r1 instanceof Polygon));
    System.out.println("s1 refers to a Square: " + (s1 instanceof Square) + " and to a Rectangle: " + (s1 instanceof Rectangle));
    System.out.println("p1_t1 refers to a Triangle: " + (p1_t1 instanceof Triangle) + " and to a Polygon: " + (p1_t1 instanceof Polygon));
    System.out.println("p2_r1 refers to a Rectangle: " + (p2_r1 instanceof Rectangle) + " and to a Polygon: " + (p2_r1 instanceof Polygon));
    System.out.println("r2_s1 refers to a Square: " + (r2_s1 instanceof Square) + " and to a Rectangle: " + (r2_s1 instanceof Rectangle));
       
    // in Java, every object is an Object
    System.out.println("r2_s1 refers to an Object: " + (r2_s1 instanceof Object));
    
    // print the surface of both the triangle, rectangle and square 
    // through the generalized reference
    System.out.println("The area of the triangle is " + p1_t1.area());
    System.out.println("The area of the rectangle is " + p2_r1.area());
    System.out.println("The area of the square is " + r2_s1.area());

    // check whether the triangle and rectangle are convex
    // although the method is defined in the Polygon class, the method called
    // is the one of the specialized class
    System.out.println("The triangle is convex: " + p1_t1.isConvex());
    System.out.println("The rectangle is convex: " + p2_r1.isConvex());
    System.out.println("The square is convex: " + r2_s1.isConvex());
    
    // the use of the generalized interface does not allow access to methods
    // defined only in subclasses such as getWidth()/getHeight()
    // does not compile
    if (p2_r1 instanceof Rectangle) {
      double width = ((Rectangle) p2_r1).getWidth();
    }
    // in this case we need to use the reference to a type defining the method
    System.out.println("The rectangle width and height are: " + r1.getWidth() + " " + r1.getHeight());
    System.out.println("The square width is: " + r2_s1.getWidth());    
  }
}
  