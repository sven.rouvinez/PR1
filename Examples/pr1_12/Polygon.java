package pr1_12;

import pr1_09.Point2D;

// the Polygon class should be an abstract class
// (this will be demonstrating when reviewing chapter 13)
public class Polygon {
  // protected data members
  // the vertices of the polygon
  protected Point2D[] vertices;
  
  // the polygon area
  protected double area;
  
  // constructor (do not allow construction outside of specialized classes)
  protected Polygon() {
    vertices = null;
    area = 0;
  }
  
  // define the method area that returns the area value
  // note that the area is computed in the constructor of each subclass
  public double area() {
    return area;
  }
  
  // return the number of vertices
  public int nbrOfVertices() {
    if (vertices == null) {
      return 0;
    }
    return vertices.length;
  }
  
  // returns true if the polygon is convex
  // this method could be defined as abstract
  public boolean isConvex() {
    return false;
  }
  
  // return true if the polygon sides are congruent
  // this method could be defined as abstract
  public boolean areSidesCongruent() {
    return false;
  }
}
