package pr1_02;

public class Precedence {

  public static void main(String[] args) {
    // first example
    System.out.println(1 + 3 * 4 - 2 - 5);
    System.out.println(((1 + (3 * 4)) - 2) - 5);
    
    // easy examples that every programmer should be able to understand
    // * or / are stronger than + or -
    // operators with the same precedence are evaluated based on their left or right associativity
    System.out.println( 3 + 3 * 2 );
    System.out.println( 1 * 1 + 1 * 1);
    System.out.println( 3 * 3 - 2 );
    System.out.println( 1 + 1 / 1 - 1);
    // same precedence, from left to right
    System.out.println( 3 * 3 / 2 );
    // mix between precedence and associativity (left to right)
    System.out.println( 3 * 3 / 2 + 2);
    
    // second example
    {
      int i = 5;
      int a = 6;
      // int v = a - i++;
      // rewritten with parentheses
      int v = (a - (i++));
      System.out.println(i);
      System.out.println(v);
    }
    
    // 
    // examples that should NOT be written as they are because
    // 1. they are very difficult to be interpreted by someone reading the expression
    // 2. they have many different side-effects which are difficult to understand
    // 
    {      
      int i = 10;
      // the + operation is executed first, from left to right
      // on the left of +, i is evaluated to 10
      // on the right of +, (i = 5) is evaluated to 5, with the side effect of assigning 5 to i
      // at this stage, i equals 5 and the two operands of the + operation are 10 and 5 which is evaluated to 15
      // at last, this value is assigned to i
      // i = i + (i=5);
      // rewritten with parentheses
      i = (i + (i=5));      
      System.out.println("Value of i is " + i);
      // for demonstrating the same operation with an assignment to another variable, replace 'i =' by 'j ='
      i = 10;
      int j = i + (i=5);
      System.out.println("Value of i is " + i);
      System.out.println("Value of j is " + j);
    }
    
    {
      int i=2;
      // i = ++i + ++i * ++i;
      // rewritten with parentheses 
      // the first operation to be executed is the left most ++i, that is evaluated to 3, 
      // with the side effect of storing 3 in i
      // 3 is thus the left operand of the + operation
      // the right operand of the + operation must now be evaluated
      //  -> first, the middle ++i is evaluated to 4, with the side effect of storing 4 in i
      //  -> second, the right most ++i is evaluated to 5, with the side effect of storing 5 in i
      //  -> the multiplication is evaluated to 20
      // 20 is the right operand of the + operation
      // the addition is evaluated to 23, which is stored in i
      i = ((++i) + ((++i) * (++i)));
      System.out.println("Value of i is " + i);
    }   
    
    {
      int x = 1;
      // similar example
      // System.out.println( x++ + x++ * --x );
      // rewritten with parentheses
      // WRITE your own comments      
      System.out.println((x++) + ((x++) * (--x)));
    }
    
    {
      int x = 1;
      //  x << 1 * 2 >> 1
      // rewritten with parentheses
      int y = ((x << (1 * 2)) >> 1);
      System.out.println(y);
    }
    
    {
      int i=1, j=2;
      // i = i+++j;
      // rewritten with parentheses
      i = ((i++) + j);
      // i++ is first evaluated to 1, with the side effect of storing 2 in i
      // the addition has operands 1 and 2, evaluated to 3
      // 3 is stoted in i
      // note that the post-increment operation on i is without useless
      System.out.println("Value of i is " + i);
      System.out.println("Value of j is " + j);
    }
    
    // exercice 02.3
    {
      int j = 7, k = 4;  boolean a = true, b = false;   // 1
      a = 7 >= 3 | (7 != 3);  // 2
      b = a != true;  // 3
      a ^= true;      // 4
      // REWRITE the expression with the correct parentheses and check the result
      a &= false | b | 7 == 3 + 4;    // 5 (*)
      b = k++ >= 3 || j-- == 4;       // 6
      // REWRITE the expression with the correct parentheses and check the result
      a = j != 6 | k++ == 6 && j-- >= 0;  // 7 (*)
    }
    
    // exercice 02.4
    {
      int a,c;          // (*) 1
      int b = 4;
      c = 8;        // (*) 2
      a = b++ + --c;
      if (a != b) {
        int d = 3;      // (*) 3
        a = 2*d;        // (*) 4
      }
      a += b;           // (*) 5
    }
  }

}
