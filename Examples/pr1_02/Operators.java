package pr1_02;

public class Operators {

  public static void main(String[] args) {
    // additive operators
    {
      System.out.println(125 + 463); // prints 588 as an integer value
      System.out.println(2.0 - 6.3); // prints -4.3 as a floating point (double) value
      int age = 65;
      System.out.println(age); // prints 65
      System.out.println(age--); // prints 65, age is now 64
      System.out.println(age++); // prints 64, age is now 65
      System.out.println(--age); // prints 64, age is now 64
      System.out.println(++age); // prints 65, age is now 65
      System.out.println("A" + "B"); // prints "AB"

      // one can also perform arithmetic operations on char values
      char ch1 = 'A';
      ch1++;
      System.out.println(ch1); // prints 'B' as 'A' + 1
    }
    
    // multiplicative operators
    {
      System.out.println(64.0 * 3.0);
      System.out.println(64 / 3);
      System.out.println(64 % 3);
      System.out.println(10.0 / 0.0);
      System.out.println(-10.0 / 0.0);
      System.out.println(0.0 / 0.0);
      // while division by 0 is accepted with floating point numbers
      // it is not for integer numbers
      // so the following operation throws an exception that must be caught      
      try
      {
        System.out.println(10 / 0);
      }
      catch (ArithmeticException e)
      {
        System.out.println("Exception caught: " + e.getMessage());
      }
    }
    
    // arithmetic mixing int and float
    {
      double i = 4/3 + 1.5; // 4/3 is evaluated as an expression with int values
      System.out.println("value of i is " + i);
      
      i = 1.5 + 4/3; // 4/3 is still evaluated as an expression with int values
      System.out.println("value of i is " + i);
      
      i = 1.5 + (double) (4/3); // 4/3 is evaluated as an expression with int values then converted to double
      System.out.println("value of i is " + i);
            
      i = 1.5 + (double) 4 / (double) 3; // 4/3 is evaluated as an expression with floating point values
      System.out.println("value of i is " + i);
      
      // or alternatively
      i = 1.5 + 4.0 / 3.0; // 4/3 is evaluated as an expression with floating point values
      System.out.println("value of i is " + i);      
    }
    
    // bitwise operators
    {
      short x = 0B0011010101110010;
      x = 13682;
      short y = 0B0110101011101011;
      System.out.println(Integer.toBinaryString(x & y));
      System.out.println(Integer.toBinaryString(~x));
      System.out.println(Integer.toBinaryString(x ^ y));
      System.out.println(Integer.toBinaryString(x | y));
      
      // shift
      System.out.println(1 << 8);
      System.out.println(Integer.toBinaryString(1 << 8));      
      System.out.println(8 >> 2);
      System.out.println(Integer.toBinaryString(8 >> 2));
      byte intAsByte = -1;
      System.out.println(Integer.toBinaryString(intAsByte));
      System.out.println(Integer.toBinaryString(intAsByte >> 1));
      String formatted = String.format("%32s", Integer.toBinaryString(intAsByte >>> 1)).replace(' ', '0');
      System.out.println(formatted);      
    }
  
    // conditional operators
    // be careful of short-circuiting: when not necessary, the right operand
    // is not always evaluated (e.g. true || (cond) -> cond is not evaluated)
    { 
      boolean booleanValue = true;
      int intValue = (booleanValue) ? 50 : 0;
      System.out.println(intValue);
      System.out.println(true && true);
      System.out.println(true && false);
      System.out.println(false && true);
      System.out.println(false && false);
      System.out.println(true || true);
      System.out.println(true || false);
      System.out.println(false || true);
      System.out.println(false || false);
      int x = 0;
      boolean status = true && ++x == 0;
      System.out.println(x);
      System.out.println(status);
      status = false && ++x == 0; // the right operand is not evaluated
      System.out.println(x);
      status = true || ++x == 0; // the right operand is not evaluated
      System.out.println(x);
      status = false || ++x == 0;
      System.out.println(x);
    }
    
    // logical operators
    // be aware that no short-circuiting happens with logical operators
    {
      int x = 0;
      System.out.println(false & ++x == 0);
      System.out.println(x);
      System.out.println(!false);
      System.out.println(true ^ true);
      System.out.println(true ^ false);
      System.out.println(true | ++x == 0);
      System.out.println(x);
    }
 
    // relational operators (equality)
    // be careful when comparing String variables
    // comparing objects means comparing reference to objects
    //  e.g. obj1 == obj2 means that the variable obj1 and obj2 refer to the same object 
    {
      int x = 0;
      System.out.println(x == 0);
      System.out.println(x != 0);
      double d = 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1;
      System.out.println(d);
      System.out.println(d == 0.9);
      System.out.println("A" == "A");
      System.out.println("A" == "B");
      System.out.println("AB" == "A" + "B");
      String s = "B";
      System.out.println("AB" == "A" + s);
      String s1 = "AB";
      String s2 = "AB";
      System.out.println(s1 == s2);    
      System.out.println(s1.equals(s2));
   }
    
    // array index operator
    {
      int[] grades = { 89, 90, 68, 73, 79 };
      System.out.println(grades[1]);
      grades[1] = 91;
      System.out.println(grades[1]);
      int index = 4;
      System.out.println(grades[index]);
      System.out.println(grades['C' - 'A']);
    }
  } 
}
